#
# Cookbook Name:: sidekiq
# Attrbutes:: default
#

default[:sidekiq] = {
  # Sidekiq will be installed on to application/solo instances,
  # unless a utility name is set, in which case, Sidekiq will
  # only be installed on to a utility instance that matches
  # the name
  # :utility_name => 'sidekiq',
  
  # Number of workers (not threads)
  :workers => 3,
  
  # Concurrency
  :concurrency => 3,
  
  # Queues
  :queues => {
    # :queue_name => priority
    :default => 5,
    :scoring_job_record_removal => 2
  },
  
  # Verbose
  :verbose => false
}


# sidekiq({
#   # Sidekiq will be installed on to application/solo instances,
#   # unless a utility name is set, in which case, Sidekiq will
#   # only be installed on to a utility instance that matches
#   # the name
#   :utility_name => 'sidekiq',
# 
#   # Number of workers (not threads)
#   :workers => 2,
# 
#   # Concurrency
#   :concurrency => 25,
# 
#   # Queues
#   :queues => {
#     # :queue_name => priority
#     :default => 1,
#   },
# 
#   # Verbose
#   :verbose => false
# })