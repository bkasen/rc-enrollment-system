# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141113203734) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "autoscoring_project_conversion_columns", :force => true do |t|
    t.integer  "autoscoring_project_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "autoscoring_project_conversion_columns", ["autoscoring_project_id"], :name => "idx_autoscore_prjct_cnvsn_clmns_atscr_prjct_id"

  create_table "autoscoring_project_conversion_values", :force => true do |t|
    t.integer  "autoscoring_project_conversion_column_id"
    t.string   "from_value"
    t.string   "to_value"
    t.text     "description"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "autoscoring_project_conversion_values", ["autoscoring_project_conversion_column_id"], :name => "idx_autoscore_prjct_cnvsn_vls_atscr_prjct_cnvsn_clmn_id"

  create_table "autoscoring_project_filter_columns", :force => true do |t|
    t.integer  "autoscoring_project_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.boolean  "filter_to_include_these_values", :default => true
  end

  add_index "autoscoring_project_filter_columns", ["autoscoring_project_id"], :name => "idx_autoscore_prjct_fltr_clmns_atscr_prjct_id"

  create_table "autoscoring_project_filter_values", :force => true do |t|
    t.integer  "autoscoring_project_filter_column_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "autoscoring_project_filter_values", ["autoscoring_project_filter_column_id"], :name => "idx_autoscore_prjct_fltr_vls_atscr_prjct_flr_clmn_id"

  create_table "autoscoring_project_geodemographic_variable_counts", :force => true do |t|
    t.integer  "autoscoring_project_geodemographic_variable_id"
    t.string   "name"
    t.integer  "enrolled"
    t.integer  "total"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "autoscoring_project_geodemographic_variable_counts", ["autoscoring_project_geodemographic_variable_id", "name"], :name => "idx_geodemo_variable_id_name"
  add_index "autoscoring_project_geodemographic_variable_counts", ["autoscoring_project_geodemographic_variable_id"], :name => "idx_autoscore_prjct_geo_var_cnts_vrble_id"
  add_index "autoscoring_project_geodemographic_variable_counts", ["name"], :name => "idx_as_prjct_geo_vr_cnts_name"

  create_table "autoscoring_project_geodemographic_variables", :force => true do |t|
    t.integer  "autoscoring_project_id"
    t.integer  "variable_id"
    t.decimal  "coefficient_value",      :precision => 10, :scale => 6
    t.decimal  "wald_score",             :precision => 10, :scale => 6
    t.boolean  "include_in_output",                                     :default => false
    t.datetime "created_at",                                                               :null => false
    t.datetime "updated_at",                                                               :null => false
    t.boolean  "has_uploaded_counts",                                   :default => false
  end

  add_index "autoscoring_project_geodemographic_variables", ["autoscoring_project_id"], :name => "idx_autoscore_project_geovariables_as_project_id"
  add_index "autoscoring_project_geodemographic_variables", ["variable_id"], :name => "idx_autoscore_project_geovariables_geo_var_id"

  create_table "autoscoring_project_job_statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "autoscoring_project_other_variable_counts", :force => true do |t|
    t.integer  "autoscoring_project_other_variable_id"
    t.string   "name"
    t.integer  "enrolled"
    t.integer  "total"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "autoscoring_project_other_variable_counts", ["autoscoring_project_other_variable_id"], :name => "idx_autoscore_prjct_othr_var_cnts_vrble_id"
  add_index "autoscoring_project_other_variable_counts", ["name", "autoscoring_project_other_variable_id"], :name => "idx_other_variable_id_name"
  add_index "autoscoring_project_other_variable_counts", ["name"], :name => "idx_as_prjct_oth_vr_cnts_name"

  create_table "autoscoring_project_other_variable_translations", :force => true do |t|
    t.integer  "autoscoring_project_other_variable_id"
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  add_index "autoscoring_project_other_variable_translations", ["autoscoring_project_other_variable_id"], :name => "idx_autoscore_project_othr_trns_othr_vr_id"
  add_index "autoscoring_project_other_variable_translations", ["code"], :name => "idx_as_prjct_othr_vr_cnts_code"

  create_table "autoscoring_project_other_variables", :force => true do |t|
    t.integer  "autoscoring_project_id"
    t.integer  "variable_id"
    t.integer  "date_format_id"
    t.string   "name"
    t.decimal  "coefficient_value",         :precision => 10, :scale => 6
    t.decimal  "wald_score",                :precision => 10, :scale => 6
    t.string   "secondary_column_name"
    t.string   "tertiary_column_name"
    t.boolean  "include_in_output",                                        :default => false
    t.datetime "created_at",                                                                  :null => false
    t.datetime "updated_at",                                                                  :null => false
    t.boolean  "has_uploaded_counts",                                      :default => false
    t.boolean  "has_uploaded_translations"
  end

  add_index "autoscoring_project_other_variables", ["autoscoring_project_id"], :name => "idx_autoscore_project_other_var_as_project_id"
  add_index "autoscoring_project_other_variables", ["date_format_id"], :name => "idx_autoscore_project_other_var_date_format_id"
  add_index "autoscoring_project_other_variables", ["variable_id"], :name => "idx_autoscore_project_gother_var_as_var_id"

  create_table "autoscoring_project_scoring_job_input_files", :force => true do |t|
    t.integer  "autoscoring_project_scoring_job_id"
    t.string   "file_name"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
    t.boolean  "processed",                          :default => false
    t.integer  "file_import_type_id"
  end

  add_index "autoscoring_project_scoring_job_input_files", ["autoscoring_project_scoring_job_id"], :name => "idx_apsj_input_file_scoring_job_id"
  add_index "autoscoring_project_scoring_job_input_files", ["created_at"], :name => "idx_ap_scr_jb_in_fl_created_at"
  add_index "autoscoring_project_scoring_job_input_files", ["file_import_type_id"], :name => "idx_apsj_input_file_file_import_type_id"

  create_table "autoscoring_project_scoring_job_logs", :force => true do |t|
    t.integer  "autoscoring_project_scoring_job_id"
    t.integer  "row_count"
    t.integer  "column_count"
    t.integer  "rows_filtered_out"
    t.text     "details",                            :limit => 2147483647
    t.text     "filter_criteria"
    t.text     "conversion_criteria"
    t.string   "split_output_by_column_name"
    t.integer  "autoscoring_project_job_status_id"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.text     "error_details"
  end

  add_index "autoscoring_project_scoring_job_logs", ["autoscoring_project_job_status_id"], :name => "idx_scoring_job_logs_scoring_job_status_id"
  add_index "autoscoring_project_scoring_job_logs", ["autoscoring_project_scoring_job_id"], :name => "idx_scoring_job_logs_scoring_job_id"
  add_index "autoscoring_project_scoring_job_logs", ["created_at"], :name => "idx_ap_scoring_job_created_at"

  create_table "autoscoring_project_scoring_job_output_files", :force => true do |t|
    t.integer  "autoscoring_project_scoring_job_id"
    t.string   "file_name"
    t.string   "split_value"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "autoscoring_project_scoring_job_output_files", ["autoscoring_project_scoring_job_id"], :name => "idx_apsj_output_file_scoring_job_id"

  create_table "autoscoring_project_scoring_job_records", :force => true do |t|
    t.integer  "autoscoring_project_scoring_job_id"
    t.integer  "postal_code_id"
    t.string   "em_opportunity_id"
    t.decimal  "probability_score",                  :precision => 7, :scale => 6
    t.decimal  "percentile_rank",                    :precision => 6, :scale => 3
    t.string   "rank"
    t.string   "sub_group"
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
    t.string   "em_person_id"
    t.string   "client_opportunity_id"
  end

  add_index "autoscoring_project_scoring_job_records", ["autoscoring_project_scoring_job_id"], :name => "idx_apsjr_autoscoring_project_scoring_job_id"
  add_index "autoscoring_project_scoring_job_records", ["postal_code_id"], :name => "idx_apsjr_postal_code_id"
  add_index "autoscoring_project_scoring_job_records", ["sub_group"], :name => "idx_as_prjct_scrd_rcds_subgroup"

  create_table "autoscoring_project_scoring_jobs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "autoscoring_project_id"
    t.integer  "row_count"
    t.integer  "column_count"
    t.integer  "rows_filtered_out"
    t.text     "details"
    t.text     "filter_criteria"
    t.text     "conversion_criteria"
    t.integer  "autoscoring_project_job_status_id"
    t.string   "split_output_by_column_name"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.boolean  "has_file_dimension_counts",         :default => false
    t.datetime "start_time"
    t.datetime "finish_time"
    t.integer  "number_of_attempts",                :default => 0
    t.integer  "cohort_year_id"
    t.boolean  "is_test_scoring",                   :default => false
    t.boolean  "is_public",                         :default => false
    t.integer  "rows_processed",                    :default => 0
  end

  add_index "autoscoring_project_scoring_jobs", ["autoscoring_project_id"], :name => "idx_apsj_autoscoring_project_id"
  add_index "autoscoring_project_scoring_jobs", ["autoscoring_project_job_status_id"], :name => "idx_apsj_job_status_id"
  add_index "autoscoring_project_scoring_jobs", ["created_at"], :name => "idx_ap_scrng_jb_created_at"
  add_index "autoscoring_project_scoring_jobs", ["user_id"], :name => "idx_apsj_user_id"

  create_table "autoscoring_project_statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "autoscoring_projects", :force => true do |t|
    t.integer  "institution_id"
    t.integer  "report_year_id"
    t.string   "name"
    t.text     "notes"
    t.string   "em_opportunity_id_column_name"
    t.string   "state_column_name"
    t.string   "postal_code_column_name"
    t.decimal  "default_yield_rate",                :precision => 10, :scale => 6
    t.decimal  "model_y_intercept",                 :precision => 10, :scale => 6
    t.integer  "admin_user_id"
    t.integer  "project_manager_id"
    t.datetime "created_at",                                                                          :null => false
    t.datetime "updated_at",                                                                          :null => false
    t.integer  "geodemographic_year_id"
    t.boolean  "active",                                                           :default => false
    t.string   "split_output_by_column_name"
    t.integer  "market_designation_level_id"
    t.string   "em_person_id_column_name"
    t.integer  "autoscoring_project_status_id",                                    :default => 1
    t.boolean  "is_em_configured",                                                 :default => true
    t.string   "client_opportunity_id_column_name"
  end

  add_index "autoscoring_projects", ["admin_user_id"], :name => "index_autoscoring_projects_on_admin_user_id"
  add_index "autoscoring_projects", ["autoscoring_project_status_id"], :name => "idx_ap_status_id"
  add_index "autoscoring_projects", ["geodemographic_year_id"], :name => "index_autoscoring_projects_on_geodemographic_year_id"
  add_index "autoscoring_projects", ["institution_id"], :name => "index_autoscoring_projects_on_institution_id"
  add_index "autoscoring_projects", ["market_designation_level_id"], :name => "idx_asp_market_designation_level_id"
  add_index "autoscoring_projects", ["project_manager_id"], :name => "index_autoscoring_projects_on_project_manager_id"
  add_index "autoscoring_projects", ["report_year_id"], :name => "index_autoscoring_projects_on_report_year_id"

  create_table "counties", :force => true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.integer  "state_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "counties", ["country_id"], :name => "index_counties_on_country_id"
  add_index "counties", ["name", "state_id", "country_id"], :name => "index_counties_on_name_and_state_id_and_country_id"
  add_index "counties", ["state_id"], :name => "index_counties_on_state_id"

  create_table "countries", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "date_formats", :force => true do |t|
    t.string   "name"
    t.string   "format"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "file_import_types", :force => true do |t|
    t.string   "name"
    t.boolean  "institution_specific"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "file_imports", :force => true do |t|
    t.string   "name"
    t.integer  "file_import_type_id"
    t.string   "file_name"
    t.boolean  "processed"
    t.integer  "institution_id"
    t.integer  "user_id"
    t.integer  "admin_user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "file_imports", ["file_import_type_id"], :name => "index_file_imports_on_file_import_type_id"

  create_table "geodemographic_variables", :force => true do |t|
    t.string   "name"
    t.integer  "variable_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "geodemographic_variables", ["variable_id"], :name => "index_geodemographic_variables_on_variable_id"

  create_table "geodemographic_variables_years", :force => true do |t|
    t.integer  "year_id"
    t.integer  "geodemographic_variable_id"
    t.decimal  "upper_threshold",            :precision => 10, :scale => 0
    t.decimal  "lower_threshold",            :precision => 10, :scale => 0
    t.boolean  "active"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "institutions", :force => true do |t|
    t.string   "unitid"
    t.string   "name"
    t.string   "city"
    t.integer  "postal_code_id"
    t.integer  "state_id"
    t.integer  "country_id"
    t.string   "display_name"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "emp_organization_name"
  end

  add_index "institutions", ["postal_code_id"], :name => "index_institutions_on_postal_code_id"

  create_table "job_titles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "market_designation_levels", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "postal_code_geodemographics", :force => true do |t|
    t.integer  "year_id"
    t.integer  "postal_code_id"
    t.integer  "geodemographic_variable_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "postal_code_geodemographics", ["geodemographic_variable_id"], :name => "index_postal_code_geodemographics_on_geodemographic_variable_id"
  add_index "postal_code_geodemographics", ["postal_code_id"], :name => "index_postal_code_geodemographics_on_postal_code_id"
  add_index "postal_code_geodemographics", ["year_id"], :name => "index_postal_code_geodemographics_on_year_id"

  create_table "postal_codes", :force => true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.integer  "country_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "postal_codes", ["country_id"], :name => "index_postal_codes_on_country_id"
  add_index "postal_codes", ["name", "country_id"], :name => "index_postal_codes_on_name_and_country_id"
  add_index "postal_codes", ["name"], :name => "index_postal_codes_on_name"
  add_index "postal_codes", ["state_id", "name"], :name => "index_postal_codes_on_state_id_and_name"
  add_index "postal_codes", ["state_id"], :name => "index_postal_codes_on_state_id"

  create_table "postal_codes_years", :force => true do |t|
    t.integer  "postal_code_id"
    t.integer  "year_id"
    t.integer  "high_school_age_population"
    t.string   "geo_name"
    t.integer  "county_id"
    t.integer  "case_district"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.boolean  "residential",                :default => false
    t.integer  "mdg_id"
    t.integer  "adg_id"
    t.integer  "eth_id"
    t.integer  "hvl_id"
    t.integer  "mfi_id"
    t.integer  "mhb_id"
    t.integer  "pop_id"
  end

  add_index "postal_codes_years", ["adg_id"], :name => "index_postal_codes_years_on_adg_id"
  add_index "postal_codes_years", ["county_id"], :name => "index_postal_codes_years_on_county_id"
  add_index "postal_codes_years", ["eth_id"], :name => "index_postal_codes_years_on_eth_id"
  add_index "postal_codes_years", ["hvl_id"], :name => "index_postal_codes_years_on_hvl_id"
  add_index "postal_codes_years", ["mdg_id"], :name => "index_postal_codes_years_on_mdg_id"
  add_index "postal_codes_years", ["mfi_id"], :name => "index_postal_codes_years_on_mfi_id"
  add_index "postal_codes_years", ["mhb_id"], :name => "index_postal_codes_years_on_mhb_id"
  add_index "postal_codes_years", ["pop_id"], :name => "index_postal_codes_years_on_pop_id"
  add_index "postal_codes_years", ["postal_code_id"], :name => "index_postal_codes_years_on_postal_code_id"
  add_index "postal_codes_years", ["year_id"], :name => "index_postal_codes_years_on_year_id"

  create_table "project_managers", :force => true do |t|
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.integer  "country_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "states", ["country_id"], :name => "index_states_on_country_id"
  add_index "states", ["name"], :name => "index_states_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "job_title_id"
    t.boolean  "active"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["job_title_id"], :name => "index_users_on_job_title_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "variable_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "variables", :force => true do |t|
    t.string   "name"
    t.boolean  "default"
    t.integer  "variable_type_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "abbreviation"
    t.boolean  "is_date",              :default => false
    t.integer  "position"
    t.boolean  "in_client_input_file", :default => true
  end

  add_index "variables", ["name"], :name => "index_variables_on_name"
  add_index "variables", ["variable_type_id"], :name => "index_variables_on_variable_type_id"

  create_table "years", :force => true do |t|
    t.string   "name"
    t.integer  "value"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "has_geodemographic_data", :default => false
  end

end
