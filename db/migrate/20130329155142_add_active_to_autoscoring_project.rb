class AddActiveToAutoscoringProject < ActiveRecord::Migration
  def change
    add_column :autoscoring_projects, :active, :boolean, :default => false
  end
end
