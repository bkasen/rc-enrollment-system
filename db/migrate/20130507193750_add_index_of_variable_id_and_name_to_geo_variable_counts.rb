class AddIndexOfVariableIdAndNameToGeoVariableCounts < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_geodemographic_variable_counts, [:autoscoring_project_geodemographic_variable_id, :name], :name => "idx_geodemo_variable_id_name"
  end
end
