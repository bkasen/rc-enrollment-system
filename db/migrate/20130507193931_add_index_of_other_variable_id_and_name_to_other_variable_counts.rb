class AddIndexOfOtherVariableIdAndNameToOtherVariableCounts < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_other_variable_counts, [:name, :autoscoring_project_other_variable_id], :name => "idx_other_variable_id_name"
  end
end
