class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :unitid
      t.string :name
      t.string :city
      t.integer :postal_code_id
      t.integer :state_id
      t.integer :country_id
      t.string :display_name

      t.timestamps
    end
  end
end
