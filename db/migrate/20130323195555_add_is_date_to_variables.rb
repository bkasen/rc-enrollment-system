class AddIsDateToVariables < ActiveRecord::Migration
  def change
    add_column :variables, :is_date, :boolean, :default => false
  end
end
