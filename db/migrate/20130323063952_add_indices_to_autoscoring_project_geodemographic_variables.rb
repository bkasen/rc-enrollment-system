class AddIndicesToAutoscoringProjectGeodemographicVariables < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_geodemographic_variables, :autoscoring_project_id, :name => 'idx_autoscore_project_geovariables_as_project_id'
    add_index :autoscoring_project_geodemographic_variables, :geodemographic_variable_id, :name => 'idx_autoscore_project_geovariables_geo_var_id'
  end
end
