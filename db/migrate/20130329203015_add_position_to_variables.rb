class AddPositionToVariables < ActiveRecord::Migration
  def change
    add_column :variables, :position, :integer
  end
end
