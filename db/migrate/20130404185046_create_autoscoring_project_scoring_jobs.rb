class CreateAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_scoring_jobs do |t|
      t.integer :user_id
      t.integer :autoscoring_project_id
      t.integer :row_count
      t.integer :column_count
      t.integer :rows_filtered_out
      t.text :details
      t.text :filter_criteria
      t.text :conversion_criteria
      t.integer :autoscoring_project_job_status_id
      t.string :split_output_by_column_name

      t.timestamps
    end
     add_index :autoscoring_project_scoring_jobs, :user_id, :name => "idx_apsj_user_id"
     add_index :autoscoring_project_scoring_jobs, :autoscoring_project_id, :name => "idx_apsj_autoscoring_project_id"
     add_index :autoscoring_project_scoring_jobs, :autoscoring_project_job_status_id, :name => "idx_apsj_job_status_id"
  end
end
