class AddGeodemographicYearIdToAutoscoringProjects < ActiveRecord::Migration
  def change

    add_column :autoscoring_projects, :geodemographic_year_id, :integer
    rename_column :autoscoring_projects, :year_id, :report_year_id

  end
end
