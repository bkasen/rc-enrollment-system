class AddIndicesToAutoscoringProjectOtherVariables < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_other_variables, :autoscoring_project_id, :name => 'idx_autoscore_project_other_var_as_project_id'
    add_index :autoscoring_project_other_variables, :variable_id, :name => 'idx_autoscore_project_gother_var_as_var_id'
    add_index :autoscoring_project_other_variables, :date_format_id, :name => 'idx_autoscore_project_other_var_date_format_id'
  end
end
