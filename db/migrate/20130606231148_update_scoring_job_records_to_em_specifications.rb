class UpdateScoringJobRecordsToEmSpecifications < ActiveRecord::Migration
  def up
    rename_column :autoscoring_project_scoring_job_records, :opportunity_id, :em_opportunity_id
    add_column :autoscoring_project_scoring_job_records, :em_person_id, :string
  end

  def down
    rename_column :autoscoring_project_scoring_job_records, :em_opportunity_id, :opportunity_id
    remove_column :autoscoring_project_scoring_job_records, :em_person_id
  end
end
