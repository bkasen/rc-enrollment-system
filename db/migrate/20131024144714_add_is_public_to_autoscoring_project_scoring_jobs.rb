class AddIsPublicToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def up
    add_column :autoscoring_project_scoring_jobs, :is_public, :boolean, :default => false
  end
  
  def down
    remove_column :autoscoring_project_scoring_jobs, :is_public
  end
end
