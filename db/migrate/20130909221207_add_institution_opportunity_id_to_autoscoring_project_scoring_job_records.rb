class AddInstitutionOpportunityIdToAutoscoringProjectScoringJobRecords < ActiveRecord::Migration
  def change
      add_column :autoscoring_project_scoring_job_records, :institution_opportunity_id, :string
  end
end
