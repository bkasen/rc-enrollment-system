class RenameFieldInGeodemographicVariablesTable < ActiveRecord::Migration
  def up
        rename_column :geodemographic_variables, :variable_type_id, :inquiry_variable_id
  end

  def down
  end
end
