class CreateAutoscoringProjects < ActiveRecord::Migration
  def change
    create_table :autoscoring_projects do |t|
      t.integer :institution_id
      t.integer :year_id
      t.string :name
      t.text :notes
      t.string :opportunity_id_column_name
      t.string :state_column_name
      t.string :postal_code_column_name
      t.decimal :default_yield_rate, :precision => 10, :scale => 6
      t.decimal :model_y_intercept, :precision => 10, :scale => 6
      t.integer :admin_user_id
      t.integer :project_manager_id
      t.integer :analyst_id

      t.timestamps

    end
    
    add_index :autoscoring_projects, :institution_id
    add_index :autoscoring_projects, :year_id
    add_index :autoscoring_projects, :admin_user_id
    add_index :autoscoring_projects, :project_manager_id
    add_index :autoscoring_projects, :analyst_id
  end
end
