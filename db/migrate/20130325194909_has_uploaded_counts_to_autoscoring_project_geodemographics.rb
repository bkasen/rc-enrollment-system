class HasUploadedCountsToAutoscoringProjectGeodemographics < ActiveRecord::Migration
  def up
      add_column :autoscoring_project_geodemographic_variables, :has_uploaded_counts, :boolean, :default => false
  end

  def down
    remove_column :autoscoring_project_geodemographic_variables, :has_uploaded_counts
  end
end
