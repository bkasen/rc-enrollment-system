class AddHasGeodemographicDataToYears < ActiveRecord::Migration
  def change
    add_column :years, :has_geodemographic_data, :boolean, :default => false
  end
end
