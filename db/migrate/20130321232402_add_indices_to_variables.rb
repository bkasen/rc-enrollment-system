class AddIndicesToVariables < ActiveRecord::Migration
  def change
    add_index :variables, :variable_type_id
  end
end
