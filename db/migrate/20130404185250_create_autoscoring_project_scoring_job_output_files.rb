class CreateAutoscoringProjectScoringJobOutputFiles < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_scoring_job_output_files do |t|
      t.integer :autoscoring_project_scoring_job_id
      t.string :file_name
      t.string :split_value

      t.timestamps
    end
      add_index :autoscoring_project_scoring_job_output_files, :autoscoring_project_scoring_job_id, :name => "idx_apsj_output_file_scoring_job_id"
  end
end
