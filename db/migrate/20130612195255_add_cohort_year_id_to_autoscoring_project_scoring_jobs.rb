class AddCohortYearIdToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def up
    add_column :autoscoring_project_scoring_jobs, :cohort_year_id, :integer
  end

  def down
    remove_column :autoscoring_project_scoring_jobs, :cohort_year_id
  end
end
