class CreateAutoscoringProjectGeodemographicVariables < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_geodemographic_variables do |t|
      t.integer :autoscoring_project_id
      t.integer :geodemographic_variable_id
      t.decimal :coefficient_value, :precision => 10, :scale => 6
      t.decimal :wald_score, :precision => 10, :scale => 6
      t.boolean :include_in_output, :default => false

      t.timestamps
    end
    
  end
end
