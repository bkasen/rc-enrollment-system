class AddAutoscoringProjectStatusIdToAutoscoringProjects < ActiveRecord::Migration
  def change
    add_column :autoscoring_projects, :autoscoring_project_status_id, :integer, :default => 1
    add_index :autoscoring_projects, :autoscoring_project_status_id, :name => "idx_ap_status_id"
  end
end
