class CreateAutoscoringProjectJobStatuses < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_job_statuses do |t|
      t.string :name

      t.timestamps
    end
  end
end
