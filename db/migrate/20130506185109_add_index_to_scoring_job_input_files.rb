class AddIndexToScoringJobInputFiles < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_scoring_job_input_files, :created_at, :name => "idx_ap_scr_jb_in_fl_created_at"
  end
end
