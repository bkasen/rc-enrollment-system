class AddTimeMetricsToAutoprojectScoringJobs < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_jobs, :start_time, :datetime
    add_column :autoscoring_project_scoring_jobs, :finish_time, :datetime
    
  end
end
