class CreateAutoscoringProjectScoringJobLogs < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_scoring_job_logs do |t|
      t.integer :autoscoring_project_scoring_job_id
      t.integer :row_count
      t.integer :column_count
      t.integer :rows_filtered_out
      t.text :details
      t.text :filter_criteria
      t.text :conversion_criteria
      t.string :split_output_by_column_name
      t.integer :autoscoring_project_job_status_id

      t.timestamps
    end
    add_index :autoscoring_project_scoring_job_logs, :autoscoring_project_scoring_job_id, :name => 'idx_scoring_job_logs_scoring_job_id'
    add_index :autoscoring_project_scoring_job_logs, :autoscoring_project_job_status_id, :name => 'idx_scoring_job_logs_scoring_job_status_id'
    
  end
end
