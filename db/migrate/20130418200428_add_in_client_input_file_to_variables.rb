class AddInClientInputFileToVariables < ActiveRecord::Migration
  def change
    add_column :variables, :in_client_input_file, :boolean, :default => true
  end
end
