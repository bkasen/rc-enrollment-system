class AddPersonIdColumnToAutoscoringProjects < ActiveRecord::Migration
  def up
    add_column :autoscoring_projects, :person_id_column_name, :string
  end

  def down
    remove_column :autoscoring_projects, :person_id_column_name
  end
end
