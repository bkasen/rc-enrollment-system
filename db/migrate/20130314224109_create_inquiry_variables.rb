class CreateInquiryVariables < ActiveRecord::Migration
  def change
    create_table :inquiry_variables do |t|
      t.string :name
      t.boolean :default
      t.integer :variable_type_id
      t.timestamps
    end
  end
end
