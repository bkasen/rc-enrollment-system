class CreateAutoscoringProjectConversionValues < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_conversion_values do |t|
      t.integer :autoscoring_project_conversion_column_id
      t.string :from_value
      t.string :to_value
      t.text :description

      t.timestamps
    end
    add_index :autoscoring_project_conversion_values, :autoscoring_project_conversion_column_id, :name => 'idx_autoscore_prjct_cnvsn_vls_atscr_prjct_cnvsn_clmn_id'
    
  end
end
