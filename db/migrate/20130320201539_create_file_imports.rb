class CreateFileImports < ActiveRecord::Migration
  def change
    create_table :file_imports do |t|
      t.string :name
      t.integer :file_import_type_id
      t.string :file_path
      t.boolean :processed
      t.integer :institution_id
      t.integer :user_id
      t.integer :admin_user_id

      t.timestamps
    end
  end
end
