class AddMarketDesignationLevelIdToAutoscoringProjects < ActiveRecord::Migration
  def change
    add_column :autoscoring_projects, :market_designation_level_id, :integer
    add_index :autoscoring_projects, :market_designation_level_id, :name => "idx_asp_market_designation_level_id"
    
  end
end
