class AddIndexToCreatedAtOnScoringJobs < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_scoring_jobs, :created_at, :name => "idx_ap_scrng_jb_created_at"
  end
end
