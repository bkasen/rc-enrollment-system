class CreateAutoscoringProjectScoringJobRecords < ActiveRecord::Migration
  def up
    create_table :autoscoring_project_scoring_job_records do |t|
      t.integer :autoscoring_project_scoring_job_id
      t.integer :postal_code_id
      t.string :opportunity_id, :length => 100
      t.decimal :probability_score, :precision => 7, :scale => 6
      t.decimal :percentile_rank, :precision => 6, :scale => 3
      t.string :rank, :length => 1
      t.string :sub_group, :length => 100

      t.timestamps
    end
    # id column was changed to unsigned manually so it only accepts positive integers increasing the upper limit to doubled since we don't need to account for negative numbers
    add_index :autoscoring_project_scoring_job_records, :autoscoring_project_scoring_job_id, :name => "idx_apsjr_autoscoring_project_scoring_job_id"
    add_index :autoscoring_project_scoring_job_records, :postal_code_id, :name => "idx_apsjr_postal_code_id"
  end
end
