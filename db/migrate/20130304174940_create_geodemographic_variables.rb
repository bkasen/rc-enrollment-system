class CreateGeodemographicVariables < ActiveRecord::Migration
  def change
    create_table :geodemographic_variables do |t|
      t.string :name
      t.string :abbreviation
      t.integer :variable_type_id

      t.timestamps
    end
  end
end
