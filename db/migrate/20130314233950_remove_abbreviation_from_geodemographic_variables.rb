class RemoveAbbreviationFromGeodemographicVariables < ActiveRecord::Migration
  def up
    remove_column :geodemographic_variables, :abbreviation
  end

  def down
  end
end
