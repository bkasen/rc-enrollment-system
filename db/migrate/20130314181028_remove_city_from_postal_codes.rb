class RemoveCityFromPostalCodes < ActiveRecord::Migration
  def up
    remove_column :postal_codes, :city
  end

  def down
  end
end
