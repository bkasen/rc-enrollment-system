class CreateProjectManagers < ActiveRecord::Migration
  def self.up
    create_table :project_managers do |t|
      t.string :email
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
    # Now populate with a default PM user

    ProjectManager.create :email => 'brian.kasen@gmail.com', :first_name => 'Brian', :last_name => 'Kasen'
  end
  
  def self.down
    drop_table :project_managers
  end
end
