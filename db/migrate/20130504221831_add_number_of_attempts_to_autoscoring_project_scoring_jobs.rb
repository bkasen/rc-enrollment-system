class AddNumberOfAttemptsToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_jobs, :number_of_attempts, :integer, :default => 0
  end
end
