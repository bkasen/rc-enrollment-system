class AddIndicesToFileImports < ActiveRecord::Migration
  def change
    add_index :file_imports, :file_import_type_id
  end
end
