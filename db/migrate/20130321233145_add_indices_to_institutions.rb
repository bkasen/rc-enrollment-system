class AddIndicesToInstitutions < ActiveRecord::Migration
  def change
     add_index :institutions, :postal_code_id
  end
end
