class CreateAutoscoringProjectFilterValues < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_filter_values do |t|
      t.integer :autoscoring_project_filter_column_id
      t.string :name
      t.text :description

      t.timestamps
    end
    
    add_index :autoscoring_project_filter_values, :autoscoring_project_filter_column_id, :name => 'idx_autoscore_prjct_fltr_vls_atscr_prjct_flr_clmn_id'
    
  end
end
