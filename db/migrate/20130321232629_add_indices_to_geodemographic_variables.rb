class AddIndicesToGeodemographicVariables < ActiveRecord::Migration
  def change
    add_index :geodemographic_variables, :variable_id
  end
end
