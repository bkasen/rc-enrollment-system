class AddIndexOnSubGroupToApScoringJobRecords < ActiveRecord::Migration
  def change
      add_index :autoscoring_project_scoring_job_records, :sub_group, :name => 'idx_as_prjct_scrd_rcds_subgroup'
  end
end
