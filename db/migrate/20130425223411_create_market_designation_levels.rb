class CreateMarketDesignationLevels < ActiveRecord::Migration
  def change
    create_table :market_designation_levels do |t|
      t.string :name

      t.timestamps
    end
  end
end
