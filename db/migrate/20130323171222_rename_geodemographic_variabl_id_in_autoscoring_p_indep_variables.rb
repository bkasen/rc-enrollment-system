class RenameGeodemographicVariablIdInAutoscoringPIndepVariables < ActiveRecord::Migration
  def up
    rename_column :autoscoring_project_geodemographic_variables, :geodemographic_variable_id, :variable_id
  end

  def down
  end
end
