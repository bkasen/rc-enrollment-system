class RenameAgeCyCountToHighSchoolAgePopulationInPostalCodesYears < ActiveRecord::Migration
  def up
    rename_column :postal_codes_years, :age_cy_count, :high_school_age_population
  end

  def down
  end
end
