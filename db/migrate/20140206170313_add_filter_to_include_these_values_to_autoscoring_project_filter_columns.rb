class AddFilterToIncludeTheseValuesToAutoscoringProjectFilterColumns < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_filter_columns, :filter_to_include_these_values, :boolean, :default => true
  end
end
