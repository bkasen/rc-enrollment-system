class AddAbbreviationToInquiryVariables < ActiveRecord::Migration
  def change
    add_column :inquiry_variables, :abbreviation, :string
  end
end
