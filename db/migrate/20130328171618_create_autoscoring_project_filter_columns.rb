class CreateAutoscoringProjectFilterColumns < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_filter_columns do |t|
      t.integer :autoscoring_project_id
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :autoscoring_project_filter_columns, :autoscoring_project_id, :name => 'idx_autoscore_prjct_fltr_clmns_atscr_prjct_id'
  end
end
