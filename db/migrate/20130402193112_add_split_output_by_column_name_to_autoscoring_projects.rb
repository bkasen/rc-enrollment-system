class AddSplitOutputByColumnNameToAutoscoringProjects < ActiveRecord::Migration
  def self.up
    add_column :autoscoring_projects, :split_output_by_column_name, :string
  end
  
  def self.down
    remove_column :autoscoring_projects, :split_output_by_column_name
  end
end
