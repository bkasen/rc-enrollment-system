class AddIndicesToPostalCodesGeodemographics < ActiveRecord::Migration
  def change
    add_index :postal_code_geodemographics, :year_id
    add_index :postal_code_geodemographics, :postal_code_id
    add_index :postal_code_geodemographics, :geodemographic_variable_id
  end
end
