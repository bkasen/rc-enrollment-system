class CreateAutoscoringProjectOtherVariables < ActiveRecord::Migration
  def up
    create_table :autoscoring_project_other_variables do |t|
      t.integer :autoscoring_project_id
      t.integer :variable_id
      t.integer :date_format_id
      t.string :name
      t.decimal :coefficient_value, :precision => 10, :scale => 6
      t.decimal :wald_score, :precision => 10, :scale => 6
      t.string :secondary_column_name
      t.string :tertiary_column_name
      t.boolean :include_in_output, :default => false

      t.timestamps
    end

  end
end
