class CreateAutoscoringProjectOtherVariableTranslations < ActiveRecord::Migration
  def change
    create_table :autoscoring_project_other_variable_translations do |t|
      t.integer :autoscoring_project_other_variable_id
      t.string :description
      t.string :code

      t.timestamps
    end
    
     add_index :autoscoring_project_other_variable_translations, :autoscoring_project_other_variable_id, :name => 'idx_autoscore_project_othr_trns_othr_vr_id'
  end
end
