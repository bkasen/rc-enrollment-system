class CreateGeodemographicVariablesYears < ActiveRecord::Migration
  def change
    create_table :geodemographic_variables_years do |t|
      t.integer :year_id
      t.integer :geodemographic_variable_id
      t.decimal :upper_threshold
      t.decimal :lower_threshold
      t.boolean :active

      t.timestamps
    end
  end
end
