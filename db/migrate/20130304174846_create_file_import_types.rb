class CreateFileImportTypes < ActiveRecord::Migration
  def change
    create_table :file_import_types do |t|
      t.string :name
      t.boolean :institution_specific

      t.timestamps
    end
  end
end
