class HasUploadedCountsToAutoscoringProjectOtherVariables < ActiveRecord::Migration
  def up
      add_column :autoscoring_project_other_variables, :has_uploaded_counts, :boolean, :default => false
  end

  def down
    remove_column :autoscoring_project_other_variables, :has_uploaded_counts
  end
end
