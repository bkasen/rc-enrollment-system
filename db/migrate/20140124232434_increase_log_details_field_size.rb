class IncreaseLogDetailsFieldSize < ActiveRecord::Migration
  def up
    change_column :autoscoring_project_scoring_job_logs, :details, :text, :limit => 4294967295
  end

  def down
    change_column :autoscoring_project_scoring_job_logs, :details, :text, :limit => 16777215
  end
end
