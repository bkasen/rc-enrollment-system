class CreateAutoscoringProjectStatuses < ActiveRecord::Migration
  def up  
    create_table :autoscoring_project_statuses do |t|
      t.string :name

      t.timestamps
    end
    
    AutoscoringProjectStatus.create(:name => "Unavailable to All Users")
    AutoscoringProjectStatus.create(:name => "Available Only to Admin Users")
    AutoscoringProjectStatus.create(:name => "Available to All Users")
    
  end
  
  def down
    drop_table :autoscoring_project_statuses
  end
end
