class AddIndexToReportYearIdInAutoscoringProjects < ActiveRecord::Migration
  def change
    add_index :autoscoring_projects, :geodemographic_year_id
  end
end
