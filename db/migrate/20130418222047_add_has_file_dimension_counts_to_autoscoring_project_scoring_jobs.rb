class AddHasFileDimensionCountsToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_jobs, :has_file_dimension_counts, :boolean, :default => false
  end
end
