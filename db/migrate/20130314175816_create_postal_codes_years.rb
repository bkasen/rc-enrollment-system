class CreatePostalCodesYears < ActiveRecord::Migration
  def change
    create_table :postal_codes_years do |t|
      t.integer :postal_code_id
      t.integer :year_id
      t.integer :age_cy_count
      t.string :geo_name
      t.integer :county_id
      t.integer :case_district

      t.timestamps
    end
  end
end
