class AddDemographicVariablesToPostalCodesYears < ActiveRecord::Migration
  def change
    add_column :postal_codes_years, :mdg_id, :integer
    add_column :postal_codes_years, :adg_id, :integer
    add_column :postal_codes_years, :eth_id, :integer
    add_column :postal_codes_years, :hvl_id, :integer
    add_column :postal_codes_years, :mfi_id, :integer
    add_column :postal_codes_years, :mhb_id, :integer
    add_column :postal_codes_years, :pop_id, :integer
    
    
    add_index :postal_codes_years, :mdg_id
    add_index :postal_codes_years, :adg_id
    add_index :postal_codes_years, :eth_id
    add_index :postal_codes_years, :hvl_id
    add_index :postal_codes_years, :mfi_id
    add_index :postal_codes_years, :mhb_id
    add_index :postal_codes_years, :pop_id
  end
end
