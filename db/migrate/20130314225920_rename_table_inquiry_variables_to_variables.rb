class RenameTableInquiryVariablesToVariables < ActiveRecord::Migration
  def up
     rename_table :inquiry_variables, :variables
      rename_column :geodemographic_variables, :inquiry_variable_id, :variable_id
  end

  def down
     rename_table :variables, :inquiry_variables
     rename_column :geodemographic_variables, :variable_id, :inquiry_variable_id
  end
end
