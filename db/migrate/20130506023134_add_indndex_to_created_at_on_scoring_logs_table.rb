class AddIndndexToCreatedAtOnScoringLogsTable < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_scoring_job_logs, :created_at, :name => "idx_ap_scoring_job_created_at"
  end
end
