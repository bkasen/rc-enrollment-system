class AddErrorDetailsToAutoscoringProjectScoringJobLogs < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_job_logs, :error_details, :text
  end
end
