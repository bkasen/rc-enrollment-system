class AddProcessedToScoringJobInputFiles < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_job_input_files, :processed, :boolean, :default => false
    
  end
end
