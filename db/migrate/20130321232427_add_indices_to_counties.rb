class AddIndicesToCounties < ActiveRecord::Migration
  def change
    add_index :counties, :country_id
    add_index :counties, :state_id
    add_index :counties,  [:name, :state_id, :country_id]
  end
end
