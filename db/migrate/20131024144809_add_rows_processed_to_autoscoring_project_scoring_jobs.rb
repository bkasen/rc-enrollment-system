class AddRowsProcessedToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def up
    add_column :autoscoring_project_scoring_jobs, :rows_processed, :integer, :default => 0
    execute "UPDATE autoscoring_project_scoring_jobs SET rows_processed = row_count WHERE autoscoring_project_job_status_id IN (3,4)"
  end
  
  def down
    remove_column :autoscoring_project_scoring_jobs, :rows_processed
  end
end
