class CreatePostalCodes < ActiveRecord::Migration
  def change
    create_table :postal_codes do |t|
      t.string :name
      t.string :city
      t.integer :state_id
      t.integer :country_id

      t.timestamps
    end
  end
end
