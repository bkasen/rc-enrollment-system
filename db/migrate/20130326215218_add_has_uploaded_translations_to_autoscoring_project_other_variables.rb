class AddHasUploadedTranslationsToAutoscoringProjectOtherVariables < ActiveRecord::Migration
  def up
    add_column :autoscoring_project_other_variables, :has_uploaded_translations, :boolean
  end
  
  def down
    remove_column :autoscoring_project_other_variables, :has_uploaded_translations
  end
end
