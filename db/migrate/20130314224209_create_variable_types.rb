class CreateVariableTypes < ActiveRecord::Migration
  def change
    create_table :variable_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
