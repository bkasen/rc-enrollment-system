class CreatePostalCodeGeodemographics < ActiveRecord::Migration
  def change
    create_table :postal_code_geodemographics do |t|
      t.integer :year_id
      t.integer :postal_code_id
      t.integer :geodemographic_variable_id

      t.timestamps
    end
  end
end
