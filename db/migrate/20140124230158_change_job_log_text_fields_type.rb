class ChangeJobLogTextFieldsType < ActiveRecord::Migration
  def up
    change_column :autoscoring_project_scoring_job_logs, :details, :text, :limit => 16777215
  end

  def down
    change_column :autoscoring_project_scoring_job_logs, :details, :text, :limit => 65535
  end
end
