class RenameInstitutionOpportunityIdToClientOpportunityIdInAutoscoringProjectScoringJobRecords < ActiveRecord::Migration
  def up
    rename_column :autoscoring_project_scoring_job_records, :institution_opportunity_id, :client_opportunity_id
  end
  
  def down
    rename_column :autoscoring_project_scoring_job_records, :client_opportunity_id, :institution_opportunity_id
  end
end
