class AddIndexToCodeInOtherVariableTranslationTable < ActiveRecord::Migration
  def change
      add_index :autoscoring_project_other_variable_translations, :code, :name => 'idx_as_prjct_oth_vr_trns_code'
  end
end
