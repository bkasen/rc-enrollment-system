class AddResidentialToPostalCodesYears < ActiveRecord::Migration
  def change
    add_column :postal_codes_years, :residential, :boolean, :default => 0
  end
end
