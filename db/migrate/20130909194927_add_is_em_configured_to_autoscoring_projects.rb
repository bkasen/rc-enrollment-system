class AddIsEmConfiguredToAutoscoringProjects < ActiveRecord::Migration
  def change
     add_column :autoscoring_projects, :is_em_configured, :boolean, :default => true
  end
end
