class RemoveAnalystIdFromAutoscoringProjects < ActiveRecord::Migration
  def up
    remove_column :autoscoring_projects, :analyst_id
  end

  def down
    add_column :autoscoring_projects, :analyst_id, :integer
  end
end
