class AddIndexToNameInVariableCounts < ActiveRecord::Migration
  def change
    add_index :autoscoring_project_geodemographic_variable_counts, :name, :name => 'idx_as_prjct_geo_vr_cnts_name'
    add_index :autoscoring_project_other_variable_counts, :name, :name => 'idx_as_prjct_oth_vr_cnts_name'
  end
end
