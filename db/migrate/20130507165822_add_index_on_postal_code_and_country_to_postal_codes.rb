class AddIndexOnPostalCodeAndCountryToPostalCodes < ActiveRecord::Migration
  def change
    add_index :postal_codes, [:name, :country_id]
  end
end
