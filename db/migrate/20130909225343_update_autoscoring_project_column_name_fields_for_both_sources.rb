class UpdateAutoscoringProjectColumnNameFieldsForBothSources < ActiveRecord::Migration
  def up
    rename_column :autoscoring_projects, :opportunity_id_column_name, :em_opportunity_id_column_name
    rename_column :autoscoring_projects, :person_id_column_name, :em_person_id_column_name
    add_column :autoscoring_projects, :client_opportunity_id_column_name, :string
  end

  def down
    rename_column :autoscoring_projects, :em_opportunity_id_column_name, :opportunity_id_column_name
    rename_column :autoscoring_projects, :em_person_id_column_name, :person_id_column_name
    remove_column :autoscoring_projects, :client_opportunity_id_column_name
  end
end
