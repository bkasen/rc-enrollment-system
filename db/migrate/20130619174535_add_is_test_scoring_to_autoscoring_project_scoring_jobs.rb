class AddIsTestScoringToAutoscoringProjectScoringJobs < ActiveRecord::Migration
  def up
    add_column :autoscoring_project_scoring_jobs, :is_test_scoring, :boolean, :default => false
  end
  
  def down
    remove_column :autoscoring_project_scoring_jobs, :is_test_scoring
  end
end
