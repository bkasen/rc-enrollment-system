class AddFileImportTypeToAutoscoringScoringJobsInputs < ActiveRecord::Migration
  def change
    add_column :autoscoring_project_scoring_job_input_files, :file_import_type_id, :integer
    add_index :autoscoring_project_scoring_job_input_files, :file_import_type_id, :name => "idx_apsj_input_file_file_import_type_id"
    
  end
  
end
