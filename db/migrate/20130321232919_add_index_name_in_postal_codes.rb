class AddIndexNameInPostalCodes < ActiveRecord::Migration
  def up
    add_index :postal_codes, :name
    add_index :postal_codes, [:state_id, :name]
  end

  def down
  end
end
