class AddIndicesToPostalCodesYears < ActiveRecord::Migration
  def change
    add_index :postal_codes_years, :postal_code_id
    add_index :postal_codes_years, :year_id
    add_index :postal_codes_years, :county_id
  end
end
