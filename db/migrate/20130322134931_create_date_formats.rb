class CreateDateFormats < ActiveRecord::Migration
  def change
    create_table :date_formats do |t|
      t.string :name
      t.string :format

      t.timestamps
    end
  end
end
