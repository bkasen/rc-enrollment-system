class AddIndicesToPostalCodes < ActiveRecord::Migration
  def change
    add_index :postal_codes, :state_id
    add_index :postal_codes, :country_id
  end
end
