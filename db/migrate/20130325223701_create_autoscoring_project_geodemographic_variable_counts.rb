class CreateAutoscoringProjectGeodemographicVariableCounts < ActiveRecord::Migration
  def up
    create_table :autoscoring_project_geodemographic_variable_counts do |t|
      t.integer :autoscoring_project_geodemographic_variable_id
      t.string :name
      t.integer :considered
      t.integer :total

      t.timestamps
    end

    add_index :autoscoring_project_geodemographic_variable_counts, :autoscoring_project_geodemographic_variable_id, :name => 'idx_autoscore_prjct_geo_var_cnts_vrble_id'

  end
  
  def down
    remove_table :autoscoring_project_geodemographic_variable_counts
  end
end
