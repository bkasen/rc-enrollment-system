class ChangeConsideredColumnToEnrolledColumn < ActiveRecord::Migration
  def up
    rename_column :autoscoring_project_other_variable_counts, :considered, :enrolled
    rename_column :autoscoring_project_geodemographic_variable_counts, :considered, :enrolled
  end

  def down
    rename_column :autoscoring_project_other_variable_counts, :enrolled, :considered
    rename_column :autoscoring_project_geodemographic_variable_counts, :enrolled, :considered
  end
end
