require 'csv'

# puts "Importing countries..."
# CSV.foreach(Rails.root.join("seed_data/countries.csv"), headers: true) do |row|
#   Country.create! do |country|
#     country.id = row[0]
#     country.name = row[1]
#   end
# end

# puts "Importing states..."
# CSV.foreach(Rails.root.join("seed_data/states.csv"), headers: true) do |row|
#   State.create! do |state|
#     state.name = row[0]
#     state.country_id = row[2]
#   end
# end
#
puts "Importing postal_codes..."
CSV.foreach(Rails.root.join("seed_data/postal_codes.csv"), headers: true) do |row|
  PostalCode.create! do |postal_code|
    # transformed values
    postal_code_string = row[0].to_i.to_s # convert to integer to remove leading zeros before appending appropriate zeros
    t_postal_code = sprintf '%05d', postal_code_string # append leading zeros if not present
    postal_code.name = t_postal_code
    @state = State.find_by_name(row[1])
    postal_code.state_id = @state.id
    postal_code.country_id = @state.country_id
  end
end

# puts "Importing institutions..."
# CSV.foreach(Rails.root.join("seed_data/institutions.csv"), headers:true) do |row|
#   institution =  Institution.find_by_unitid(row[0])
#   if institution.nil? then
#     institution = Institution.new()
#   end
#   institution.unitid = row[0]
#   institution.name = row[1]
#   institution.city = row[2]
#   @state = State.find_by_abbreviation(row[3])
#   institution.state_id = @state.id unless @state.nil?
#   postal_code = sprintf '%05d', row[4][0,4]
#   @postal_code = PostalCode.where("name = ? AND state_id = ?", postal_code, @state.id).first_or_create(:name => postal_code, :state_id => @state.id, :country_id => @state.country_id)
#   institution.postal_code_id = @postal_code.id
#   institution.country_id = @state.country_id
#   institution.display_name = row[5]
#   institution.save
# end
#
#
# puts "Importing terms..."
# Term.delete_all
# CSV.foreach(Rails.root.join("seed_data/terms.csv"), headers:true) do |row|
#   @session = Session.find_or_create_by_name(row[0])
#   @year = Year.find_or_create_by_name_and_value(row[1], row[1])
#
#   term =  Term.find_by_session_id_and_year_id(@session.id, @year.id)
#   if term.nil? then
#     term = Term.new()
#   end
#   term.session = @session
#   term.year = @year
#   term.save
#
# end


# puts "Importing years..."
# CSV.foreach(Rails.root.join("seed_data/years.csv"), headers: true) do |row|
#   Year.create! do |year|
#     year.name = row[0]
#     year.value = row[0]
#   end
# end

# puts "Importing geodemographic_variables..."
# CSV.foreach(Rails.root.join("seed_data/geodemographic_variables.csv"), headers: true) do |row|
#   geodemographic_variable =  GeodemographicVariable.find_by_name(row[0])
#   if geodemographic_variable.nil? then
#     geodemographic_variable = GeodemographicVariable.new()
#   end
#   geodemographic_variable.name = row[0]
#   @variable = Variable.find_by_name(row[1])
#   geodemographic_variable.variable_id = @variable.id
#   geodemographic_variable.save
#
# end

# puts "Importing counties..."
# CSV.foreach(Rails.root.join("seed_data/counties.csv"), headers: true) do |row|
#   County.create! do |county|
#     @state = State.find_by_name(row[0])
#     county.name = row[1]
#     county.state_id =  @state.id
#     county.country_id = @state.country_id
#   end
# end
