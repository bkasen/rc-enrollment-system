class InitiateScoringJobCleanupWorker
  include Sidekiq::Worker
  if !Rails.env.development?
    sidekiq_options :queue => "scoring_job_record_removal"
  end
  sidekiq_options :retry => 1
  sidekiq_options :backtrace => true

  # sidekiq_retries_exhausted do |msg|
  #   autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(msg['args'].to_i)
  #   if autoscoring_project_scoring_job
  #     autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 4) # Completed With Errors
  #     ScoringJobMailer.scoring_job_completed_with_error(autoscoring_project_scoring_job_id).deliver
  #   end
  #   logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  # end

  def perform(autoscoring_project_scoring_job_id)
    AutoscoringProjectScoringJobRecord.select(:id).where('autoscoring_project_scoring_job_id = ?', autoscoring_project_scoring_job_id).find_each do |scoring_job_record|
        DeleteScoringJobRecordWorker.perform_async(scoring_job_record.id)
    end
  end
end