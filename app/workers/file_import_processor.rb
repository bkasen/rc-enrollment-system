class FileImportProcessor
  @queue = :file_imports_processer_queue

  def self.perform(file_import_id)
    begin
      file_import = FileImport.find(file_import_id)

      if file_import.import
        FileImportMailer.file_import_completed_successfully(file_import_id, file_import.admin_user_id).deliver
        file_import.update_column(:processed, true) # Completed With Errors
      else
        # send error email
        if FileImportMailer.file_import_completed_with_error(file_import_id, file_import.admin_user_id, file_import.error_details).deliver
          file_import.update_column(:processed, false) # Completed With Errors
        end
      end
      
    rescue => e
      ExceptionNotifier.notify_exception(e)
    end
  end

end
