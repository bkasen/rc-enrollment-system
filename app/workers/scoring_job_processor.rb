class ScoringJobProcessor
  @queue = :scoring_jobs_processer_queue

  def self.perform(autoscoring_project_scoring_job_id)
    begin
      autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(autoscoring_project_scoring_job_id)
      autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 2) # Currently Processing
      autoscoring_project_scoring_job.touch
      autoscoring_project_scoring_job.start_time = Time.now

      if autoscoring_project_scoring_job.process
        if autoscoring_project_scoring_job.create_scored_csv_output_file
          autoscoring_project_scoring_job.finish_time = Time.now
          if autoscoring_project_scoring_job.save
            # scoring & csv creation completed successfully
            autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 3) # Completed Successfully
            autoscoring_project_scoring_job.touch
            # send completed email
            ScoringJobMailer.scoring_job_completed_successfully(autoscoring_project_scoring_job_id).deliver
          else
            # send error email
            if ScoringJobMailer.scoring_job_completed_with_error(autoscoring_project_scoring_job_id).deliver
              autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 4) # Completed With Errors
              autoscoring_project_scoring_job.touch
            end
          end

        else
          #error creating scored csv file
          autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 4) # Completed With Errors
          autoscoring_project_scoring_job.touch
          ScoringJobMailer.scoring_job_completed_with_error(autoscoring_project_scoring_job_id).deliver
          ScoringJobMailer.scoring_job_missing_id_columns_error(self.id).deliver if autoscoring_project_scoring_job.missing_id_column
        end
      else
        #error scoring file
        # send error email
        autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 4) # Completed With Errors
        autoscoring_project_scoring_job.touch
        ScoringJobMailer.scoring_job_completed_with_error(autoscoring_project_scoring_job_id).deliver
        ScoringJobMailer.scoring_job_missing_id_columns_error(self.id).deliver if autoscoring_project_scoring_job.missing_id_column

      end
      # Run Garbage Collector to try & clear memory from scoring
      # GC.start
    rescue => e
      ExceptionNotifier.notify_exception(e)
    end
  end
end
