//= require active_admin/base
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery_nested_form

window.NestedFormEvents.prototype.insertFields = function(content, assoc, link) {
  var $tr = $(link).closest('tr');
  return $(content).insertBefore($tr);
}