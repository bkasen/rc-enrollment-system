class AutoscoringProjectJobStatus < ActiveRecord::Base
  attr_accessible :name

  validates :name, :presence => true

  has_many :autoscoring_project_scoring_job_logs
  has_many :autoscoring_project_scoring_jobs

end
