class AutoscoringProjectScoringJob < ActiveRecord::Base
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  attr_accessible :autoscoring_project_id, :autoscoring_project_job_status_id, :has_file_dimension_counts, :column_count, :conversion_criteria, :details, :filter_criteria, :row_count, :rows_filtered_out, :split_output_by_column_name, :user_id, :autoscoring_project_scoring_job_input_files_attributes, :start_time, :finish_time, :number_of_attempts, :cohort_year_id, :is_test_scoring, :is_public, :rows_processed

  attr_accessor :encountered_error, :processed, :processing_error_details, :scoring_error_details, :processing_details, :other_variable_values_using_default_yields, :state_postal_codes_using_default_yields, :opportunity_yield_rates, :missing_id_column
  # order of scoring jobs is set to ASC so the last file is always the most recent version when last method is called
  # default_scope order('created_at ASC')

  belongs_to :autoscoring_project
  belongs_to :autoscoring_project_job_status
  belongs_to :cohort_year, :class_name => "Year"
  belongs_to :user
  has_one :institution, :through => :autoscoring_project

  has_many :autoscoring_project_scoring_job_input_files, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_scoring_job_input_files, allow_destroy: true

  has_many :autoscoring_project_scoring_job_output_files, :dependent => :destroy
  has_many :autoscoring_project_scoring_job_logs, :dependent => :delete_all
  has_many :autoscoring_project_scoring_job_records, :dependent => :delete_all

  validates :autoscoring_project_id, :presence => true
  validates :autoscoring_project_job_status_id, :presence =>true
  validates :user_id, :presence => true
  validates :details, :presence => true
  # validates :column_count, :numericality => true, :presence => true, :if => :file_counts_required?
  # validates :row_count, :numericality => true, :presence => true, :if => :file_counts_required?
  validates :rows_filtered_out, :numericality => true, :allow_nil => true

  after_save :update_autoscoring_project_scoring_details_if_completed

  # before_destroy :delete_all_scoring_job_records, :delete_all_scoring_job_logs

  def autoscoring_project_scoring_job_input_file
    self.autoscoring_project_scoring_job_input_files.order('created_at DESC').first
  end

  def cohort_year_string
    self.cohort_year.present? ? "#{self.cohort_year.name}_cohort" : ""
  end

  def number_of_attempts_count
    # Rails.cache.fetch([self, "number_of_attempts_count", updated_at.to_s(:number)]) { number_of_attempts.to_s }
    number_of_attempts.to_s
  end

  def other_variable_code_yield_cache
    @other_variable_code_yield_cache ||= ScoringOtherVariableCodeYieldCache.new
  end

  def percentile_rank_cache
    @percentile_rank_cache ||= ScoringPercentileRankCache.new
  end

  def transformed_postal_code_value_cache
    # global to entire class not just scoring job instance
    @transformed_postal_code_value_cache ||= ScoringTransformedPostalCodeValueCache.new
  end

  def contact_date_conversion_cache
    @contact_date_conversion_cache ||= ScoringAutoscoringProjectContactDateConversionCache.new
  end

  def ceeb_code_cache
    # global to entire class not just scoring job instance
    @ceeb_code_cache ||= ScoringCeebCodeCache.new
  end

  def autoscoring_project_other_variable_cache
    @autoscoring_project_other_variable_cache ||= ScoringAutoscoringProjectOtherVariableCache.new
  end

  def autoscoring_project_other_variable_count_cache
    @autoscoring_project_other_variable_count_cache ||= ScoringAutoscoringProjectOtherVariableCountCache.new
  end

  def autoscoring_project_geodemographic_variable_count_cache
    @autoscoring_project_geodemographic_variable_count_cache ||= ScoringAutoscoringProjectGeodemographicVariableCountCache.new
  end

  def geodemographic_variable_code_name_cache
    @geodemographic_variable_code_name_cache ||= ScoringGeodemographicVariableCodeNameCache.new
  end

  def state_cache
    @state_cache ||= ScoringStateCache.new
  end

  def postal_code_state_cache
    @postal_code_state_cache ||= ScoringPostalCodeStateCache.new
  end

  def postal_codes_year_cache
    @postal_codes_year_cache ||= ScoringPostalCodesYearCache.new
  end

  def autoscoring_project_other_variable_translation_cache
    @autoscoring_project_other_variable_translation_cache ||= ScoringAutoscoringProjectOtherVariableTranslationCache.new
  end

  def institution_name
    autoscoring_project.institution.try(:name)
  end

  def institution_name=(name)
    if name.present? && !name.nil?
      self.autoscoring_project.institution = Institution.find_by_name(name)
    else
      self.autoscoring_project.institution = nil
    end
  end

  # def scoring_job_autoscoring_project_cache
  #   @scoring_job_autoscoring_project_cache ||= ScoringScoringJobAutoscoringProjectCache.new
  # end

  def rank_from_percentile_cache
    @rank_from_percentile_cache ||= ScoringRankFromPercentileCache.new
  end

  def create_scored_csv_output_file
    self.processing_error_details = Array.new
    begin
      autoscoring_project_geodemographic_variables = get_autoscoring_project_geodemographic_variables
      now = DateTime.now

      # Iterate through Scoring Job's records by their sub_group to split records into separate files if there are valid sub_group terms to split based on
      sub_groups =  self.autoscoring_project_scoring_job_records.count(:all, :group => "sub_group")

      # Define fields array which is the header row for the CSV file
      if scoring_job_autoscoring_project.is_em_configured?
        fields = [scoring_job_autoscoring_project.em_person_id_column_name, scoring_job_autoscoring_project.em_opportunity_id_column_name, "RANK", "PCNT", "PROB"]
      else
        fields = [scoring_job_autoscoring_project.client_opportunity_id_column_name, "RANK", "PCNT", "PROB"]
      end
      # Iterate through all geodemographic variables and add a column header for it
      autoscoring_project_geodemographic_variables.each do |geodemographic_variable|
        fields << geodemographic_variable.variable.abbreviation.upcase
      end

      sub_groups.each do |sub_group|

        sub_group_name = sub_group[0]
        autoscoring_project_scoring_job_output_file = AutoscoringProjectScoringJobOutputFile.find_or_create_by_autoscoring_project_scoring_job_id_and_split_value(self.id, sub_group_name)

        if sub_groups.size > 1
          # Sub Groups Specified
          file_name = "#{self.id}_#{scoring_job_autoscoring_project.name.underscore}_project_#{self.cohort_year_string}_scoring_job_subgroup[sub_group-#{sub_group_name}]_#{now.strftime('%m %d %Y %H %M %S').to_s}.csv".downcase.tr(' ', '_')
        else
          # No Sub Groups Specified - All Records will be in one file
          file_name = "#{self.id}_#{scoring_job_autoscoring_project.name.underscore}_project_#{self.cohort_year_string}_scoring_job_#{now.strftime('%m %d %Y %H %M %S').to_s}.csv".downcase.tr(' ', '_')
        end

        file_name = file_name.gsub(/[\x00\/\\:\*\?\"<>\|]/, '_')

        directory_name = "#{Rails.root}/tmp/files"
        Dir.mkdir(directory_name) unless File.exists?(directory_name)
        temp_path = "#{Rails.root}/tmp/files/#{file_name}"

        is_em_configured = scoring_job_autoscoring_project.is_em_configured?

        CSV.open(temp_path, "wb") do |csv|

          # Append fields as first row to CSV
          csv << fields
          # Not using below code because it would query all records which may instantiate too many records at once so instead we process in batches using find_each
          # scored_records = AutoscoringProjectScoringJobRecord.includes(:postal_code).find_all_by_autoscoring_project_scoring_job_id_and_sub_group(self.id, sub_group_name)
          # Iterate through all scored records in chunks of 1000 for query *performance*

          AutoscoringProjectScoringJobRecord.includes(:postal_code).select("id, client_opportunity_id, em_person_id, em_opportunity_id, rank, percentile_rank, probability_score, postal_code_id").where(:autoscoring_project_scoring_job_id => self.id, :sub_group => sub_group_name).find_each do |scored_record|

            if is_em_configured
              record = [scored_record.em_person_id, scored_record.em_opportunity_id, scored_record.rank, scored_record.percentile_rank, scored_record.probability_score]
            else
              record = [scored_record.client_opportunity_id, scored_record.rank, scored_record.percentile_rank, scored_record.probability_score]
            end

            # Get GeoDemo Characteristics for record's postal code based on the autoscoring project's geoyear
            if scored_record.postal_code.nil?
              autoscoring_project_geodemographic_variables.each do |geodemographic_variable|
                record <<  ""
              end
            else
              postal_codes_year = get_postal_codes_year(scored_record.postal_code_id, scoring_job_autoscoring_project.geodemographic_year_id)
              # unless postal_codes_year = scoring_dbf_postal_codes_years_cache.get(scored_record.postal_code.id.to_s + autoscoring_project.geodemographic_year.id.to_s)
              #   postal_codes_year = PostalCodesYear.includes(:hvl, :mfi, :mhb, :eth, :pop, :mdg, :adg).find_by_postal_code_id_and_year_id(scored_record.postal_code.id, autoscoring_project.geodemographic_year.id)
              #   scoring_dbf_postal_codes_years_cache.put(scored_record.postal_code.id.to_s + autoscoring_project.geodemographic_year.id.to_s, postal_codes_year)
              # end


              if postal_codes_year.nil?
                autoscoring_project_geodemographic_variables.each do |geodemographic_variable|
                  record <<  ""
                end
              else
                # Only try to add geodemographic data if record has a valid posta_codes_year to process
                autoscoring_project_geodemographic_variables.each do |geodemographic_variable|
                  # Only include those GEO Variables data in output file which have been set to be included in Output
                  case geodemographic_variable.variable_id
                  when 1 # HVL
                    value = postal_codes_year.hvl.name
                  when 2 # MFI
                    value = postal_codes_year.mfi.name
                  when 3 # MHB
                    value = postal_codes_year.mhb.name
                  when 4 # ETH
                    value = postal_codes_year.eth.name
                  when 5 # POP
                    value =postal_codes_year.pop.name
                  when 6 # MDG
                    value = postal_codes_year.mdg.name
                  when 7 # ADG
                    value = postal_codes_year.adg.name
                  else
                    value = ""
                  end
                  geodemographic_variable.include_in_output? ? record << value : record <<  ""
                end
              end
            end
            csv << record
          end
        end

        # Save Output File Record
        autoscoring_project_scoring_job_output_file.file_name = File.open(temp_path)
        autoscoring_project_scoring_job_output_file.split_value = sub_group_name

        if autoscoring_project_scoring_job_output_file.save!
          # delete temp file if saved
          File.delete(temp_path)
        end
      end
      # delete scoring_job_records since they have been successfully added to output file & no longer needing to be housed in db.
      # ActiveRecord::Base.transaction do
      #   self.autoscoring_project_scoring_job_records.delete_all
      # end
      self.delete_all_scoring_job_records
    end
    # Output File(s) created Successfully so we return true
    return true
  rescue => e
    # CSV File could not be created successfully so we delete the scoring job output file record that may have been created
    self.autoscoring_project_scoring_job_output_files.destroy
    self.processing_error_details << " System Error Occurred and Could not Create CSV File for Scoring Job ID##{self.id}. System Admin has been notified and will review the error"
    # update_scoring_job_input_file_processed_status(true)
    self.update_attributes(:autoscoring_project_job_status_id => 4  ) # Did Not Complete Due to Error(s)
    if create_scoring_job_log_record
      ExceptionNotifier::Notifier.background_exception_notification(e).deliver
      return false
    end
  end


  def create_scoring_job_log_record
    begin
      autoscoring_project_scoring_job_log = AutoscoringProjectScoringJobLog.new
      autoscoring_project_scoring_job_log.details = self.processing_details
      autoscoring_project_scoring_job_log.autoscoring_project_scoring_job = self
      autoscoring_project_scoring_job_log.row_count = self.row_count
      autoscoring_project_scoring_job_log.column_count = self.column_count
      autoscoring_project_scoring_job_log.rows_filtered_out = self.rows_filtered_out
      autoscoring_project_scoring_job_log.filter_criteria = scoring_job_autoscoring_project.filter_criteria
      autoscoring_project_scoring_job_log.conversion_criteria = scoring_job_autoscoring_project.conversion_criteria
      autoscoring_project_scoring_job_log.split_output_by_column_name = scoring_job_autoscoring_project.split_output_by_column_name_criteria

      if self.processing_error_details.present?
        processing_error_details = ""
        self.processing_error_details.each do |detail|
          processing_error_details += "- " + detail + "\n"
        end
        autoscoring_project_scoring_job_log.error_details = processing_error_details
        autoscoring_project_scoring_job_log.autoscoring_project_job_status_id = 4 # Did Not Complete Due to Error(s)
      else
        processing_details = "- No errors while processing. Scoring completed successfully!\n\n"
        self.processing_details.each do |detail|
          processing_details += "- " + detail + "\n"
        end
        # Log Other Variable Values whose prospects used default yield rates
        processing_details += "\n\n"

        processing_details += "Other Variables Using Default Yield Rate (Only reporting values present in >= 10 prospect records)\n\n"
        old_key = ""
        self.other_variable_values_using_default_yields.keys.sort.each do |key|
          if key == "" || key[0..key.index('|')-2] != old_key
            # New group of other_variables
            old_key = key[0..key.index('|')-2]
            processing_details += "\n\n"
          end
          if self.other_variable_values_using_default_yields[key] >=10
            processing_details += "- #{key} : #{self.other_variable_values_using_default_yields[key]} record(s) used default yield rate of #{helpers.number_to_percentage(scoring_job_autoscoring_project.default_yield_rate * 100, :precision => 4)}. \n"
          end
        end
        # Log State/Postal Codes whose prospects used default yield rates for geodemographic variables
        processing_details += "\n\n"
        self.state_postal_codes_using_default_yields.keys.sort.each do |key|
          if self.state_postal_codes_using_default_yields[key] >=5
            processing_details += "- #{key} : #{self.state_postal_codes_using_default_yields[key]} record(s) used default yield rate of #{helpers.number_to_percentage(scoring_job_autoscoring_project.default_yield_rate * 100, :precision => 4)} for geodemographic variables in model. \n"
          end
        end
        autoscoring_project_scoring_job_log.details = processing_details
        autoscoring_project_scoring_job_log.autoscoring_project_job_status_id = 3 # Completed Without Errors

      end

      if autoscoring_project_scoring_job_log.save
        self.update_attributes(:number_of_attempts => self.number_of_attempts + 1  )
        return true
      else
        self.scoring_error_details = ""
        autoscoring_project_scoring_job_log.errors.full_messages.each do |message|
          self.scoring_error_details += "System Error - Scoring Job Log Could Not Be Saved. Log Record's #{message}. "
        end
        return false
      end
    rescue => e
      self.update_attributes(:autoscoring_project_job_status_id => 4  ) # Did Not Complete Due to Error(s)
      ExceptionNotifier::Notifier.background_exception_notification(e).deliver
    end
  end

  def completed?
    self.autoscoring_project_job_status_id == 3
  end

  def processing_time_in_seconds
    if (start_time && finish_time)
      "#{(finish_time - start_time).to_s}"
    else
      "No Processing Time Recorded"
    end
  end

  def processing_time_approximate
    (start_time && finish_time) ? helpers.distance_of_time_in_words(start_time, finish_time, true) : "No Processing Time Recorded"
  end

  def processing_time_exact
    if (start_time && finish_time)
      "#{(finish_time - start_time).to_s} seconds "
    else
      "No Processing Time Recorded"
    end
  end


  def encountered_errors?
    self.autoscoring_project_job_status_id == 4
  end

  def file_counts_required?
    !self.new_record?
  end

  def has_multiple_output_files?
    self.autoscoring_project_scoring_job_output_files.size > 1
  end

  def helpers
    ActionController::Base.helpers
  end

  def in_progress?
    self.autoscoring_project_job_status_id == 2
  end

  def in_queue?
    self.autoscoring_project_job_status_id == 5
  end

  def last_autoscoring_project_scoring_job_log
    self.autoscoring_project_scoring_job_logs.last
  end

  def open_spreadsheet
    begin
      # Roo::Spreadsheet.open(self.autoscoring_project_scoring_job_input_file.file_name.url)
      case File.extname(self.autoscoring_project_scoring_job_input_file.file_name.path)
      when ".csv" then  Roo::Spreadsheet.open(self.autoscoring_project_scoring_job_input_file.file_name.url)
      when ".xls" then  Roo::Excel.new(self.autoscoring_project_scoring_job_input_file.file_name.url, nil, :ignore)
      when ".xlsx" then  Roo::Excelx.new(self.autoscoring_project_scoring_job_input_file.file_name.url, nil, :ignore)
      when ".xlsx" then Roo::Spreadsheet.open(self.autoscoring_project_scoring_job_input_file.file_name.url)
      else raise "Unknown file type: #{file_name.path}"
      end
    rescue => e
      self.processing_error_details << " System Error Occurred opening the input file. Please check your file format. If file is a CSV, ensure that the file is saved with the UTF-8 file encoding."
      # update_scoring_job_input_file_processed_status(true)
      self.update_attributes(:autoscoring_project_job_status_id => 4) # Did Not Complete Due to Error(s)
      if create_scoring_job_log_record
        ExceptionNotifier::Notifier.background_exception_notification(e).deliver
        returnf false
      end
    end
  end

  # def encoding_is_utf8?(file_or_string)
  #   require 'iconv'
  #   file_or_string = [file_or_string] if file_or_string.is_a?(String)
  #   is_utf8 = file_or_string.all? { |line| Iconv.conv('UTF-8//IGNORE', 'UTF-8', line) == line }
  #   file_or_string.rewind if file_or_string.respond_to?(:rewind)
  #   is_utf8
  # end

  def process
    # GC::Profiler.enable
    # GC::Profiler.clear

    sniffed_delimiter = ""
    current_row = 0
    self.encountered_error = false
    missing_id_column = false
    split_output_by_column = false
    scored_opportunities_groups = Hash.new
    master_file_header = Array.new
    master_column_keys = Array.new
    scored_opportunities = Array.new
    filter_column_include_values = Hash.new
    filter_column_exclude_values = Hash.new
    self.processing_error_details = Array.new
    self.processing_details = Array.new
    self.other_variable_values_using_default_yields = Hash.new
    self.state_postal_codes_using_default_yields = Hash.new
    self.rows_processed = 0
    self.rows_filtered_out = 0

    # Begin Transaction so we only keep scored records if the scoring process is completes without any errors
    begin

      # Check to make sure we have at least 1 record to process
      if self.row_count == 0
        # No Valid Data so the scoring fails
        self.processing_error_details << "System could not read any valid rows of data in the Input File. Please check to make sure the Input File has valid records for each row."
        update_scoring_job_input_file_processed_status(true)
        self.autoscoring_project_job_status_id = 4 # Did Not Complete Due to Error(s)
        if create_scoring_job_log_record
          return false
        end
      end

      # Open Spreadsheet
      # spreadsheet = open_spreadsheet

      # Setup master_file_header array with hashes of each column required for scoring. We uppercase all column names prior to adding into hash for case in-sensitive comparisons later on

      if scoring_job_autoscoring_project.is_em_configured?
        master_file_header.push({:primary_column_name => scoring_job_autoscoring_project.em_person_id_column_name.upcase, :is_id_column => true})
        master_file_header.push({:primary_column_name => scoring_job_autoscoring_project.em_opportunity_id_column_name.upcase, :is_id_column => true})
      else
        master_file_header.push({:primary_column_name =>  scoring_job_autoscoring_project.client_opportunity_id_column_name.upcase, :is_id_column => true})
      end

      master_file_header.push({:primary_column_name => scoring_job_autoscoring_project.postal_code_column_name.upcase})
      master_file_header.push({:primary_column_name => scoring_job_autoscoring_project.state_column_name.upcase})

      if scoring_job_autoscoring_project.split_output_by_column_name.present?
        split_output_by_column = true
        master_file_header.push({:primary_column_name => scoring_job_autoscoring_project.split_output_by_column_name.upcase})
      end

      scoring_job_autoscoring_project.other_variables_in_client_input_file.each do |other_variable|
        master_file_header.push({:primary_column_name => other_variable.name.upcase, :secondary_column_name => other_variable.secondary_column_name.upcase, :tertiary_column_name => other_variable.tertiary_column_name.upcase})
      end

      # Append any Conversion Columns to the required Fields
      scoring_job_autoscoring_project.autoscoring_project_conversion_columns.each do |conversion_column|
        master_file_header.push({:primary_column_name => conversion_column.name.upcase})
        conversion_column.autoscoring_project_conversion_values.each do |conversion_value|
          self.processing_details << "System converted the column #{conversion_column.name.upcase} value '#{conversion_value.from_value}' into '#{conversion_value.to_value}'."
        end
      end

      # Append any Filter Columns to the required Fields
      if scoring_job_autoscoring_project.autoscoring_project_filter_columns.present?
        scoring_job_autoscoring_project.autoscoring_project_filter_columns.each do |filter_column|
          if filter_column.filter_to_include_these_values?
            filter_method = "include"
          else
            filter_method = "exclude"
          end
          master_file_header.push({:primary_column_name => filter_column.name.upcase})
          filter_column.autoscoring_project_filter_values.each do |filter_value|
            self.processing_details << "System filtered the column #{filter_column.name.upcase} to #{filter_method} records with the value: '#{filter_value.name}'."
          end
        end

        scoring_job_autoscoring_project.autoscoring_project_filter_columns.each do |filter_column|
          # Add new filter column & values array to the filter_column_include_values hash to use for filtering
          if filter_column.filter_to_include_these_values?
            filter_column_include_values.merge!({filter_column.name.upcase => Hash[filter_column.autoscoring_project_filter_values.map(&:name).map.with_index.to_a]})
          else
            filter_column_exclude_values.merge!({filter_column.name.upcase => Hash[filter_column.autoscoring_project_filter_values.map(&:name).map.with_index.to_a]})
          end
        end
      end

      input_file = self.autoscoring_project_scoring_job_input_file.file_name

      # Store the file locally in the tmp file, Cronjob will clean it up later on
      # input_file.retrieve_from_store!(File.basename(input_file.url))
      input_file.cache_stored_file!
      # Check if file's first row is UTF-8 Encoded before we attempt to process file.
      begin
        CSV.foreach(input_file.path, :encoding => 'utf-8') do |row|
          break # read first line & if UTF-8 encoding is acceptable then we proceed assuming rest of file is UTF-8 acceptable
          end
        rescue ArgumentError
          self.processing_error_details << "First Row in Input File suggests the CSV Encoding is not UTF-8. Please re-save CSV file with UTF-8 encoding & re-upload to queue. \n"
          self.encountered_error = true
        end
        # Check if file is either comma or tab delimited file.
        if !encountered_error
          # only proceed to sniff delimiter if encoding is acceptable
          begin
            sniffed_delimiter = sniff(input_file.path)
            CSV.foreach(input_file.path,{:col_sep =>  sniffed_delimiter}) do |row|
              break # read first line so column separater is acceptable
            end
            self.processing_details << "Input File's Column Separator Character identified & used is '#{sniffed_delimiter}'."
          rescue => e
            self.processing_error_details << "Input File CSV column separater character is not valid. Please ensure CSV file saved with either comma or tab separated file. Please re-save & re-upload to queue. \n"
            self.encountered_error = true
          end
        end

        # Only continue processing if file is UTF-8 encoded.
        if !encountered_error
          # ActiveRecord::Base.transaction do
          autoscoring_project_scoring_job_records = Array.new
          current_row = 1
          input_file_header = Array.new
          master_column_keys = Array.new

          # (2..spreadsheet.last_row).map do |i|
          ActiveRecord::Base.uncached do
            CSV.foreach(input_file.path, {:col_sep => sniffed_delimiter}) do |input_row|
              if current_row == 1 # maybe could use FasterCSV is_header_row? method????

                # Get array of column headers from input file. We upper case all values for case in-sensitive column header value presence validations
                input_file_header = input_row

                # encoding: utf-8
                input_file_header.map!{|e| e ? e.remove_non_ascii : ""} # replace any possible nil column headers to empty string
                input_file_header.map!(&:upcase)

                # Iterate through master_file_header to ensure input_file_header contains all the required column headers required for scoring
                master_file_header.each do |master_column_header|
                  master_column_keys << master_column_header[:primary_column_name] if !master_column_header[:primary_column_name].blank?
                  master_column_keys << master_column_header[:secondary_column_name] if !master_column_header[:secondary_column_name].blank?
                  master_column_keys << master_column_header[:tertiary_column_name] if !master_column_header[:tertiary_column_name].blank?

                  if !input_file_header.include?(master_column_header[:primary_column_name])
                    # input_file_header missing column_header primary name
                    column_header_value = master_column_header[:primary_column_name]
                    self.encountered_error = true
                    if master_column_header[:is_id_column]
                      unless missing_id_column
                        missing_id_column = true
                        if scoring_job_autoscoring_project.is_em_configured?
                          self.processing_error_details << "System cannot find the required '#{scoring_job_autoscoring_project.em_person_id_column_name}' and/or '#{scoring_job_autoscoring_project.em_opportunity_id_column_name}' columns in the Input File. \n"
                        else
                          self.processing_error_details << "System cannot find the required '#{scoring_job_autoscoring_project.client_opportunity_id_column_name}' column in the Input File. \n"
                        end
                      end
                    else
                      self.processing_error_details << "System cannot find the required column '#{column_header_value}' in the Input File."
                    end
                  end
                  # Check if input file is missing the secondary column name & fail if case is true
                  if !master_column_header[:secondary_column_name].blank? && !input_file_header.include?(master_column_header[:secondary_column_name])
                    # master_file_header requires a secondary column name & input_file_header missing column_header secondary column name
                    column_header_value = master_column_header[:secondary_column_name]
                    self.encountered_error = true
                    self.processing_error_details << "System cannot find the required column '#{column_header_value}' in the Input File. \n"
                  end
                  # Check if input file is missing the tertiary column name & fail if case is true
                  if !master_column_header[:tertiary_column_name].blank? && !input_file_header.include?(master_column_header[:tertiary_column_name])
                    # master_file_header requires a secondary column name & input_file_header missing column_header secondary column name
                    column_header_value = master_column_header[:tertiary_column_name]
                    self.encountered_error = true
                    self.processing_error_details << "System cannot find the required column '#{column_header_value}' in the Input File. \n"
                  end

                end

                break if self.encountered_error

              else
                # Non-Header Row which is a prospect record row
                include_row = false
                exclude_row = false

                self.update_column(:rows_processed,  current_row - 1)

                # Extracts relevent hash key/value pairs from row to reduce the size of row to just the columns of data we need.
                # row = Hash[[input_file_header, input_row].transpose].extract!(*master_column_keys) # original hash reduction approach which caused some nil values once the extract method was applied
                row = Hash[[input_file_header, input_row].transpose].slice(*master_column_keys) # new hash reduction method which rejects any columns not in the master_column_keys array

                # Check if AutoScoring Project has any Conversions configured and apply conversions prior to any filtering or scoring
                if scoring_job_autoscoring_project.autoscoring_project_conversion_columns.present?
                  scoring_job_autoscoring_project.autoscoring_project_conversion_columns.each do |conversion_column|
                    column_name = conversion_column.name.upcase
                    conversion_column.autoscoring_project_conversion_values.each do |conversion_value|
                      if row[column_name] == conversion_value.from_value
                        # Convert value in the row hash to be the conversion value's to_value
                        row[column_name] = conversion_value.to_value
                        self.processing_details << "System converted the column #{conversion_column.name.pluralize} value #{conversion_value.from_value} into #{conversion_value.to_value}. \n"
                      end
                    end
                  end
                end

                # Check if row matches filter criteria (if specified) to know if we should skip this record or score it
                if filter_column_include_values.any?
                  filter_column_include_values.each do |key, include_values|
                    include_row  = include_values.has_key?(row[key].blank? ? "" : row[key]) # if row[key] is nil we treat as empty string to include empty string records if it's a filter value to include. check if the value is not in the row hash
                    break if !include_row # This row should be skipped so we break the loop early to stop processing any additional filter columns since it's already going to be omitted
                  end
                else
                  # No include filter criteria specified so we plan to include this row & continue
                  include_row = true
                end

                if include_row && filter_column_exclude_values.any?
                  # continue checking this row to determine if include_row is still true & determine if we include it based on the defined exclude filters
                  filter_column_exclude_values.each do |key, exclude_values|
                    exclude_row  = exclude_values.has_key?(row[key].blank? ? "" : row[key]) # if row[key] is nil we treat as empty string to include empty string records if it's a filter value to include. check if the value is not in the row hash
                    break if exclude_row == true # This row should be skipped so we break the loop early to stop processing any additional filter columns since it's already going to be omitted
                  end
                include_row = !exclude_row # set to the opposite of exclude so we know to exclude when it meets meets an exclude *criteria*
                end

                # Check if we should include this row and continue based on our inclusion/exclusion criteria
                if include_row
                  # Valid Record to Score so begin scoring the current row's prospect
                  if scoring_job_autoscoring_project.is_em_configured?
                    client_opportunity_id = nil
                    em_opportunity_id = get_em_formatted_id_string(row["EMOPPORTUNITYID"])
                    em_person_id = get_em_formatted_id_string(row["EMPERSONID"])
                  else
                    client_opportunity_id = row[scoring_job_autoscoring_project.client_opportunity_id_column_name.upcase]
                    em_opportunity_id = nil
                    em_person_id = nil
                  end

                  if split_output_by_column
                    split_output_by_column_value = row[scoring_job_autoscoring_project.split_output_by_column_name.upcase].to_s
                  else
                    split_output_by_column_value = "SINGLE OUTPUT"
                  end

                  # Get Input File Postal Code Value, Transform to 5 digit Postal Code, and Lookup US Postal Code
                  state_abbreviation = row[scoring_job_autoscoring_project.state_column_name.upcase].to_s

                  if is_state_value_a_valid_usa_state(state_abbreviation)
                    postal_code_name = get_transformed_us_five_digit_postal_code_name(row[scoring_job_autoscoring_project.postal_code_column_name.upcase].to_s)
                    # get postal_code_id from postal_code_id_cache
                    postal_code = get_postal_code(postal_code_name)
                    postal_code.nil? ? postal_code_id = nil : postal_code_id = postal_code.id
                  else
                    postal_code_name = ""
                    postal_code_id = nil
                    # Add state abbreviation & postal code value pair to hash to know how many combinations did not match up for monitor logs
                    if row[scoring_job_autoscoring_project.postal_code_column_name.upcase].to_s != ""
                      # Add entry to hash of variable/values that used the default yield rate
                      hash_key = "State(#{row[scoring_job_autoscoring_project.state_column_name.upcase].to_s })/Postal Code(#{row[scoring_job_autoscoring_project.postal_code_column_name.upcase].to_s})"
                      if self.state_postal_codes_using_default_yields.has_key?(hash_key)
                        self.state_postal_codes_using_default_yields[hash_key] += 1
                      else
                        # Store new entry into hash
                        self.state_postal_codes_using_default_yields.merge!(hash_key => 1)
                      end
                    end
                  end

                  probability_score = get_probability_score(row, postal_code_name, scoring_job_autoscoring_project.geodemographic_year_id)
                  # Create hash of opportunity we just scored & append to the array of all scored opportunity hashes
                  scored_opportunity = { :sub_group => split_output_by_column_value, :client_opportunity_id => client_opportunity_id, :em_person_id => em_person_id, :em_opportunity_id => em_opportunity_id, :postal_code_id => postal_code_id, :probability_score => probability_score, :percentile_rank => "", :rank => ""}

                  # Merge scored opportunity hash into Hash of all Scored Groups
                  if scored_opportunities_groups.has_key?(split_output_by_column_value)
                    scored_opportunities_groups[split_output_by_column_value] << scored_opportunity
                  else
                    # Store new sub group into hash
                    new_scored_opportunities_group = [scored_opportunity]
                    scored_opportunities_groups.merge!(split_output_by_column_value => new_scored_opportunities_group)
                  end

                else
                  #  Record is filtered out based on filter criteria.
                  self.rows_filtered_out += 1
                end

                if self.encountered_error
                  break
                end
              end
              current_row +=1
            end
          end
        end

        if self.encountered_error
          update_scoring_job_input_file_processed_status(true)
          self.update_attributes(:row_count => nil, :column_count => nil, :autoscoring_project_job_status_id => 4) # Did Not Complete Due to Error(s)
          if create_scoring_job_log_record
            return false
          end
        end



        # Get row & column Counts for Scoring Job after we have processed the file
        if !self.has_file_dimension_counts
          self.row_count = current_row - 2 # not considers header plus the last +=1 we do in the loop
            self.column_count = input_file_header.size
            self.has_file_dimension_counts = true
          end

          # Iterate through scored_opportunities group hash
          ratistics = Ratistics

          scored_opportunities_groups.each do |scored_opportunities_group|
            scored_opportunities_sub_group = scored_opportunities_group.last
            # Calculate Percentile Ranks for Records to Include
            # Sort Array of Opportunity hashes by the probability score value ordered by probability score descending
            scored_opportunities_sub_group.sort_by!{ |scored_opportunity| scored_opportunity[:probability_score] }
            # Map! single column array of scores to reference for quicker lookup than mapping each time.
            
            sorted_scores = scored_opportunities_sub_group.map{|opportunity| opportunity[:probability_score]}
            scored_opportunities_sub_group.each_with_index do |scored_opportunity, idx|
              # Calculate & Set percentile_rank and alpha rank
              # Get Percentile Rank by passing in the array's probability_score hashed values
                scored_opportunity[:percentile_rank] = "%.3f" % ratistics.percent_rank(sorted_scores, idx+1, {:sorted => true})

              unless scored_opportunity[:rank] = self.rank_from_percentile_cache.get(scored_opportunity[:percentile_rank])
                scored_opportunity[:rank] = get_rank_from_percentile_rank(scored_opportunity[:percentile_rank])
                self.rank_from_percentile_cache.put(scored_opportunity[:percentile_rank], scored_opportunity[:rank])
              end
              # Create autoscoring_project_scoring_job_record of prospect & append to array of prospects to save
              autoscoring_project_scoring_job_records << AutoscoringProjectScoringJobRecord.new(:autoscoring_project_scoring_job_id => id, :client_opportunity_id => scored_opportunity[:client_opportunity_id], :em_opportunity_id => scored_opportunity[:em_opportunity_id], :em_person_id => scored_opportunity[:em_person_id], :postal_code_id => scored_opportunity[:postal_code_id], :probability_score => scored_opportunity[:probability_score], :percentile_rank => scored_opportunity[:percentile_rank], :rank => scored_opportunity[:rank], :sub_group => scored_opportunity[:sub_group])
            end
          end

          # Import Scored Records 1000 at a time into the Scoring Job Table for Historical Record
          ActiveRecord::Base.transaction do
            autoscoring_project_scoring_job_records.each_slice(1000) do |slice|
              AutoscoringProjectScoringJobRecord.import slice
            end
          end

          # GC::Profiler.report

          # Scoring completed successfully
          update_scoring_job_input_file_processed_status(true)
          if create_scoring_job_log_record
            self.autoscoring_project_job_status_id = 3 # Completed
            return true
          else
            update_scoring_job_input_file_processed_status(true)
            self.autoscoring_project_job_status_id = 4 #  Did Not Complete Due to Error(s)

            return false
          end
          # end
        rescue => e
          if e.class == CSV::MalformedCSVError
            self.processing_error_details << "CSV File is not formed correctly. Error Details (#{e.class}) #{e.message.inspect}. Please Check your input file to ensure it is formatted correctly. Note which line that is causing issues and also check for similar cases elsewhere in the file."
          elsif e.class == ArgumentError && e.message == "invalid byte sequence in UTF-8"
            self.processing_error_details << "Input File CSV Encoding is not properly UTF-8 encoded. Please re-save CSV file with UTF-8 encoding & re-upload to queue. \n"
          elsif e.class == IndexError
            self.processing_error_details << "CSV File is not formed correctly. Expected row value count differs on line #{current_row}. Please check to ensure you have correct number of comma separator values notating both null & non-null values on that row. Error Details #{e.message.inspect}. \n"
          else
            self.processing_error_details << "System Error Occurred and Could not Complete Scoring Process at line #{current_row}. System Admin has been notified and will review the error"
            # update_scoring_job_input_file_processed_status(true)
            ExceptionNotifier::Notifier.background_exception_notification(e).deliver
          end
          if create_scoring_job_log_record
            self.update_attributes(:autoscoring_project_job_status_id => 4) # Did Not Complete Due to Error(s)
            return false
          end
        end
      end

      def processed?
        self.autoscoring_project_scoring_job_input_file.processed
      end

      def not_started?
        self.autoscoring_project_job_status_id == 1
      end

      def row_inclusion_percentage
        ( (row_count.to_f - rows_filtered_out) / row_count )  * 100 if row_count
      end

      def update_scoring_job_input_file_processed_status(status)
        autoscoring_project_scoring_job_input_file = AutoscoringProjectScoringJobInputFile.find(self.autoscoring_project_scoring_job_input_file)
        autoscoring_project_scoring_job_input_file.update_attributes(:processed => status)
      end

      def status_class
        case self.autoscoring_project_job_status_id
        when 1
          ""
        when 2
          "warning"
        when 3
          "success"
        when 4
          "error"
        else
          ""
        end
      end

      def self.to_csv(options = {})

        CSV.generate(options) do |csv|
          csv << ["Scoring Job ID", "Institution", "Auto-Scoring Project", "Auto-Scoring Project Configuration", "Job Status", "Processing Time (Seconds)", "Is Test Scoring", "Is Public", "Input File Row Count", "Rows Filtered Out", "Number of Attempts", "User", "# of Output Files", "Creator Details", "Created At", "Updated At"]
          all.each do |scoring_job|
            csv <<  [scoring_job.id, scoring_job.autoscoring_project.institution.name, scoring_job.autoscoring_project.name, scoring_job.autoscoring_project.configuration_type, scoring_job.autoscoring_project_job_status.name, scoring_job.processing_time_in_seconds, scoring_job.is_test_scoring, scoring_job.is_public, scoring_job.row_count, scoring_job.rows_filtered_out, scoring_job.number_of_attempts, scoring_job.user.display_name, scoring_job.autoscoring_project_scoring_job_output_files.count, scoring_job.details, scoring_job.created_at, scoring_job.updated_at]
          end
        end
      end

      def delete_all_scoring_job_records
        # Initiate the Scoring Job Cleanup Background Process that spawns more jobs to delete the scoring_job_records
        InitiateScoringJobCleanupWorker.perform_async(self.id)
      end



      private

      def opportunity_yield_rates
        @opportunity_yield_rates ||= Array.new # array to hold all yield rates for the current record's other_variable yields
      end

      def contact_codes_last_dates
        unless @contact_codes_last_dates
          # Setup array to hold the start of the next date range to check if a date is before therefore setting it to the correct contact date codes
          @contact_codes_last_dates = Array.new
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 4, 6, 1) # 0 before june 1st of four years ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 3, 6, -1) # 1 before june of three years ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 2, 6, 1) # 2 before june of two years ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 2, 9, 1) # 3 before september of two years ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 2, 12, 1) # 4 before december of two years ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 1, 3, 1) # 5 before march of one year ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 1, 6, 1) # 6 before june of one year ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 1, 9, 1) # 7 before september of one year ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value - 1, 12, 1) # 8 before december of one year ago
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value, 3, 1) # 9 before march of current year
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value, 3, 1) # 10 before june of current year
          contact_codes_last_dates << Date.new(scoring_job_autoscoring_project.report_year.value, 9, 1) # 11 before september of current year
        end
        @contact_codes_last_dates
      end

      def current_variable_yield_rate
        current_variable_yield_rate ||= scoring_job_autoscoring_project.default_yield_rate
      end

      def delete_all_scoring_job_logs
        self.autoscoring_project_scoring_job_logs.delete_all
      end

      def get_autoscoring_project_other_variable_count_yield(value, other_variable_id, postal_code_name, state_abbreviation)
        # Set to empty string so we can use it in lookups and we aren't trying to use a nil object
        lookup_value = ""

        state_abbreviation = "" if state_abbreviation.blank?
        value = "" if value.blank?

        other_variable = get_autoscoring_project_other_variable(other_variable_id)

        # set value to the postal_code_value when the variable_id represents the Market Variable which is used in caching yield rates
        value = postal_code_name if other_variable.variable_id == 18
        # set value to to state_abbreviation + ceeb value if the variable is High School which is used in caching yield rates
        value = state_abbreviation + "-" + value if other_variable.variable_id == 16
        # Check if yield rate has already been determined & cached for the given value & other_variable
        unless yield_object = self.other_variable_code_yield_cache.get([other_variable.id, other_variable.name, value])

          if other_variable.present?

            # Check if variable is GPA & apply GPA classification logic to convert GPA value to 01-05 GPA Codes if the value is not nil
            if other_variable.variable_id == 15 && value.present?

              # Variable is GPA so we now need to determine what code to use based on if the value is on the 4 pt. or 100 pt. scale
              if value.is_numeric?
                value = value.to_f
                if value <= 5
                  # assume value is on the traditional 4pt. GPA scale
                  if value >= 4
                    lookup_value = "01"
                  elsif value >= 3.75
                    lookup_value = "02"
                  elsif value >= 3.5
                    lookup_value = "03"
                  elsif value >= 3
                    lookup_value = "04"
                  elsif value < 3
                    lookup_value = "05"
                  end
                elsif value <= 110
                  # assume value is on the 100 pt. scale
                  if value >= 90
                    lookup_value = "01"
                  elsif value >= 87.5
                    lookup_value = "02"
                  elsif value >= 85
                    lookup_value = "03"
                  elsif value >= 80
                    lookup_value = "04"
                  elsif value < 80
                    lookup_value = "05"
                  end

                end

              end
            end

            # Check if variable is High School CEEB to know whether we need to transform the lookkup value to a proper CEEB value
            if other_variable.variable_id == 16
              # Other variable is High School CEEB so we need to get the transformed CEEB code value
              ceeb_value = get_transformed_ceeb_code(value) unless value.nil?
              lookup_value = state_abbreviation +"-" + ceeb_value
            end

            # Check if Variable is Market to reset the value to the apprpriate market designation level before doing any translation lookups
            if other_variable.variable_id == 18

              if postal_code_name
                case scoring_job_autoscoring_project.market_designation_level.id
                when 1 # Market Variable Not Used in Model
                when 2 # Postal Code Region
                  # value = postal_code.name
                  value = postal_code_name
                when 3 # Single State
                  # value = postal_code.state.abbreviation
                  value = state_abbreviation
                when 4 # Multiple State Region
                  # value = postal_code.state.abbreviation
                  value = state_abbreviation
                end
              end
            end



            # Get yield rate based on the primary column name's lookup
            if other_variable.has_uploaded_translations?
              # Set value to the translation's description value
              # translation = get_autoscoring_project_other_variable_translation([other_variable.id, value])
              unless translation = self.autoscoring_project_other_variable_translation_cache.get([other_variable.id, value])
                translation = AutoscoringProjectOtherVariableTranslation.where("autoscoring_project_other_variable_id = ? AND code = ?", other_variable.id, value).select(:description).first
                self.autoscoring_project_other_variable_translation_cache.put([other_variable.id, value], translation)
              end

              lookup_value = translation.description unless translation.nil?

            end

            # Lookup counts for given value & current other_variable

            # no to lookup replacement value so we will use the default value if after attempting to replace with postal code name and translations
            lookup_value = value if lookup_value.blank?

            # Don't need to cache this because we only his this cache once for a given other_variable and lookup_value
            # unless autoscoring_project_other_variable_count = self.autoscoring_project_other_variable_count_cache.get([other_variable.id, lookup_value])
            autoscoring_project_other_variable_count = AutoscoringProjectOtherVariableCount.find(:first, :select => 'enrolled, total', :conditions => ["autoscoring_project_other_variable_id = ? AND name = ?",other_variable.id, lookup_value])
            #   self.autoscoring_project_other_variable_count_cache.put([other_variable.id, lookup_value], autoscoring_project_other_variable_count)
            # end

            # IF MARKET is the other_variable and we did not find a successful count lookup, then we set the variable_count to the OTHER market count record
            if other_variable.variable_id == 18 && autoscoring_project_other_variable_count.nil?
              autoscoring_project_other_variable_count = get_market_other_count(other_variable.id)
            end


            # Check if Market is the other_variable and we changed the value to be a state abbreviation to know if we need to change it back.
            if other_variable.variable_id == 18 && [3, 4].include?(scoring_job_autoscoring_project.market_designation_level.id)
              # Market variable's value originally looked up was not the postal value but the value needs to be set back to the postal code before we add item to cache so the yield rate can be retrieved again
              value = postal_code_name
            end

            if autoscoring_project_other_variable_count.nil?
              # Set yield_rate to default yield rate & use if the lookup fails
              yield_rate = scoring_job_autoscoring_project.default_yield_rate
              using_default_yield_rate = true
            else
              yield_rate = autoscoring_project_other_variable_count.yield_as_decimal
              using_default_yield_rate = false
            end


          end

          yield_object = [yield_rate, using_default_yield_rate]

          self.other_variable_code_yield_cache.put([other_variable.id, other_variable.name, value], yield_object)
        end

        # check if the saved yield_object is a yield rate lookup that failed & therefore used the default yield rate
        if yield_object[1]
          # Add entry to hash of variable/values that used the default yield rate
          value = "" if value.nil? # convert nil values to empty string so we can correctly create key value
          hash_key = other_variable.name + " | " + value.to_s
          if self.other_variable_values_using_default_yields.has_key?(hash_key)
            self.other_variable_values_using_default_yields[hash_key] += 1
          else
            # Store new entry into hash
            self.other_variable_values_using_default_yields.merge!(hash_key => 1)
          end
        end

        # get first item in yield_object array that is holding the yield rate
        yield_rate = yield_object[0]

        return yield_rate
      end

      def get_geodemographic_variable_code_name(postal_code_id, geodemographic_year_id, variable_id)
        unless geodemographic_variable_code_name = self.geodemographic_variable_code_name_cache.get([postal_code_id, geodemographic_year_id, variable_id])
          # Try & Lookup if we have Postal Code Geodemographic data for the postal code value
          if postal_codes_year = get_postal_codes_year(postal_code_id, geodemographic_year_id)
            # We have Postal Code GeoDemographic data for postal code, so we need to determine the geodemo code for this
            case variable_id
            when 1 #HVL
              geodemographic_variable_code_name = postal_codes_year.hvl.name
            when 2 #MFI
              geodemographic_variable_code_name = postal_codes_year.mfi.name
            when 3 #MHB
              geodemographic_variable_code_name = postal_codes_year.mhb.name
            when 4 #ETH
              geodemographic_variable_code_name = postal_codes_year.eth.name
            when 5 #POP
              geodemographic_variable_code_name = postal_codes_year.pop.name
            when 6 #MDG
              geodemographic_variable_code_name = postal_codes_year.mdg.name
            when 7 #ADG
              geodemographic_variable_code_name = postal_codes_year.adg.name
            else # Set to nil in case no matching variable id was found
              geodemographic_variable_code_name = nil
            end
          else
            # Use default yield rate when we don't have a valid postal code
            geodemographic_variable_code_name = nil
          end
          self.geodemographic_variable_code_name_cache.put([postal_code_id, geodemographic_year_id, variable_id],geodemographic_variable_code_name)
        end

        # Return geodemographic_variable_code_name
        geodemographic_variable_code_name
      end

      def get_file_info

      end

      def update_autoscoring_project_scoring_details_if_completed
        if self.completed? && self.details.nil?
          last_autoscoring_project_scoring_job_log = self.last_autoscoring_project_scoring_job_log
          self.details = last_autoscoring_project_scoring_job_log.details
          self.filter_criteria = last_autoscoring_project_scoring_job_log.filter_criteria
          self.conversion_criteria = last_autoscoring_project_scoring_job_log.conversion_criteria
          self.split_output_by_column_name = last_autoscoring_project_scoring_job_log.split_output_by_column_name
          self.save
        end
      end

      # def scoring_output_file_yield_check
      #   scoring_output_file_yield_check ||= File.open("#{Rails.root}/tmp/sample_scorings/scoring_output_file_yield_check_" + self.id.to_s + self.updated_at.to_s + ".txt", 'a' )
      # end

      def get_em_formatted_id_string(id)
        if id.is_a? Numeric
          # convert to integer and then to string to remove any added precision that may have been appended to the value before converting to string
          id.to_i.to_s
        else
          # id is non-numeric so we can convert to string
          id.to_s
        end
      end

      def get_probability_score(row, postal_code_name, geodemographic_year_id)
        begin

          # File.open("#{Rails.root}/tmp/sample_scorings/scoring_output_file_yield_check_" + self.id.to_s + self.updated_at.to_s + ".txt", 'a' ) do |check_file|
          #            check_file.write(row[scoring_job_autoscoring_project.em_person_id_column_name.upcase] + " (#{postal_code_name}):\n") if  scoring_job_autoscoring_project.is_em_configured?
          #            check_file.write(row[scoring_job_autoscoring_project.client_opportunity_id_column_name.upcase] + " (#{postal_code_name}):\n") if  !scoring_job_autoscoring_project.is_em_configured?
          #
          #            check_file.write("\t Y Intercept #{scoring_job_autoscoring_project.model_y_intercept}\n")

          # Set the default probability_score to the model_y_intercept before processing any additional variables included in model
          log_tot = scoring_job_autoscoring_project.model_y_intercept

          if postal_code_name.present?
            if postal_code = get_postal_code(postal_code_name)
              postal_code_id = postal_code.id
              postal_code_name = postal_code.name
              state_abbreviation = postal_code.state.abbreviation
              # Retrieve from cache the postal code before we loop through geodemographics to only lookup once
            end
          end

          # check_file.write("State: #{state_abbreviation}\n")


          # We have postal code so we can process each geodemographic variable & determine which yield rate value to use
          scoring_job_autoscoring_project_relevant_geodemographic_variables.each do |geodemographic_variable|
            # Only consider those geodemographic_variables with a valid coefficient since these were the only variables used in the model
            if geodemographic_variable.coefficient_value.present?
              # Set yield_rate to default yield rate in case we can't find another yield rate to use
              current_variable_yield_rate = scoring_job_autoscoring_project.default_yield_rate

              # Valid Postal Code geodemographic_variable_count so we can use autoscoring_project_geodemographic_variable_count yield as decimal value
              if postal_code_name.present?
                geodemographic_variable_code_name = get_geodemographic_variable_code_name(postal_code_id, scoring_job_autoscoring_project.geodemographic_year_id, geodemographic_variable.variable_id)
                if autoscoring_project_geodemographic_variable_count = get_autoscoring_project_geodemographic_variable_count(geodemographic_variable.id, geodemographic_variable_code_name)
                  # Valid autoscoring_project_geodemographic_variable_count record so we can use it's true yield rate
                  current_variable_yield_rate = autoscoring_project_geodemographic_variable_count.yield_as_decimal
                end
              end
              # compute new probability score based on current geodemographic variables' coefficient_value and yield rate
              # check_file.write "\t #{geodemographic_variable.variable.name} - #{geodemographic_variable_code_name} (#{geodemographic_variable.coefficient_value} * #{current_variable_yield_rate})\n"

              log_tot += geodemographic_variable.coefficient_value * current_variable_yield_rate
            end
          end

          # We process each autoscoring_project_other_variable and apply any translations or conversions or multi-column reference lookups

          scoring_job_autoscoring_project_relevant_other_variables.each do |other_variable|
            # Check to ensure we have a valid numeric coefficient_value
            if other_variable.coefficient_value.present?
              # Set yield_rate to default yield rate in case we can't find another yield rate to use
              current_variable_yield_rate = scoring_job_autoscoring_project.default_yield_rate
              primary_column_value = row[other_variable.name.upcase]
              # Check if current other variable is a date type & if the value matches the defined format.
              if other_variable.is_date?
                if other_variable.variable_id == 11 # Variable is Contact Date
                  # Rails.cache.fetch(["contact_date_conversion", row[other_variable.name.upcase]]) do
                  unless primary_column_value = self.contact_date_conversion_cache.get(row[other_variable.name.upcase])
                    # contact_date = Date.strptime(row[other_variable.name.upcase], other_variable.date_format.format)
                    contact_date = row[other_variable.name.upcase]
                    # attempt to cast the string representation into a date object
                    begin
                      unless contact_date.blank? || contact_date == "NULL"
                        contact_date = Date.strptime(contact_date.to_s.strip, other_variable.date_format.format)
                        if contact_date.year <= 1900
                          # likely the contact_date didn't convert properly based on the format
                          raise "contact_date year suggests an error with the date casting"
                        end
                      end
                    rescue
                      self.processing_error_details << "Inproper Date Column Formatting in the column #{other_variable.name}. Error occurred trying to convert '#{row[other_variable.name.upcase]}'. Please make sure the format is '#{other_variable.date_format.name}' in the Input File. \n"
                      self.encountered_error = true
                      return
                    end
                    if contact_date.is_a?(Date)
                      contact_codes_last_dates.each_with_index do |codes_last_date, index|
                        if contact_date - codes_last_date < 0
                          primary_column_value = sprintf '%02d', index.to_s
                          break
                        end
                      end
                    else
                      # contact_date value is not within the valid range of 01 - 11 contact date codes so we give it the default value to use the default yield rate
                      # typically applies to records with a really old date prior to june 1st of four years ago
                      primary_column_value = "00"
                    end
                    self.contact_date_conversion_cache.put(row[other_variable.name.upcase], primary_column_value)
                  end
                  # end
                end
              end

              # get yield rate of primary column name

              current_variable_yield_rate = get_autoscoring_project_other_variable_count_yield(primary_column_value, other_variable.id, postal_code_name, state_abbreviation)


              # Determine if this variable has multi-columns specified have higher yields than the primary column name's yield & if so update the yield rate to use

              if !other_variable.secondary_column_name.blank? || !other_variable.tertiary_column_name.blank?
                opportunity_yield_rates.clear
                opportunity_yield_rates << current_variable_yield_rate

                if !other_variable.secondary_column_name.blank?
                  if !row[other_variable.secondary_column_name.upcase].blank?
                    # only check the cache if we have a non-nil value for the current row
                    secondary_column_yield_rate = get_autoscoring_project_other_variable_count_yield(row[other_variable.secondary_column_name.upcase], other_variable.id, postal_code_name, state_abbreviation)
                    opportunity_yield_rates.push(secondary_column_yield_rate)
                  end
                end
                if !other_variable.tertiary_column_name.blank?
                  if !row[other_variable.tertiary_column_name.upcase].blank?
                    # only check the cache if we have a non-nil value for the current row
                    tertiary_column_yield_rate = get_autoscoring_project_other_variable_count_yield(row[other_variable.tertiary_column_name.upcase], other_variable.id, postal_code_name, state_abbreviation)
                    opportunity_yield_rates.push(tertiary_column_yield_rate)
                  end
                end

                # Use the highest yield_rate out of the three
                current_variable_yield_rate = opportunity_yield_rates.max
              else
                opportunity_yield_rates.clear
              end
              # compute new probabilty score based on current other_varabe variables' coefficient_value and yield rate determined to use for current column's value
              # check_file.write "\t #{other_variable.name} [#{primary_column_value}, #{row[other_variable.secondary_column_name.upcase]}, #{row[other_variable.tertiary_column_name.upcase]}] (#{other_variable.coefficient_value} * #{current_variable_yield_rate} [#{opportunity_yield_rates.join(',')}])\n"

              log_tot += other_variable.coefficient_value * current_variable_yield_rate
            end
          end

          #
          # check_file.write "\t Log Tot - #{log_tot}\n"
          #            probability_score = (Math.exp(log_tot) / (1 + Math.exp(log_tot)))
          #            check_file.write "\t Probability Score - #{probability_score.to_s} \n\n"

          # Return probability_score
          # Apply Log Odds Ratio to Regression Equation Result
          return (Math.exp(log_tot) / (1 + Math.exp(log_tot)))
          # end # end of check_file block
        rescue => e
          ExceptionNotifier::Notifier.background_exception_notification(e).deliver
          self.encountered_error = true
        end
      end

      def get_rank_from_percentile_rank(percentile_rank)
        unless rank = self.percentile_rank_cache.get(percentile_rank)
          if percentile_rank.nil?
            rank = "-"
          else
            case 10 - (percentile_rank.to_f / 10).to_i
            when 0
              rank = "A"
            when 1
              rank =  "A"
            when 2
              rank =  "B"
            when 3
              rank =  "C"
            when 4
              rank =  "D"
            when 5
              rank =   "E"
            when 6
              rank =   "F"
            when 7
              rank =   "G"
            when 8
              rank =   "H"
            when 9
              rank =   "I"
            when 10
              rank =   "J"
            else
              rank =  ""
            end
          end
          self.percentile_rank_cache.put(percentile_rank, rank)
        end
        rank
      end

      def get_transformed_ceeb_code(ceeb_value_string)
        unless t_ceeb_code = self.ceeb_code_cache.get(ceeb_value_string)
          t_ceeb_code = ceeb_value_string.gsub(/[^0-9.]/, "").to_i.to_s # strip out non-numeric characters, convert to integer to remove leading zeros before appending appropriate zeros
          if t_ceeb_code.length == 0
            self.ceeb_code_cache.put(ceeb_value_string, "000000")
          else
            t_ceeb_code = sprintf('%06d', t_ceeb_code)
            self.ceeb_code_cache.put(ceeb_value_string, t_ceeb_code)
          end
        end
        t_ceeb_code
      end

      def get_transformed_us_five_digit_postal_code_name(postal_code_string)

        unless t_postal_code = self.transformed_postal_code_value_cache.get(postal_code_string)
          t_postal_code = postal_code_string
          # strip all characters beyond the '-' if character is present
          if t_postal_code.include?('-')
            last_character = t_postal_code.index('-') - 1
            t_postal_code = t_postal_code[0..last_character]
          end

          t_postal_code = t_postal_code[0..-3] if postal_code_string.last(2) == ".0" # convert object to string & remove .0 if it's present
          t_postal_code = t_postal_code.gsub(/[^0-9.]/, "") # strip out non-numeric characters, convert to integer to remove leading zeros before appending appropriate zeros
          if t_postal_code.length != 5 && t_postal_code.length < 10 # e.g. '60048.0' == true, take stripped out version if the initial string was 7 characters & ended in .0
            t_postal_code = t_postal_code.to_i.to_s # convert to int to remove leading zeros before adding appropriate number back
            case
            when  t_postal_code.length > 5
              # append correct # of zeros to front of postal_code that may have been removed when converting
              i = 0
              until postal_code_string[i] != "0"
                t_postal_code =  "0" + t_postal_code
                i +=1
              end
              # append necessary number of zeros on the end until the full string length is 9
              until t_postal_code.length == 9 do
                t_postal_code = t_postal_code + "0"
              end
            when t_postal_code.length < 5
              t_postal_code = sprintf '%05d', t_postal_code # append necessary leading zeros
              until t_postal_code.length == 9 do
                t_postal_code = t_postal_code + "0"
              end
            end
          end
          t_postal_code = t_postal_code[0..4] # take the first five characters of a postal code

          self.transformed_postal_code_value_cache.put(postal_code_string, t_postal_code)
        end
        t_postal_code
      end

      def is_state_value_a_valid_usa_state(state_value)
        unless is_state = self.state_cache.get(state_value)
          state = State.where("(abbreviation = ? OR name = ?) AND country_id = 1228", state_value, state_value).select(:id).first
          is_state = state.present?
          self.state_cache.put(state_value,is_state)
        end
        is_state
      end

      def get_postal_code(postal_code_name)
        # Rails.cache.fetch(["postal_and_state", postal_code_name], expires_in: 1.year) do
        unless postal_code_state = self.postal_code_state_cache.get(postal_code_name)
          postal_code_state = PostalCode.includes(:state).find(:first, :conditions => ["postal_codes.name = ? AND postal_codes.country_id= 1228", postal_code_name])
          self.postal_code_state_cache.put(postal_code_name,postal_code_state)
        end
        postal_code_state
      end

      def get_market_other_count(other_variable_id)
        if !@market_other_count
          primary_market_name = AutoscoringProjectOtherVariableTranslation.where("autoscoring_project_other_variable_id = ?", other_variable_id).select(:description).first
          if primary_market_name
            description = primary_market_name.description
          else
            description = ""
          end
          @market_other_count = AutoscoringProjectOtherVariableCount.where("autoscoring_project_other_variable_id = ? AND name != ?", other_variable_id, description).first
        end
        @market_other_count
      end

      def get_autoscoring_project_geodemographic_variables
        AutoscoringProjectGeodemographicVariable.includes(:variable).where("autoscoring_project_geodemographic_variables.autoscoring_project_id = ?", autoscoring_project_id).order("variables.position ASC")
      end

      def get_autoscoring_project_other_variable(other_variable_id)
        # Rails.cache.fetch(["autoscoring_project", autoscoring_project_id, "autoscoring_project_other_variable",variable_id, self.autoscoring_project.updated_at.to_s(:number)], expires_in: 1.month) do
        unless other_variable = autoscoring_project_other_variable_cache.get(other_variable_id)
          other_variable = AutoscoringProjectOtherVariable.includes(:variable).find_by_id(other_variable_id)
          autoscoring_project_other_variable_cache.put(other_variable_id, other_variable)
        end
        other_variable
      end

      def scoring_job_autoscoring_project
        @scoring_job_autoscoring_project ||= AutoscoringProject.includes(:market_designation_level, :autoscoring_project_geodemographic_variables => :variable, :autoscoring_project_other_variables => [:variable, :date_format], :autoscoring_project_conversion_columns => [:autoscoring_project_conversion_values], :autoscoring_project_filter_columns => [:autoscoring_project_filter_values]).find(autoscoring_project_id)
      end

      def scoring_job_autoscoring_project_relevant_geodemographic_variables
        @scoring_job_autoscoring_project_relevant_geodemographic_variables ||= AutoscoringProjectGeodemographicVariable.includes(:variable).find(:all, :conditions => ["autoscoring_project_geodemographic_variables.autoscoring_project_id = ? AND coefficient_value IS NOT NULL", self.autoscoring_project_id])
      end

      def scoring_job_autoscoring_project_relevant_other_variables
        @scoring_job_autoscoring_project_relevant_other_variables ||= AutoscoringProjectOtherVariable.includes(:variable, :date_format).find(:all, :conditions => ["autoscoring_project_other_variables.autoscoring_project_id = ? AND coefficient_value IS NOT NULL", self.autoscoring_project_id])
      end

      def get_postal_codes_year(postal_code_id, year_id)
        unless postal_codes_year = self.postal_codes_year_cache.get([postal_code_id, year_id])
          # Rails.cache.fetch(["postal_codes_years", "postal_code", postal_code_id, "geodemographic_year", year_id], expires_in: 1.year) do
          postal_codes_year =  PostalCodesYear.includes(:hvl, :mfi, :mhb, :eth, :pop, :mdg, :adg).find_by_postal_code_id_and_year_id(postal_code_id, year_id)
          self.postal_codes_year_cache.put([postal_code_id, year_id], postal_codes_year)
        end
        postal_codes_year
      end

      def get_autoscoring_project_geodemographic_variable_count(geodemographic_variable_id, geodemographic_variable_code_name)
        unless autoscoring_project_geodemographic_variable_count = self.autoscoring_project_geodemographic_variable_count_cache.get([geodemographic_variable_id, geodemographic_variable_code_name])
          autoscoring_project_geodemographic_variable_count = AutoscoringProjectGeodemographicVariableCount.find_by_autoscoring_project_geodemographic_variable_id_and_name(geodemographic_variable_id, geodemographic_variable_code_name)
          self.autoscoring_project_geodemographic_variable_count_cache.put([geodemographic_variable_id, geodemographic_variable_code_name],autoscoring_project_geodemographic_variable_count)
        end
        autoscoring_project_geodemographic_variable_count
      end

      def sanitize_filename(filename)
        # Split the name when finding a period which is preceded by some
        # character, and is followed by some character other than a period,
        # if there is no following period that is followed by something
        # other than a period (yeah, confusing, I know)
        fn = filename.split /(?<=.)\.(?=[^.])(?!.*\.[^.])/m

        # We now have one or two parts (depending on whether we could find
        # a suitable period). For each of these parts, replace any unwanted
        # sequence of characters with an underscore
        fn.map! { |s| s.gsub /[^a-z0-9\-]+/i, '_' }

        # Finally, join the parts with a period and return the result
        return fn.join '.'
      end

      COMMON_DELIMITERS = [",","\t"]

      def sniff(path)
        first_line = nil
        # first_line = File.open(path).first
        CSV.foreach(path) do |row|
          first_line = row
          break # sniff first row & break out of file loop
        end
        return nil unless first_line
        snif = {}
        COMMON_DELIMITERS.each {|delim|snif[delim]=first_line.count(delim)}
        snif = snif.sort {|a,b| b[1]<=>a[1]}
        snif.size > 0 ? snif[0][0] : nil
      end
    end
