class DateFormat < ActiveRecord::Base
  attr_accessible :format, :name
  
  validates :name, :presence => true, :uniqueness => true
  validates :name, :presence => true, :uniqueness => true
  
  has_many :autoscoring_project_other_variables
  
end
