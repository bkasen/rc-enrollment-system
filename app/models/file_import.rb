class FileImport < ActiveRecord::Base
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessible :admin_user_id, :file_import_type_id, :file_name, :institution_id, :name, :processed, :user_id
  mount_uploader :file_name, FileUploader

  attr_accessor :error_details

  validates :name, :presence => true
  validates :file_import_type, :presence => true
  validates :file_name, :presence => true

  belongs_to :file_import_type
  belongs_to :admin_user
  belongs_to :user
  belongs_to :institution


  scope :unprocessed, where(:processed => false)
  scope :complete, where(:processed => true)

  COMPLETE = "complete"
  UNPROCESSED = "unprocessed"

  def state
    processed? ? COMPLETE : UNPROCESSED
  end

  def import
    current_row = 0
    error = ""
    begin
      ActiveRecord::Base.transaction do

        spreadsheet = open_spreadsheet
        header = spreadsheet.row(1)


        case self.file_import_type.id
        when 1 # Annual Geodemographic Census Data

          # define arrays to use with import method
          row = nil
          postal_codes_years = []
          postal_code_geodemographics = []
          # lookups
          state = nil
          year = nil
          county = nil
          mdg = nil
          adg = nil
          eth = nil
          hvl = nil
          mfi = nil
          mhb = nil
          pop = nil
          postal_code = nil
          postal_codes_year = nil
          t_postal_code = nil

          variable_hash = Hash.new
          state_hash = Hash.new
          year_hash = Hash.new
          county_hash = Hash.new
          geodemographic_variable_hash = Hash.new
          postal_code_hash = Hash.new
          postal_codes_year_hash = Hash.new

          (2..spreadsheet.last_row).each do |i|
            current_row += 1
            row = Hash[[header, spreadsheet.row(i)].transpose]

            # transformed values
            # convert to integer to remove leading zeros before appending appropriate zeros
            t_postal_code = sprintf '%05d', row["ZIPCODE"].to_i.to_s # append leading zeros if not present



            unless state = state_hash[row["STNAME"].to_sym]
              unless state = State.select("id, country_id").where("name = ? AND country_id = ?", row["STNAME"], 1228).first
                error = "Can't Find a State with the Name of - " + row["STNAME"].to_s
                raise ActiveRecord::RecordNotFound
              end
              state_hash[row["STNAME"].to_sym] = state
            end

            unless year = year_hash[row["YEAR"].to_s.to_sym]
              unless year = Year.select(:id).where("name =?", row["YEAR"]).first.id
                error = "Can't Find a Year with the Name of - " + row["YEAR"].to_s
                raise ActiveRecord::RecordNotFound
              end
              year_hash[row["YEAR"].to_s.to_sym] = year
            end

            unless county = county_hash[row["CTYNAME"].to_sym]
              unless county = County.select(:id).where("name = ? AND state_id = ? AND country_id = ?", row["CTYNAME"], state.id, state.country_id).first.id
                error = "Can't Find a County with the Name of - " + row["CTYNAME"].to_s
                raise ActiveRecord::RecordNotFound
              end
              county_hash[row["CTYNAME"].to_sym] = county
            end

            unless mdg = variable_hash[row["MDG"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "MDG").first.id
              unless mdg = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["MDG"]).first.id
                error = "Can't Find a MDG Geodemographic Variable with the Name of - " + row["MDG"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["MDG"].to_sym] = mdg
            end

            unless adg = variable_hash[row["ADG"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "ADG").first.id
              unless adg = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["ADG"]).first.id
                error = "Can't Find a ADG Geodemographic Variable with the Name of - " + row["ADG"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["ADG"].to_sym] = adg
            end

            unless eth = variable_hash[row["ETH"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "ETH").first.id
              unless eth = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["ETH"]).first.id
                error = "Can't Find a ETH Geodemographic Variable with the Name of - " + row["ETH"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["ETH"].to_sym] = eth
            end

            unless hvl = variable_hash[row["HVL"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "HVL").first.id
              unless hvl = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["HVL"]).first.id
                error = "Can't Find a HVL Geodemographic Variable with the Name of - " + row["HVL"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["HVL"].to_sym] = hvl
            end

            unless mfi = variable_hash[row["MFI"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "MFI").first.id
              unless mfi = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["MFI"]).first.id
                error = "Can't Find a MFI Geodemographic Variable with the Name of - " + row["MFI"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["MFI"].to_sym] = mfi
            end

            unless mhb = variable_hash[row["MHB"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "MHB").first.id
              unless mhb = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["MHB"]).first.id
                error = "Can't Find a MHB Geodemographic Variable with the Name of - " + row["MHB"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["MHB"].to_sym] = mhb
            end

            unless pop = variable_hash[row["POP"].to_sym]
              variable = Variable.select(:id).where("abbreviation = ?", "POP").first.id
              unless pop = GeodemographicVariable.select(:id).where("variable_id = ? AND name = ?", variable, row["POP"]).first.id
                error = "Can't Find a POP Geodemographic Variable with the Name of - " + row["POP"].to_s
                raise ActiveRecord::RecordNotFound
              end
              variable_hash[row["POP"].to_sym] = pop
            end
            # unless postal_code = postal_code_hash[t_postal_code.to_sym]
            #   postal_code = PostalCode.select(:id).where("name = ? AND state_id = ? AND country_id = ?", t_postal_code, state.id, state.country_id).first
            #   postal_code_hash[t_postal_code.to_sym] = postal_code
            # end
            # unless postal_code = PostalCode.find_by_name_and_state_id_and_country_id(t_postal_code, state.id, state.country_id)
            #              error = "Can't Find a Postal Code with the Name of - " + t_postal_code.to_s + ", State of " + state.name + ", and Country of " + state.country.name
            #              raise ActiveRecord::RecordNotFound
            #            end
            unless postal_code = postal_code_hash[t_postal_code.to_sym]
              unless postal_code = PostalCode.select(:id).where("name = ? AND state_id = ? AND country_id = ?", t_postal_code, state.id, state.country_id).first
                # create postal code record if does not exist in database
                postal_code = PostalCode.new
                postal_code.name = t_postal_code
                postal_code.state_id = state.id
                postal_code.country_id = state.country_id
                postal_code.save!
              end
              postal_code_hash[t_postal_code.to_sym] = postal_code.id
              postal_code = postal_code_hash[t_postal_code.to_sym]
            end

            # unless postal_codes_year = postal_codes_year_hash["#{postal_code.id}/#{year.id}".to_sym]
            #   postal_codes_year = PostalCodesYear.find_by_postal_code_id_and_year_id(postal_code, year)
            #   postal_codes_year_hash["#{postal_code.id}/#{year.id}".to_sym] = postal_codes_year
            # end

            unless postal_codes_year = PostalCodesYear.find_by_postal_code_id_and_year_id(postal_code, year)

              # create postal_codes_year record if does not exist in database
              postal_codes_year = PostalCodesYear.new
              postal_codes_year.postal_code_id = postal_code
              postal_codes_year.year_id = year
              postal_codes_year.county_id = county
              postal_codes_year.high_school_age_population = row["AGECY"]
              postal_codes_year.geo_name = row["GEONAME"]
              postal_codes_year.case_district = row["CASE"]
              postal_codes_year.residential = row["RESIDENTIAL"].to_s.to_bool
              postal_codes_year.mdg_id = mdg
              postal_codes_year.adg_id = adg
              postal_codes_year.eth_id = eth
              postal_codes_year.hvl_id = hvl
              postal_codes_year.mfi_id = mfi
              postal_codes_year.mhb_id = mhb
              postal_codes_year.pop_id = pop

              postal_codes_years << postal_codes_year
            end

          end
          # check validity and import records in array
          if postal_codes_years.map(&:valid?).all?
            postal_codes_years.each_slice(1000) do |slice|
              PostalCodesYear.import slice
            end
          else
            postal_codes_years.each_with_index do |postal_codes_year, index|
              postal_codes_year.errors.full_messages.each do |message|
                errors.add :base, "Row #{index+2}: #{message}"
              end
            end
            raise "Error with Row(s)"
          end

        else
          raise "Unknown file import type: #{file.original_filename}"
        end

      end


    end
    self.processed = true
    return true
    # rescue Exception => e
    #   self.error_details = "Row #{current_row}: #{error}"
    #   self.error_details += "An error occurred on row " + current_row.to_s + ", " + e.message
    #   self.processed = false
    #   return false
  end

  def open_spreadsheet
    Roo::Spreadsheet.open(file_name.url)
    # case File.extname(file_name.path)
    # when ".csv" then Roo::Csv.new(file_name.url, nil, :ignore).encode('UTF-8', 'UTF-8', :invalid => :replace)
    # when ".xls" then  Roo::Excel.new(file_name.url, nil, :ignore)
    # when ".xlsx" then  Roo::Excelx.new(file_name.url, nil, :ignore)
    # when ".xlsx" then Roo::Spreadsheet.open(file_name.url)
    # else raise "Unknown file type: #{file_name.path}"
    # end
  end

end
