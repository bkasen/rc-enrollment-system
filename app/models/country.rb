class Country < ActiveRecord::Base
  attr_accessible :name
  
  validates :name, :presence => true
  
  has_many :counties
  has_many :institutions
  has_many :postal_codes
  has_many :states
  
end
