class AutoscoringProject < ActiveRecord::Base
  attr_accessible :active, :project_manager_id, :admin_user_id, :default_yield_rate, :institution_id, :model_y_intercept, :name, :notes, :em_opportunity_id_column_name, :client_opportunity_id_column_name, :postal_code_column_name, :state_column_name, :split_output_by_column_name, :report_year_id, :geodemographic_year_id, :institution_display_name, :autoscoring_project_geodemographic_variables_attributes, :autoscoring_project_other_variables_attributes, :autoscoring_project_filter_columns_attributes, :autoscoring_project_conversion_columns_attributes, :market_designation_level_id, :em_person_id_column_name, :autoscoring_project_status_id, :is_em_configured

  belongs_to :institution
  belongs_to :report_year, :class_name => "Year"
  belongs_to :geodemographic_year, :class_name => "Year"
  belongs_to :admin_user
  belongs_to :project_manager
  belongs_to :market_designation_level
  belongs_to :autoscoring_project_status

  has_many :autoscoring_project_geodemographic_variables, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_geodemographic_variables, allow_destroy: true

  has_many :autoscoring_project_other_variables, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_other_variables, allow_destroy: true

  has_many :autoscoring_project_filter_columns, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_filter_columns, allow_destroy: true

  has_many :autoscoring_project_conversion_columns, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_conversion_columns, allow_destroy: true

  has_many :autoscoring_project_scoring_jobs, :dependent => :destroy

  validates :admin_user, :presence => true
  validates :default_yield_rate, :presence => true, :numericality => true
  validates :geodemographic_year, :presence => true
  validates :institution_display_name, :presence => true
  validates :market_designation_level, :presence => true
  validates :model_y_intercept, :presence => true, :numericality => true
  validates :name, :presence => true
  validates :em_person_id_column_name, :presence => { :if => :is_em_configured? }
  validates :em_opportunity_id_column_name, :presence => { :if => :is_em_configured? }
  validates :client_opportunity_id_column_name, :presence => { :unless => :is_em_configured? }
  validates :project_manager, :presence => true

  validates :postal_code_column_name, :presence => true
  validates :state_column_name, :presence => true
  validates :report_year, :presence => true
  validates :market_designation_level, :presence => true

  before_validation :strip_whitespace


  before_save :clear_irrelevent_id_column_name_fields_by_em_configured_flag
  after_save :deactivate_other_institution_autoscoring_projects
  after_save :update_autoscoring_project_active_status_variable_counts
  after_initialize :default_values

  scope :active, where(:active => true)
  scope :inactive, where(:active => false)


  ACTIVE = "active"
  INACTIVE = "inactive"

  scope :em_configured, where(:is_em_configured => true)
  scope :client_configured, where(:is_em_configured => false)

  EMCONFIGURED = "EM-Configured Project"
  UNPROCESSED = "Client-Configured-Project"

  def state
    processed? ? COMPLETE : UNPROCESSED
  end

  def is_public?
    autoscoring_project_status_id == 3 ? true : false
  end

  def self.all_publicly_available
    self.where(:active => true, :autoscoring_project_status_id => 3)
  end

  def self.all_active_and_available
    self.where(:active => true, :autoscoring_project_status_id => [2,3])
  end

  def configuration_type
    if self.is_em_configured.present?
      "EM Configured Project"
    else
      "Client Configured Project"
    end
  end

  def name_with_configuration
    if self.is_em_configured.present?
      "#{self.name} (EM Configured)"
    else
      "#{self.name} (Client Configured)"
    end
  end

  def public_autoscoring_project_scoring_job_count
    # AutoscoringProject.includes(:autoscoring_project_scoring_jobs).where('autoscoring_projects.id = ? AND autoscoring_project_scoring_jobs.is_test_scoring = ?', self.id, false).count
    # AutoscoringProjectScoringJob.where('autoscoring_project_id = ? AND is_test_scoring = ? AND autoscoring_project_job_status_id = ?', self.id, false, 3).count
    self.autoscoring_project_scoring_jobs.where("is_test_scoring = ? AND autoscoring_project_job_status_id = ?", false, 3).count
  end

  def update_autoscoring_project_active_status_variable_counts
    is_active = true
    autoscoring_project = AutoscoringProject.includes(:autoscoring_project_geodemographic_variables, :autoscoring_project_other_variables).find(id)
    autoscoring_project.autoscoring_project_geodemographic_variables.each do |geodemographic_variable|
      if geodemographic_variable.coefficient_value.present? && !geodemographic_variable.has_uploaded_counts?
        is_active = false
        break
      end
    end
    if is_active
      autoscoring_project.autoscoring_project_other_variables.each do |other_variable|
        if other_variable.coefficient_value.present? && !other_variable.has_uploaded_counts?
          is_active = false
          break
        end
      end
    end

    self.update_column(:active, is_active)
  end


  def self.filter(user_id)
    user = User.find(user_id)
    if user.is_analyst?
      scoped
    elsif user.is_administrator?
      scoped
    else
      nil
    end
  end

  def other_variables_in_client_input_file
    # Rails.cache.fetch([:autoscoring_project, id, :other_variables_in_client_input_file, updated_at], expires_in:  1.month) do
    AutoscoringProjectOtherVariable.includes(:variable).where('autoscoring_project_other_variables.autoscoring_project_id = ? AND variables.in_client_input_file = true', id)
    # end
  end

  def other_variables_not_in_client_input_file
    # Rails.cache.fetch([:autoscoring_project, id, :other_variables_not_in_client_input_file, updated_at], expires_in: 1.month) do
    AutoscoringProjectOtherVariable.includes(:variable).where('autoscoring_project_other_variables.autoscoring_project_id = ? AND variables.in_client_input_file = false', id)
    # end
  end

  def can_accept_new_scoring_job?
    if self.autoscoring_project_scoring_jobs.size
      true
    elsif  self.current_autoscoring_project_scoring_job.completed?
      true
    else
      false
    end
  end

  def current_autoscoring_project_scoring_job
    self.autoscoring_project_scoring_jobs.limit(1).last || nil
  end

  def split_output_by_column_name_criteria
    self.split_output_by_column_name.empty? ? "* No Split Output By Column Criteria Specified for Auto-Scoring Project." : self.split_output_by_column_name
  end

  def conversion_criteria
    conversion_criteria_text = ""
    s = ""
    autoscoring_project_conversion_columns = self.autoscoring_project_conversion_columns.includes(:autoscoring_project_conversion_values)

    if autoscoring_project_conversion_columns.present?
      autoscoring_project_conversion_columns.each do |column|
        s+= "* Column Name: *" + column.name.upcase + "*\n"
        s+= "** Description: " + column.description + "\n"
        s+= "** Filter Value(s)" + "\n"
        column.autoscoring_project_conversion_values.each_with_index do |value, index|
          s+= "*** Conversion #{index + 1}: '" + value.from_value + "' to '" + value.to_value + "'\n"
          s+= "**** Description: " + value.description + "\n"
        end
        s+= "\n"
      end
      conversion_criteria_text = s
    else
      conversion_criteria_text = "* No Conversion Criteria Specified for Auto-Scoring Project."
    end
  end

  def filter_criteria
    filter_criteria_text = ""
    s = ""
    autoscoring_filter_columns = self.autoscoring_project_filter_columns.includes(:autoscoring_project_filter_values)

    if autoscoring_filter_columns.present?
      autoscoring_filter_columns.each do |column|
        s+= "* Column Name: *" + column.name.upcase + "*\n"
        s+= "** Description: " + column.description + "\n"
        column.filter_to_include_these_values? ? s+= "** Filter Method: Include Values" + "\n" : s+= "** Filter Method: Exclude Values" + "\n"
        s+= "** Filter Value(s)" + "\n"
        column.autoscoring_project_filter_values.each_with_index do |value, index|
          s+= "*** Filter Value: '" + value.name + "'\n"
          s+= "**** Description: " + value.description + "\n"
        end
        s+= "\n"
      end
      filter_criteria_text = s
    else
      filter_criteria_text = "* No Filter Criteria Specified for Auto-Scoring Project."
    end
  end

  def institution_display_name
    self.institution ? self.institution.auto_complete_name : nil
  end

  def institution_display_name=(name)
    # extract unitid from between the parentheseis
    unitid = name.scan(/\(([^)]+)\)/)
    if name.present? && !name.nil?
      self.institution = Institution.find_by_unitid(unitid)
    else
      self.institution = nil
    end
  end

  def state
    active? ? ACTIVE : INACTIVE
  end

  def touch
    self.updated_at = Time.now
    self.autoscoring_project_scoring_jobs.each do |job|
      job.touch
      job.autoscoring_project_scoring_job_input_files.each do |file|
        file.touch
      end
      job.autoscoring_project_scoring_job_output_files.each do |file|
        file.touch
      end
    end
  end

  private

  def strip_whitespace
    self.client_opportunity_id_column_name = self.client_opportunity_id_column_name.strip
    self.em_person_id_column_name = self.em_person_id_column_name.strip
    self.em_opportunity_id_column_name = self.em_opportunity_id_column_name.strip
    self.postal_code_column_name = self.postal_code_column_name.strip
    self.state_column_name = self.state_column_name.strip
    self.split_output_by_column_name = self.split_output_by_column_name.strip
  end

  def clear_irrelevent_id_column_name_fields_by_em_configured_flag
    if self.is_em_configured
      self.client_opportunity_id_column_name = ""
    else
      self.em_opportunity_id_column_name = ""
      self.em_person_id_column_name = ""
    end
  end

  def default_values
    if self.new_record?
      self.em_person_id_column_name = "EMPersonID"
      self.em_opportunity_id_column_name = "EMOpportunityID"
    end
  end

  def deactivate_other_institution_autoscoring_projects
    if self.active? && [self.active_changed? || [self.autoscoring_project_status_id ==  3 && self.autoscoring_project_status_id_changed?]]
      AutoscoringProject.update_all("active = 0", ['institution_id = ? && id <> ? && is_em_configured = ?', self.institution_id, self.id, self.is_em_configured])
    end
  end


end
