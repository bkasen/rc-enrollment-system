class PostalCode < ActiveRecord::Base
  attr_accessible :country_id, :name, :state_id

  validates :country, :presence => true
  validates :name, :presence => true
  validates :state, :presence => true


  belongs_to :country
  belongs_to :state

  has_many :institutions
  has_many :postal_codes_years
  has_many :autoscoring_project_scoring_job_records

end
