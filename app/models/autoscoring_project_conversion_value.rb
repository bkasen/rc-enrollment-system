class AutoscoringProjectConversionValue < ActiveRecord::Base
  attr_accessible :autoscoring_project_conversion_column_id, :description, :from_value, :to_value
  
  belongs_to :autoscoring_project_conversion_column
  
  validates :from_value, :presence => true
  validates :to_value, :presence => true

  default_scope order('from_value asc')
  
  before_validation :strip_whitespace
  
  private
  
  def strip_whitespace
     self.name = self.name.strip
   end
   
end
