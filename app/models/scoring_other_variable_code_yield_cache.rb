class ScoringOtherVariableCodeYieldCache
  def initialize
    # @_scoring_variable_code_yield_cache = GoogleHashDenseLongToRuby.new
    @_scoring_other_variable_code_yield_cache = Hash.new
    
  end

  def contains?(key_value)
    @_scoring_other_variable_code_yield_cache.has_key?(key_value.join("/").to_s.to_sym)
  end
  
  def get(key_value)
    @_scoring_other_variable_code_yield_cache[key_value.join("/").to_s.to_sym]
  end

  def put(key_value, value)
    @_scoring_other_variable_code_yield_cache[key_value.join("/").to_s.to_sym] = value
  end
end