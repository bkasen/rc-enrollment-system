class AutoscoringProjectFilterColumn < ActiveRecord::Base
  attr_accessible :autoscoring_project_id, :description, :name, :autoscoring_project_filter_values_attributes, :filter_to_include_these_values, :filter_to_include_these_values_radio
  
  belongs_to :autoscoring_project
  
  has_many :autoscoring_project_filter_values, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_filter_values, :allow_destroy => true
  
  # validates :autoscoring_project, :presence => true
  validates :name, :presence => true
  
  before_validation :strip_whitespace
  
  def filter_to_include_these_values_radio
    self.filter_to_include_these_values? ? "Include These Values" : "Exclude These Values"
  end
  
  def filter_to_include_these_values_radio=(value)
    if value == "Include These Values"
      self.filter_to_include_these_values = true
    else
      self.filter_to_include_these_values = false
    end
  end
  
  private
  
  def strip_whitespace
     self.name = self.name.strip
   end
  
end
