class VariableType < ActiveRecord::Base
  attr_accessible :name
  
  validates :name, :presence => true
  
  has_many :inquiry_variables
  
end
