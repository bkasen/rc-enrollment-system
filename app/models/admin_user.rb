class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
  :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name
  # attr_accessible :title, :body

  validates :email, :presence => {:message => "Email is required"}, :uniqueness => {:message => "Email address is already used"}

  has_many :file_imports
  has_many :autoscoring_projects
  
  

  after_create :invite

  def invite
    self.reset_password_token = AdminUser.reset_password_token
    self.reset_password_sent_at = Time.now
    self.save
    UserMailer.invite_admin_user(id).deliver
  end

  def reset_password
    UserMailer.reset_admin_user_password(id).deliver
  end

  def has_signed_in_before?
    self.sign_in_count > 0 ? true : false
  end

  def password_required?
    new_record? ? false : super
  end
  
  def name
    "#{self.first_name} #{self.last_name}"
  end

end
