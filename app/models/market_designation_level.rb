class MarketDesignationLevel < ActiveRecord::Base
  attr_accessible :name
  
  validates :name, :presence => true
  
  has_many :autoscoring_projects
  
end
