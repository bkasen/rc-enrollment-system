class AutoscoringProjectScoringJobRecord < ActiveRecord::Base
  attr_accessible :autoscoring_project_scoring_job_id, :client_opportunity_id, :em_opportunity_id, :em_person_id, :percentile_rank, :postal_code_id, :probability_score, :rank, :sub_group

  belongs_to :autoscoring_project_scoring_job
  belongs_to :postal_code

  validates :autoscoring_project_scoring_job, :presence => true
  validates :probability_score, :presence => true
  validates :percentile_rank, :presence => true
  validates :rank, :presence => true
  
end
