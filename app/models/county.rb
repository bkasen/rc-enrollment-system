class County < ActiveRecord::Base
  attr_accessible :country_id, :name, :state_id
  
  validates :country, :presence => true
  validates :state, :presence => true
  validates :name, :presence => true
  
  belongs_to :country
  belongs_to :state
  
  has_many :postal_code_years
  
end
