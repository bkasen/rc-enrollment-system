class AutoscoringProjectStatus < ActiveRecord::Base
  attr_accessible :name
  
  has_many :autoscoring_projects
end
