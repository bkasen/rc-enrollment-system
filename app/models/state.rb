class State < ActiveRecord::Base
  attr_accessible :abbreviation, :country_id, :name
  
  validates :name, :presence => true
  validates :abbreviation, :presence => true
  validates :country, :presence => true
  
  belongs_to :country
  
  has_many :institutions
  has_many :counties
  has_many :postal_codes
  
end
