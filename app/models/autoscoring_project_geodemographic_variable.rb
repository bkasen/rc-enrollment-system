class AutoscoringProjectGeodemographicVariable < ActiveRecord::Base
  attr_accessible :autoscoring_project_id, :coefficient_value, :variable_id, :include_in_output, :wald_score, :has_uploaded_counts
  
  belongs_to :autoscoring_project
  belongs_to :variable
  
  has_many :autoscoring_project_geodemographic_variable_counts

  
  # validates :autoscoring_project, :presence => true
  validates :variable, :presence => true #only geodemographic type variables!
  validates :coefficient_value, :numericality => true, :allow_nil => true
  validates :wald_score, :numericality => true, :allow_nil => true
  
  scope :include, where(:include_in_output => true)
  scope :exclude, where(:include_in_output => false)
  
  scope :present, where(:has_uploaded_counts => true)
  scope :missing, where(:has_uploaded_counts => false)


  INCLUDE = "Yes"
  EXCLUDE = "No"
  
  COUNTS_PRESENT = "Present"
  COUNTS_MISSING = "Missing"
  
  def included
    include_in_output? ? INCLUDE : EXCLUDE
  end
  
  def counts_present
    has_uploaded_counts? ? COUNTS_PRESENT : COUNTS_MISSING
  end

end
