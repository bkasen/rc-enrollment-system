class ProjectManager < ActiveRecord::Base
  attr_accessible :email, :first_name, :last_name

  validates :email, :presence => true, :uniqueness => {:message => "Email address is already used"},  :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
  validates :first_name, :presence => true
  validates :last_name, :presence => true


  has_many :autoscoring_projects

  def name
    self.first_name + " " + self.last_name
  end


end
