class Variable < ActiveRecord::Base
  attr_accessible :default, :name, :variable_type_id, :abbreviation, :is_date, :in_client_input_file

  validates :name, :presence => true, :uniqueness => true
  validates :variable_type, :presence => true
  validates :abbreviation,  :uniqueness => true, :allow_nil => true

  belongs_to :variable_type

  has_many :geodemographic_variables
  has_many :autoscoring_project_geodemographic_variables
  has_many :autoscoring_project_other_variables
  
  
  default_scope :order => "name DESC"

end
