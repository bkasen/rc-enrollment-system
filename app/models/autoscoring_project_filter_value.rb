class AutoscoringProjectFilterValue < ActiveRecord::Base
  attr_accessible :autoscoring_project_filter_column_id, :description, :name

  belongs_to :autoscoring_project_filter_column


  # uses special reg ex to allow for space, numbers, or letters
  validates_format_of :name, :with => /^[a-zA-Z0-9\-_.() ]*$/i
  # validates_inclusion_of :is_to_be_included, :in => ["Filter To Include" : "Filter to Exclude"]

  default_scope order('name asc')

  before_validation :strip_whitespace



  private

  def strip_whitespace
    self.name = self.name.strip
  end
end
