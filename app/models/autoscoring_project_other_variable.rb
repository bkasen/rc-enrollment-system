class AutoscoringProjectOtherVariable < ActiveRecord::Base
  attr_accessible :autoscoring_project_id, :coefficient_value, :date_format_id, :include_in_output, :name, :secondary_column_name, :tertiary_column_name, :variable_id, :wald_score, :has_uploaded_counts, :has_uploaded_translations

  belongs_to :autoscoring_project
  belongs_to :variable
  belongs_to :date_format

  has_many :autoscoring_project_other_variable_counts
  has_many :autoscoring_project_other_variable_translations

  validates :variable, :presence => true #only geodemographic type variables!
  validates :date_format, :presence => true, :if => :is_date?
  validates :coefficient_value, :presence => true, :numericality => true
  validates :wald_score, :numericality => true, :allow_nil => true
  validates :name, :presence => true
  validates :secondary_column_name, :presence => true, :if => :has_third_column_name?
  
  before_validation :strip_whitespace

  scope :include, where(:include_in_output => true)
  scope :exclude, where(:include_in_output => false)
  
  scope :present, where(:has_uploaded_counts => true)
  scope :missing, where(:has_uploaded_counts => false)

  INCLUDE = "Yes"
  EXCLUDE = "No"
  
  COUNTS_PRESENT = "Present"
  COUNTS_MISSING = "Missing"
  
  def has_third_column_name?
    !self.tertiary_column_name.blank?
  end

  def included
    include_in_output? ? INCLUDE : EXCLUDE
  end
  
  def counts_present
    has_uploaded_counts? ? COUNTS_PRESENT : COUNTS_MISSING
  end

  def is_date?
    if self.variable then
      self.variable.is_date?
    else
      false
    end
  end
  
  private
  
  def strip_whitespace
    self.name = self.name.strip
    self.secondary_column_name = self.secondary_column_name.strip
    self.tertiary_column_name = self.tertiary_column_name.strip
  end
  
end
