class AutoscoringProjectOtherVariableTranslation < ActiveRecord::Base
  attr_accessible :autoscoring_project_other_variable_id, :code, :description

  validates :autoscoring_project_other_variable, :presence => true
  validates :code, :presence => true
  validates :description, :presence => {:message => " value can't be blank. Please check to ensure you have included a column titled Description"}


  belongs_to :autoscoring_project_other_variable
end
