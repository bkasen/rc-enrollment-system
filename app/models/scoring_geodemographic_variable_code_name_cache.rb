class ScoringGeodemographicVariableCodeNameCache 
  def initialize
    # @_scoring_geodemographic_variable_code_name_cache = GoogleHashDenseLongToRuby.new
    @_scoring_geodemographic_variable_code_name_cache = Hash.new
  end

  def contains?(key_value)
    @_scoring_geodemographic_variable_code_name_cache.has_key?(key_value.to_s.to_sym)
  end
  
  def get(key_value)
    @_scoring_geodemographic_variable_code_name_cache[key_value.to_s.to_sym]
  end

  def put(key_value, value)
    @_scoring_geodemographic_variable_code_name_cache[key_value.to_s.to_sym] = value
  end
end