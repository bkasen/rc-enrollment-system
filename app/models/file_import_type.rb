class FileImportType < ActiveRecord::Base
  attr_accessible :institution_specific, :name
  
  validates :name, :presence => true

  has_many :autoscoring_project_scoring_job_input_files
  has_many :file_imports
end
