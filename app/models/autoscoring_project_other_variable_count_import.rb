class AutoscoringProjectOtherVariableCountImport
  # switch to ActiveModel::Model in Rails 4
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :file, :autoscoring_project_other_variable_id, :delete_existing_records
  
  validates :file, :presence => true
  validates :autoscoring_project_other_variable_id, :presence => true
  validates :delete_existing_records, :presence => true
  

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def delete_existing_records?
    !!@delete_existing_records
  end

  def persisted?
    false
  end

  def save

    begin
      ActiveRecord::Base.transaction do
        if self.delete_existing_records.to_bool
          AutoscoringProjectOtherVariableCount.delete_all(["autoscoring_project_other_variable_id = ?", self.autoscoring_project_other_variable_id])
          autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find(self.autoscoring_project_other_variable_id)
          autoscoring_project_other_variable.update_attributes(:has_uploaded_counts => false)
          
        end

        if imported_counts.map(&:valid?).all?

          #imported_counts.each(&:save!)
          AutoscoringProjectOtherVariableCount.import imported_counts

          true
        else
          imported_counts.each_with_index do |count, index|
            count.errors.full_messages.each do |message|
              errors.add :base, "Row #{index+2}: #{message}"
            end
          end
          false
        end

      end
    end

  end

  def imported_counts
    @imported_counts ||= load_imported_counts
  end

  def load_imported_counts
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find(self.autoscoring_project_other_variable_id)
    (2..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      count = AutoscoringProjectOtherVariableCount.find_by_autoscoring_project_other_variable_id_and_name(autoscoring_project_other_variable.id, row["Code"])
      if count.nil? then
        count = AutoscoringProjectOtherVariableCount.new
        count.autoscoring_project_other_variable_id = autoscoring_project_other_variable.id
        if row["Description"].to_s.is_numeric? && autoscoring_project_other_variable.variable.id == 16 # HS ceeb codes variable ID
          description_string = row["Description"].to_i.to_s # convert to integer to remove leading zeros before appending appropriate zeros
          t_description = sprintf '%06d', description_string # append leading zeros if not present
          count.name = t_description
        elsif row["Description"].to_s.is_numeric? && autoscoring_project_other_variable.variable.id == 11 #contact date codes variable ID
          description_string = row["Description"].to_i.to_s # convert to integer to remove leading zeros before appending appropriate zeros
          t_description = sprintf '%02d', description_string # append leading zeros if not present
          count.name = t_description
        else
          count.name = t_description = row["Description"]
        end
        count.enrolled = row["Enrolled"] || 0
        count.total = row["Total"]
      else
        count.enrolled = row["Enrolled"]
        count.total = row["Total"]
      end
      count
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename).downcase
    when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
