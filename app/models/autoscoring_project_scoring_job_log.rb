class AutoscoringProjectScoringJobLog < ActiveRecord::Base
  attr_accessible :autoscoring_project_job_status_id, :autoscoring_project_scoring_job_id, :column_count, :conversion_criteria, :details, :error_details, :filter_criteria, :row_count, :rows_filtered_out, :split_output_by_column_name

  # order of scoring job logs is set to ASC so the last file is always the most recent version
  default_scope order('created_at ASC')
  
  belongs_to :autoscoring_project_scoring_job
  belongs_to :autoscoring_project_job_status
  
  validates :autoscoring_project_scoring_job, :presence => true
  validates :autoscoring_project_job_status, :presence => true
  validates :column_count, :numericality => true, :allow_nil => true
  validates :row_count, :numericality => true, :allow_nil => true
  validates :rows_filtered_out, :numericality => true
  validates :filter_criteria, :presence => true
  validates :conversion_criteria, :presence => true
  validates :split_output_by_column_name, :presence => true
  validates :details, :presence => true
  
  def row_inclusion_percentage
    self.row_count.nil? ? 0 : (( (self.row_count.to_f - self.rows_filtered_out) / self.row_count )  * 100)
  end
end
