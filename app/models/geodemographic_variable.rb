class GeodemographicVariable < ActiveRecord::Base
  attr_accessible :name, :variable_id


  has_many :mdgs, :foreign_key => "mdg_id", :class_name => "PostalCodesYears"
  has_many :adgs, :foreign_key => "adg_id", :class_name => "PostalCodesYears"
  has_many :eths, :foreign_key => "eth_id", :class_name => "PostalCodesYears"
  has_many :hvls, :foreign_key => "hvl_id", :class_name => "PostalCodesYears"
  has_many :mfis, :foreign_key => "mfi_id", :class_name => "PostalCodesYears"
  has_many :mhbs, :foreign_key => "mhb_id", :class_name => "PostalCodesYears"
  has_many :pops, :foreign_key => "pop_id", :class_name => "PostalCodesYears"

  belongs_to :variable


  validates :variable, :presence => true
  validates :name, :presence => true, :uniqueness => true


end
