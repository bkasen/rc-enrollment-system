class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :job_title_id, :first_name, :last_name, :active, :job_title_name
  # attr_accessible :title, :body

  has_many :file_imports
  has_many :project_managers, :foreign_key => "project_manager_id", :class_name => "AutoscoringProjects"

  belongs_to :job_title

  validates :email, :presence => {:message => "Email is required"}, :uniqueness => {:message => "Email address is already used"}
  validates :password, :presence => true, :if => :password_required?
  validates :job_title_name, :presence => true
  validates :first_name, :presence => true
  validates :last_name, :presence => true

  scope :active, where(:active => true)
  scope :inactive, where(:active => false)


  ACTIVE = "active"
  INACTIVE = "inactive"

  after_create :invite

  def invite
    # original_token = self.reset_password_token
    # self.reset_password_token = Devise.token_generator.digest(User, :reset_password_token, original_token)
    self.reset_password_token = User.reset_password_token
    self.reset_password_sent_at = Time.now
    self.save
    UserMailer.invite_general_user(id).deliver
  end

  def has_signed_in_before?
    self.sign_in_count > 0 ? true : false
  end

  def is_administrator?
    self.job_title_id == 3
  end

  def is_analyst?
    self.job_title_id == 2
  end

  def is_project_manager?
    self.job_title_id == 1
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def display_name
    if self.present?
      "#{self.first_name} #{self.last_name} (#{email})"
    else
      "N/A"
    end
  end

  def state
    active? ? ACTIVE : INACTIVE
  end


  def job_title_name
    job_title.try(:name)
  end

  def job_title_name=(name)
    self.job_title = JobTitle.find_or_create_by_name(name) if name.present?
  end
  
  def active_for_authentication?
    super && self.active # i.e. super && self.is_active
  end

  def inactive_message
    "Sorry, this account has been deactivated."
  end

  private

  def password_required?
    if !encrypted_password.blank? # If the person already has a pass, only validate if they are updating pass
      password.present? || password_confirmation.present?
    end
  end
  


end
