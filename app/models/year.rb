class Year < ActiveRecord::Base
  attr_accessible :name, :value, :has_geodemographic_data

  default_scope order('value DESC')

  validates :name, :presence => true
  validates :value, :presence => true

  has_many :postal_codes_years
  has_many :cohort_years, :foreign_key => "cohort_year_id", :class_name => "AutoscoringProjectScoringJobs"
  has_many :geodemographic_years, :foreign_key => "geodemographic_year_id", :class_name => "AutoscoringProjects"
  has_many :report_years, :foreign_key => "report_year_id", :class_name => "AutoscoringProjects"


end
