class GeodemographicVariablesYear < ActiveRecord::Base
  attr_accessible :active, :geodemographic_variable_id, :lower_threshold, :upper_threshold, :year_id
end
