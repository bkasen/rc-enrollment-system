class AutoscoringProjectConversionColumn < ActiveRecord::Base
  attr_accessible :autoscoring_project_id, :description, :name, :autoscoring_project_conversion_values_attributes
  
  belongs_to :autoscoring_project
  
  has_many :autoscoring_project_conversion_values, :dependent => :destroy
  accepts_nested_attributes_for :autoscoring_project_conversion_values, :allow_destroy => true
  
  validates :name, :presence => true
  
  before_validation :strip_whitespace
  
  private
  
  def strip_whitespace
     self.name = self.name.strip
   end
   
end
