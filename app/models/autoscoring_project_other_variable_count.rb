class AutoscoringProjectOtherVariableCount < ActiveRecord::Base
  attr_accessible :autoscoring_project_other_variable_id, :enrolled, :name, :total

  validates :autoscoring_project_other_variable, :presence => true
  validates :enrolled, :numericality => true, :allow_nil => true
  validates :name, :presence => true
  validates :total, :numericality => true, :allow_nil => true
  
  validates_uniqueness_of :name, scope: :autoscoring_project_other_variable_id

  belongs_to :autoscoring_project_other_variable

  before_save :set_default_values

  def yield_as_decimal
    # Rails.cache.fetch(["yield_as_decimal", "enrolled", self.enrolled, "total", self.total], expires_in: 0) do
    (self.enrolled.to_f / self.total)
    # end
  end

  def yield_as_percentage
    # Rails.cache.fetch(["yield_as_percentage", "enrolled", self.enrolled, "total", self.total], expires_in: 0) do
    (self.enrolled.to_f / self.total) * 100
    # end
  end

  private

  def set_default_values
    self.enrolled = 0 if self.enrolled.nil?
    self.total = 0 if self.total.nil?
  end

end
