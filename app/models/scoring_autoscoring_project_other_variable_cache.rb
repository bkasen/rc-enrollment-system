class ScoringAutoscoringProjectOtherVariableCache 
  def initialize
    # @_scoring_autoscoring_project_other_variable_cache = GoogleHashDenseLongToRuby.new
    @_scoring_autoscoring_project_other_variable_cache = Hash.new
    
  end

  def contains?(key_value)
    @_scoring_autoscoring_project_other_variable_cache.has_key?(key_value.to_s.to_sym)
  end
  
  def get(key_value)
    @_scoring_autoscoring_project_other_variable_cache[key_value.to_s.to_sym]
  end

  def put(key_value, value)
    @_scoring_autoscoring_project_other_variable_cache[key_value.to_s.to_sym] = value
  end
end