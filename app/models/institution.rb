class Institution < ActiveRecord::Base
  attr_accessible :city, :country_id, :display_name, :name, :postal_code_id, :state_id, :unitid, :postal_code_name, :emp_organization_name

  validates :unitid, :presence => true
  validates :name, :presence => true
  validates :display_name, :presence => true
  # validates :postal_code, :presence => true
  validates :state, :presence => true
  validates :country, :presence => true

  has_many :autoscoring_projects
  has_many :file_imports

  belongs_to :country
  belongs_to :postal_code
  belongs_to :state
  
  def auto_complete_name
    "#{name} - #{state.name} (#{unitid})"
  end
  
  def postal_code_name
    self.postal_code ? self.postal_code.name : nil
  end

  def postal_code_name=(name)
    if name.present? && !name.nil?
      self.postal_code = PostalCode.find_by_name(name)
    else
      self.postal_code = nil
    end
  end

end
