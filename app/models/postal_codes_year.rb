class PostalCodesYear < ActiveRecord::Base
  attr_accessible :high_school_age_population, :case_district, :county_id, :geo_name, :postal_code_id, :year_id, :residential, :mdg_id, :adg_id, :eth_id, :hvl_id, :mfi_id, :mhb_id, :pop_id
  
  validates :postal_code, :presence => true
  validates :year, :presence => true
  validates :county, :presence => true
  validates :high_school_age_population, :presence => true
  validates :case_district, :presence => true
  validates :geo_name, :presence => true
  validates :postal_code, :presence => true
  
  belongs_to :county
  belongs_to :postal_code
  belongs_to :year
  #geodemographic variables
  belongs_to :mdg, :class_name => "GeodemographicVariable"
  belongs_to :adg, :class_name => "GeodemographicVariable"
  belongs_to :eth, :class_name => "GeodemographicVariable"
  belongs_to :hvl, :class_name => "GeodemographicVariable"
  belongs_to :mfi, :class_name => "GeodemographicVariable"
  belongs_to :mhb, :class_name => "GeodemographicVariable"
  belongs_to :pop, :class_name => "GeodemographicVariable"
  
end
