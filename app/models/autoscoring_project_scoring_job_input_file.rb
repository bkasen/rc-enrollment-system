class AutoscoringProjectScoringJobInputFile < ActiveRecord::Base
  # switch to ActiveModel::Model in Rails 4
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessible :autoscoring_project_scoring_job_id, :file_name, :processed, :file_import_type_id
  mount_uploader :file_name, FileUploader
  before_destroy :delete_file
  # order of input *files* is set to ASC so the last file is always the most recent version
  # default_scope order('created_at ASC')

  belongs_to :autoscoring_project_scoring_job
  belongs_to :file_import_type


  validates :file_name, :presence => true

  def file_type_extension
    File.extname(self.file_name.path)
  end

  def delete_file
    remove_file_name!
  end

  def delete_file_from_file_system
    File.delete(file_name) if File.exist?(file_name)
  end

end
