class AutoscoringProjectGeodemographicVariableCount < ActiveRecord::Base
  attr_accessible :autoscoring_project_geodemographic_variable_id, :enrolled, :name, :total

  belongs_to :autoscoring_project_geodemographic_variable

  validates :autoscoring_project_geodemographic_variable_id, :presence => true
  validates :name, :presence => true
  validates :enrolled, :presence => true
  validates :total, :presence => true
  
  
  validates_uniqueness_of :name, scope: :autoscoring_project_geodemographic_variable_id

  before_save :set_default_values

  def yield_as_decimal
    # Rails.cache.fetch([:yield_as_decimal, "enrolled", self.enrolled, "total", self.total], expires_in: 0) do
    (self.enrolled.to_f / self.total)
    # end
  end

  def yield_as_percentage
    # Rails.cache.fetch(["yield_as_percentage", "enrolled", self.enrolled, "total", self.total], expires_in: 0) do
    (self.enrolled.to_f / self.total) * 100
    # end
  end

  private

  def set_default_values
    self.enrolled = 0 unless self.enrolled
    self.total = 0 unless self.total
  end

end
