class ScoringPostalCodeCache 
  def initialize
    # @_scoring_postal_code_cache = GoogleHashDenseLongToRuby.new
    @_scoring_postal_code_cache = Hash.new
    
  end

  def contains?(key_value)
    @_scoring_postal_code_cache.has_key?(key_value.to_sym)
  end
  
  def get(key_value)
    @_scoring_postal_code_cache[key_value.to_sym]
  end

  def put(key_value, value)
    @_scoring_postal_code_cache[key_value.to_sym] = value
  end
end