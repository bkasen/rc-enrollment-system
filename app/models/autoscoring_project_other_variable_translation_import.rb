class AutoscoringProjectOtherVariableTranslationImport
  # switch to ActiveModel::Model in Rails 4
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :file, :autoscoring_project_other_variable_id, :delete_existing_records

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end
  
  def delete_existing_records?
    !!@delete_existing_records
  end

  def persisted?
    false
  end

  # def save
  #   if imported_translations.map(&:valid?).all?
  #     imported_translations.each(&:save!)
  #     true
  #   else
  #     imported_translations.each_with_index do |translation, index|
  #       translation.errors.full_messages.each do |message|
  #         errors.add :base, "Row #{index+2}: #{message}"
  #       end
  #     end
  #     false
  #   end
  # end
  
  def save

    begin
      ActiveRecord::Base.transaction do
        if self.delete_existing_records?
          AutoscoringProjectOtherVariableTranslation.delete_all(["autoscoring_project_other_variable_id = ?", self.autoscoring_project_other_variable_id])
        end

        if imported_translations.map(&:valid?).all?
          #imported_counts.each(&:save!)
          AutoscoringProjectOtherVariableTranslation.import imported_translations
          true
        else
          imported_counts.each_with_index do |count, index|
            count.errors.full_messages.each do |message|
              errors.add :base, "Row #{index+2}: #{message}"
            end
          end
          false
        end

      end
    end

  end

  def imported_translations
    @imported_translations ||= load_imported_translations
  end

  def load_imported_translations
    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).map do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      translation = AutoscoringProjectOtherVariableTranslation.find_by_autoscoring_project_other_variable_id_and_code_and_description(self.autoscoring_project_other_variable_id, row["Code"], row["Description"])
      if translation.nil? then
        translation = AutoscoringProjectOtherVariableTranslation.new
        translation.autoscoring_project_other_variable_id = self.autoscoring_project_other_variable_id
        translation.description = row["Description"]
        translation.code = row["Code"]
        translation
      else
        translation.description = row["Description"]
        translation.code = row["Code"]
        translation
      end
    end
  end

  def open_spreadsheet
    case File.extname(file.original_filename).downcase
    when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
