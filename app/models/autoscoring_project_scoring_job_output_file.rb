class AutoscoringProjectScoringJobOutputFile < ActiveRecord::Base
  attr_accessible :autoscoring_project_scoring_job_id, :file_name, :split_value

  belongs_to :autoscoring_project_scoring_job

  validates :autoscoring_project_scoring_job, :presence => true
  validates :file_name, :presence => true
  mount_uploader :file_name, FileDownloader
  before_destroy :delete_file


  # def file_path
  #   self.file_name
  # end

  def delete_file_from_file_system
    File.delete(file_name) if File.exist?(file_name)
  end
  
  def delete_file
    remove_file_name!
  end

end
