class ScoringPercentileRankCache 
  def initialize
    # @_scoring_percentile_rank_cache = GoogleHashDenseLongToRuby.new
    @_scoring_percentile_rank_cache = Hash.new
    
  end

  def contains?(key_value)
    @_scoring_percentile_rank_cache.has_key?(key_value.to_s.to_sym)
  end
  
  def get(key_value)
    @_scoring_percentile_rank_cache[key_value.to_s.to_sym]
  end

  def put(key_value, value)
    @_scoring_percentile_rank_cache[key_value.to_s.to_sym] = value
  end
end