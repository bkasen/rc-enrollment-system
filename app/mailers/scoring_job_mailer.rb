class ScoringJobMailer < ActionMailer::Base
  default :from => 'RuffaloCODY EM Auto-Score System <noreply@austengroup.com>'
  def scoring_job_completed_with_error(scoring_job_id)
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(scoring_job_id)
    if @autoscoring_project_scoring_job.encountered_errors?
      @autoscoring_project_scoring_job_log = @autoscoring_project_scoring_job.last_autoscoring_project_scoring_job_log
      mail(:to => "#{@autoscoring_project_scoring_job.user.name} <#{@autoscoring_project_scoring_job.user.email}>", :subject => "#{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Scoring Job Encountered an Error.")
    end
  end

  def scoring_job_completed_successfully(scoring_job_id)
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(scoring_job_id)
    if @autoscoring_project_scoring_job.completed?
      # Notify User who created Scoring Job
      @autoscoring_project_scoring_job_log = @autoscoring_project_scoring_job.last_autoscoring_project_scoring_job_log
      if @autoscoring_project_scoring_job.user.is_administrator?
        @user = @autoscoring_project_scoring_job.user
        mail(:to => "#{@user.name} <#{@user.email}>", :subject => "#{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Scoring Job Completed Successfully.")
      else
        @user = @autoscoring_project_scoring_job.user

        mail(:to => "#{@user.name} <#{@user.email}>", :subject => "#{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Scoring Job Completed Successfully.")
        ScoringJobMailer.scoring_job_completed_project_manager_notification(@autoscoring_project_scoring_job.id).deliver
        ScoringJobMailer.scoring_job_completed_admin_user_notification(@autoscoring_project_scoring_job.id).deliver if @autoscoring_project_scoring_job.autoscoring_project.admin_user.present?
      end
    end
  end

  def scoring_job_completed_admin_user_notification(scoring_job_id)
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(scoring_job_id)
    @autoscoring_project_scoring_job_log = @autoscoring_project_scoring_job.last_autoscoring_project_scoring_job_log
    @admin_user =  @autoscoring_project_scoring_job.autoscoring_project.admin_user
    mail(:to => "#{@admin_user.name} <#{@admin_user.email}>", :subject => "Auto-Score Notification - New #{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Scoring Job Completed Successfully - Detailed Report")
  end

  def scoring_job_completed_project_manager_notification(scoring_job_id)
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(scoring_job_id)
    @user =  @autoscoring_project_scoring_job.autoscoring_project.project_manager
    mail(:to => "#{@user.name} <#{@user.email}>", :subject => "Auto-Score Notification - New #{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Scoring Job Completed Successfully")
  end

  def scoring_job_missing_id_columns_error(scoring_job_id)
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(scoring_job_id)
    @autoscoring_project_scoring_job_log = @autoscoring_project_scoring_job.last_autoscoring_project_scoring_job_log
    # Notify System Admin who created Scoring Job
    mail(:to => "Brian Kasen <brian.kasen@ruffalocody.com>", :subject => "#{@autoscoring_project_scoring_job.autoscoring_project.name_with_configuration} Last Scoring Attempt Was Missing ID columns")

  end

end
