class FileImportMailer < ActionMailer::Base
  
  default :from => 'RuffaloCODY EM Auto-Score System <noreply@austengroup.com>'

    
  def file_import_completed_with_error(file_import_id, admin_user_id, error_details)
    @file_import = FileImport.find(file_import_id)
    @admin_user = AdminUser.find(admin_user_id)
    @error_details = error_details

    mail(:to => "#{@admin_user.name} <#{@admin_user.email}>", :subject => "#{@file_import.name} File Import an Error.")
  end

  def file_import_completed_successfully(file_import_id, admin_user_id)
    @file_import = FileImport.find(file_import_id)
    @admin_user = AdminUser.find(admin_user_id)
    mail(:to => "#{@admin_user.name} <#{@admin_user.email}>", :subject => "#{@file_import.name} File Import Completed Successfully.")

  end


end
