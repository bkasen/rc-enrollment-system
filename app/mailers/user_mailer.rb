class UserMailer < ActionMailer::Base
  default :from => 'RuffaloCODY EM Auto-Score System <noreply@austengroup.com>'
  def invite_general_user(id)
    @user = User.find(id)
    if @user
      mail to: @user.email, subject: "RuffaloCODY EM Auto-Score System Invitation"
    end
  end

  def reset_general_user_password(id)
    # creates a new token and send it with instructions about how to reset the password
    User.find(id).send_reset_password_instructions
  end

  def invite_admin_user(id)
    @admin_user = AdminUser.find(id)
    if @admin_user
      mail to: @admin_user.email, subject: "RuffaloCODY EM Auto-Score Admin System Invitation"
    end
  end

  def reset_admin_user_password(id)
    # creates a new token and send it with instructions about how to reset the password
    AdminUser.find(id).send_reset_password_instructions
  end

end
