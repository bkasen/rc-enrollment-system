ActiveAdmin.register GeodemographicVariable do
  menu :parent => "Variable Items"

  index do
    selectable_column
    column :variable
    column :name
    default_actions
  end

  filter :variable, :as => :select, :collection => proc { Variable.where(:variable_type_id => 1) }
  filter :name

  form do |f|
    f.inputs "Details" do
      f.input :variable,   :as => :select, :collection => Variable.order("name ASC").where("variable_type_id = ? ", 1)
      f.input :name
    end

    f.buttons
  end

end
