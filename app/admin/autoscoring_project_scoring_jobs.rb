ActiveAdmin.register AutoscoringProjectScoringJob do
  config.batch_actions = false

  action_item :only => :index do
    link_to('Download Report of All Scoring Jobs', "#{download_all_scoring_jobs_report_admin_autoscoring_project_scoring_jobs_path(format:'csv')}", :id =>'download_file')
  end

  action_item :only => :show do
    link_to('Clear Unnecessary Scoring Job Records', delete_scoring_job_records_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job), :confirm => "Are you sure?") if autoscoring_project_scoring_job.autoscoring_project_scoring_job_records.any?
  end

  action_item :only => :show do
    link_to('Re-queue Scoring Job', enqueue_file_on_sidekiq_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job), :confirm => "Are you sure?") unless autoscoring_project_scoring_job.in_progress?
  end

  collection_action :download_all_scoring_jobs_report, :method => :get do
    @autoscoring_project_scoring_jobs = AutoscoringProjectScoringJob.includes([:autoscoring_project => :institution], :autoscoring_project_job_status, :user).order("created_at ASC")
    now = DateTime.now
    file_name = "all_scoring_jobs_report_created_on_#{now.strftime('%m %d %Y %H %M %S').to_s}.csv".downcase.tr(' ', '_')
    file_name = file_name.gsub(/[\x00\/\\:\*\?\"<>\|]/, '_')
    respond_to do |format|
      format.html
      format.csv {send_data @autoscoring_project_scoring_jobs.to_csv, :disposition => "attachment;filename=#{file_name}"
      }
    end
  end

  controller do
    # This code is evaluated within the controller class

    def scoped_collection
      AutoscoringProjectScoringJob.includes(:autoscoring_project, :autoscoring_project_job_status, :user)
    end

    # def index
    #   redirect_to admin_autoscoring_projects_path
    # end

    def show
      @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(params[:id])
      if @autoscoring_project_scoring_job.present?
        @autoscoring_project_scoring_job_input_files  = @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_files.order("created_at DESC")
        @autoscoring_project_scoring_job_output_files = @autoscoring_project_scoring_job.autoscoring_project_scoring_job_output_files.order("created_at DESC")
        @autoscoring_project_scoring_job_logs = @autoscoring_project_scoring_job.autoscoring_project_scoring_job_logs.order("created_at DESC")
      else
        redirect_to admin_autoscoring_project_scoring_jobs_path
      end
    end

    def destroy
      @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:autoscoring_project).find(params[:id])
      @autoscoring_project = @autoscoring_project_scoring_job.autoscoring_project
      @autoscoring_project_scoring_job.destroy

      redirect_to admin_autoscoring_project_path(@autoscoring_project)
    end

    def download_all_instituions

    end

  end

  # filter :institution, :as => :select, :collection => proc { Institution.where('value >= ?', 2000) }
  filter :institution_name, :as => :string

  filter :user_email, :as => :select, :collection => proc { User.all}
  filter :autoscoring_project_job_status
  filter :is_test_scoring
  filter :is_public

  form do |f|
    f.inputs "Details" do
      f.input :autoscoring_project_job_status
      f.input :row_count
      f.input :column_count
      f.input :is_public
      f.input :is_test_scoring
      f.input :updated_at
    end

    f.buttons
  end

  index do
    column :id
    column :autoscoring_project, :sortable => "autoscoring_projects.name" do |autoscoring_project_scoring_job|
      link_to autoscoring_project_scoring_job.autoscoring_project.name_with_configuration, admin_autoscoring_project_path(autoscoring_project_scoring_job.autoscoring_project)
    end
    column "Status", :sortable => "autoscoring_project_job_statuses.name" do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.autoscoring_project_job_status.name
    end
    column "Processing Time" do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.processing_time_in_seconds
    end
    column :user, :sortable => "users.first_name"
    column "Scoring Type", :sortable => "is_test_scoring" do |autoscoring_project_scoring_job|
      if autoscoring_project_scoring_job.is_test_scoring && !autoscoring_project_scoring_job.is_public
        "Test Scoring / Private"
      elsif !autoscoring_project_scoring_job.is_test_scoring && !autoscoring_project_scoring_job.is_public
        "Actual Scoring / Private"
      elsif !autoscoring_project_scoring_job.is_test_scoring && autoscoring_project_scoring_job.is_public
        "Actual Scoring / Public"
      end
    end

    column "Input File Row Count", :sortable => "row_count" do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.row_count ? autoscoring_project_scoring_job.row_count : "N/A"
    end
    column "Rows Filtered Out", :sortable => "rows_filtered_out" do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.rows_filtered_out ? autoscoring_project_scoring_job.rows_filtered_out : "N/A"
    end
    column "Rows Processed", :sortable => "rows_processed" do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.rows_processed
    end
    column :number_of_attempts, :sortable => true
    column "# of Output Files", :sortable =>false do |autoscoring_project_scoring_job|
      autoscoring_project_scoring_job.autoscoring_project_scoring_job_output_files.count
    end
    default_actions
  end



  show do
    columns do
      column do
        attributes_table do
          row :id
          row "Current Status" do |autoscoring_project_scoring_job|
            if autoscoring_project_scoring_job.not_started?
              "Scoring Job Scoring Job Process Has Not Started"

            elsif autoscoring_project_scoring_job.in_queue?
              "Scoring Job Successfully Placed In Queue. Waiting For Processing"
            elsif autoscoring_project_scoring_job.in_progress?
              "Currently Processing Scoring Job..."
            elsif autoscoring_project_scoring_job.completed?
              "Scoring Job Processed & Completed Successfully!"
            elsif autoscoring_project_scoring_job.encountered_errors?
              "Error Occurred during Scoring Job Processing!"
            else
              none
            end
          end
          row :is_test_scoring do |autoscoring_project_scoring_job|
            if autoscoring_project_scoring_job.is_test_scoring
              "Yes"
            else
              "No"
            end
          end
          row :is_public do |autoscoring_project_scoring_job|
            if autoscoring_project_scoring_job.is_public
              "Yes"
            else
              "No"
            end
          end
          row :autoscoring_project do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.autoscoring_project.name
          end
          row :institution do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.autoscoring_project.institution.name
          end
          row "Auto-Scoring Project Report Year" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.autoscoring_project.report_year.name
          end
          row "Auto-Scoring Project Geodemographic Year" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.autoscoring_project.geodemographic_year.name
          end
          row "Auto-Scoring Project Project Manager" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.autoscoring_project.project_manager.name
          end
          row "Initial Scoring Job Creator" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.user.name
          end
          row "Auto-Scoring Project Configuration Type" do |autoscoring_project_scoring_job|
            if autoscoring_project_scoring_job.autoscoring_project.is_em_configured?
              "EM Configured Project"
            else
              "Client Configured Project"
            end
          end
          row "Input File Row Count" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.row_count
          end
          row "Input File Column Count" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.column_count
          end
          row "Rows Processed" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.rows_processed
          end

          row :details

          if autoscoring_project_scoring_job.autoscoring_project.is_em_configured?
            row :em_person_id_column_name do |autoscoring_project_scoring_job|
              autoscoring_project_scoring_job.autoscoring_project.em_person_id_column_name
            end
            row :em_opportunity_id_column_name do |autoscoring_project_scoring_job|
              autoscoring_project_scoring_job.autoscoring_project.em_opportunity_id_column_name
            end
          else
            row :client_opportunity_id_column_name do |autoscoring_project_scoring_job|
              autoscoring_project_scoring_job.autoscoring_project.client_opportunity_id_column_name
            end
          end

          row "Number of Scoring Attempts" do |autoscoring_project_scoring_job|
            autoscoring_project_scoring_job.number_of_attempts_count
          end
          row "Last Attempt's Processing Time" do |autoscoring_project_scoring_job|
            "#{autoscoring_project_scoring_job.processing_time_approximate} / #{autoscoring_project_scoring_job.processing_time_exact}"
          end

          panel("Scoring Attempt Log Details") do
            table_for(autoscoring_project_scoring_job_logs) do
              column "Created At", :sortable => :created_at do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.created_at.strftime("%m/%d/%y %I:%M %p %Z")
              end
              column "Attempt's Final Status" do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.autoscoring_project_job_status.name
              end
              column "Row Count" do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.row_count
              end
              column "Column Count" do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.column_count
              end
              column "Rows Filtered out" do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.rows_filtered_out
              end
              column "Error Details" do |autoscoring_project_scoring_job_log|
                autoscoring_project_scoring_job_log.error_details? ? raw(RedCloth.new(autoscoring_project_scoring_job_log.error_details).to_html)  : "No Errors"
              end
              column ""do |autoscoring_project_scoring_job_log|
                link_to  "All Details", admin_autoscoring_project_scoring_job_log_path(autoscoring_project_scoring_job_log)

              end
            end
          end

          panel("Uploaded Input File(s)") do
            table_for(autoscoring_project_scoring_job_input_files) do
              column "Uploaded On", :sortable => :created_at do |autoscoring_project_scoring_job_input_file|
                autoscoring_project_scoring_job_input_file.created_at.strftime("%m/%d/%y %I:%M %p %Z")
              end
              column "File Name" do |autoscoring_project_scoring_job_input_file|
                File.basename(autoscoring_project_scoring_job_input_file.file_name.url, ".csv")
              end
              column "Actions" do |autoscoring_project_scoring_job_input_file|
                link_to  "Download", download_csv_input_file_autoscoring_project_scoring_job_path(:input_file_id => autoscoring_project_scoring_job_input_file.id)
              end
            end
          end


          panel("Scored Output File(s)") do
            table_for(autoscoring_project_scoring_job_output_files) do
              column "Group" do |autoscoring_project_scoring_job_output_file|
                if autoscoring_project_scoring_job.has_multiple_output_files?
                  autoscoring_project_scoring_job_output_file.split_value
                else
                  "All Records"
                end
              end
              column "File Name" do |autoscoring_project_scoring_job_output_file|
                File.basename(autoscoring_project_scoring_job_output_file.file_name.url, ".csv")
              end
              column "Created On" do |autoscoring_project_scoring_job_output_file|
                autoscoring_project_scoring_job_output_file.created_at.strftime("%m/%d/%y %I:%M %p %Z")
              end
              column "Updated On" do |autoscoring_project_scoring_job_output_file|
                autoscoring_project_scoring_job_output_file.updated_at.strftime("%m/%d/%y %I:%M %p %Z")
              end
              column "Actions" do |autoscoring_project_scoring_job_output_file|
                link_to  "Download", download_csv_output_file_autoscoring_project_scoring_job_path(:output_file_id => autoscoring_project_scoring_job_output_file.id)
              end
            end
          end


        end
      end
    end
  end



end
