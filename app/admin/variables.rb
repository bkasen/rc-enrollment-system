ActiveAdmin.register Variable do
  menu :parent => "Variable Items"

  filter :variable_type
  filter :name
  filter :default
  filter :in_client_input_file
  

  form do |f|
    f.inputs "Details" do
      f.input :variable_type
      f.input :name
      f.input :abbreviation
      f.input :default
      f.input :in_client_input_file
    end

    f.buttons
  end
end
