ActiveAdmin.register AdminUser do     
  menu :parent => "Users"
  
  action_item :only => :show do
    link_to "Reset Password & Email Admin User",reset_password_admin_user_path(admin_user)
  end
  action_item :only => :show, :if => proc{ !admin_user.has_signed_in_before? } do
    link_to("Re-send Admin User Invitation", invite_admin_user_path(admin_user), :confirm => "Are you sure?")
  end
  
  index do                            
    column :email 
    column :first_name
    column :last_name                    
    column :current_sign_in_at        
    column :last_sign_in_at           
    column :sign_in_count             
    default_actions                   
  end                                 

  filter :email                       

  form do |f|                         
    f.inputs "Admin Details" do       
      f.input :email 
      f.input :first_name
      f.input :last_name                 
    end                               
    f.actions                         
  end                                 
end                                   
