ActiveAdmin.register Institution do

  controller do
    # This code is evaluated within the controller class

    # POST /institutions
    def create
      @institution = Institution.new(params[:institution])
      @postal_code = PostalCode.find(@institution.postal_code_id)
      @institution.state_id = @postal_code.state_id
      @institution.country_id = @postal_code.country_id
      respond_to do |format|
        if @institution.save
          format.html { redirect_to [:admin, @institution], :notice => 'Institution was successfully created.' }
        else
          format.html {
            flash[:error] = 'Institution not be saved. Please revise your selections.'
          render :action => "new"}
        end
      end
    end

    # PUT /institutions/1
    def update
      @institution = Institution.find(params[:id])
      @postal_code = PostalCode.find_by_name(params[:institution][:postal_code_name])
      params[:institution][:state_id] = @postal_code.state_id if @postal_code.present?
      params[:institution][:country_id] = @postal_code.country_id if @postal_code.present?
      respond_to do |format|
        if @institution.update_attributes(params[:institution])
          format.html { redirect_to [:admin, @institution], :notice => 'Institution was successfully updated.' }
        else
          format.html {
            flash[:error] = 'Institution not be saved. Please revise your selections'
            render :action => "edit"
          }
        end
      end
    end

  end

  filter :name
  filter :state

  form do |f|
    f.inputs "Details" do
      f.input :unitid
      f.input :name
      f.input :display_name
      f.input :emp_organization_name
      f.input :city
      f.input :postal_code_name, :label => "Postal Code", input_html:{data: {autocomplete_source: autocomplete_search_postal_codes_path} }
      f.buttons
    end
  end

end
