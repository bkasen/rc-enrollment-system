ActiveAdmin.register PostalCodesYear do
  menu :parent => "Other Items"


  index do
    column :postal_code
    column :year
    column :county
    column :mdg
    column :adg
    column :eth
    column :hvl
    column :mfi
    column :mhb
    column :pop
    default_actions
  end

  filter :year, :as => :select, :collection => Year.where("has_geodemographic_data = true") #only geodemographic years
  
end
