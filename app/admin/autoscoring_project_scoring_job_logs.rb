ActiveAdmin.register AutoscoringProjectScoringJobLog do
  menu false

  show do |autoscoring_project_scoring_job_log|


    attributes_table do
      row :id
      row "Autoscoring Project Scoring Job" do |autoscoring_project_scoring_job_log|
        link_to "#{autoscoring_project_scoring_job_log.autoscoring_project_scoring_job.autoscoring_project.name} - Scoring Job ##{autoscoring_project_scoring_job_log.autoscoring_project_scoring_job.id.to_s}", admin_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job_log.autoscoring_project_scoring_job)
      end
      row :created_at
      row :autoscoring_project_job_status do |autoscoring_project_scoring_job_log|
        autoscoring_project_scoring_job_log.autoscoring_project_job_status.name
      end
      row :row_count
      row :column_count
      row :rows_filtered_out
      row :error_details do |autoscoring_project_scoring_job_log|
        raw(RedCloth.new(autoscoring_project_scoring_job_log.error_details).to_html) if autoscoring_project_scoring_job_log.error_details.present?
      end
      row :filter_criteria do |autoscoring_project_scoring_job_log|
        raw(RedCloth.new(autoscoring_project_scoring_job_log.filter_criteria).to_html) if autoscoring_project_scoring_job_log.filter_criteria.present?
      end
      row :conversion_criteria do |autoscoring_project_scoring_job_log|
        raw(RedCloth.new(autoscoring_project_scoring_job_log.conversion_criteria).to_html) if autoscoring_project_scoring_job_log.conversion_criteria.present?
      end
      row :split_output_by_column_name do |autoscoring_project_scoring_job_log|
        raw(RedCloth.new(autoscoring_project_scoring_job_log.split_output_by_column_name).to_html) if autoscoring_project_scoring_job_log.split_output_by_column_name.present?
      end

      row :details do |autoscoring_project_scoring_job_log|
        raw(RedCloth.new(autoscoring_project_scoring_job_log.details).to_html) if autoscoring_project_scoring_job_log.details.present?
      end
    end
  end

end
