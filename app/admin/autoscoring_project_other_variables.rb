ActiveAdmin.register AutoscoringProjectOtherVariable do

  menu false

  controller do
    # This code is evaluated within the controller class

    def index
      redirect_to admin_autoscoring_projects_path
    end
  end

  show do
    columns do
      column do
        attributes_table do
          row "Auto-Scoring Project" do |autoscoring_project_other_variable|
            link_to autoscoring_project_other_variable.autoscoring_project.name, admin_autoscoring_project_path(autoscoring_project_other_variable.autoscoring_project)
          end
          row :variable do |autoscoring_project_other_variable|
            autoscoring_project_other_variable.variable.name
          end
          row :coefficient_value
          row :wald_score
          row "Primary Column Name" do |autoscoring_project_other_variable|
            autoscoring_project_other_variable.variable.name
          end
          row :secondary_column_name
          row :tertiary_column_name
          row :include_in_output
          row :has_uploaded_counts
          row :has_uploaded_translations
          row :date_format
        end
      end

      column do
        panel("Counts") do
          table_for(autoscoring_project_other_variable.autoscoring_project_other_variable_counts.order('name ASC') ) do
            column  "Description" do |autoscoring_project_other_variable_count|
              autoscoring_project_other_variable_count.name
            end
            column :enrolled
            column :total
            column "Yield Rate" do |autoscoring_project_other_variable_count|
              number_to_percentage(autoscoring_project_other_variable_count.yield, :precision => 6)
            end
          end
        end
      end
    end
  end

end
