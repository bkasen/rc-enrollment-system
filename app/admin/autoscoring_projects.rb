ActiveAdmin.register AutoscoringProject do

  scope :all, :default => true
  scope :em_configured
  scope :client_configured

  controller do
    # This code is evaluated within the controller class
    def scoped_collection
      # AutoscoringProject.includes(:autoscoring_project_status, :market_designation_level, :autoscoring_project_scoring_jobs, :institution, :autoscoring_project_geodemographic_variables => :variable, :autoscoring_project_other_variables => :variable, :autoscoring_project_filter_columns => :autoscoring_project_filter_values, :autoscoring_project_conversion_columns => :autoscoring_project_conversion_values)
      AutoscoringProject.includes(:autoscoring_project_status, :institution)
    end

    # def index
    #   @autoscoring_projects = AutoscoringProject.includes(:autoscoring_project_status, :institution).all
    # end

    def show
      @autoscoring_project = AutoscoringProject.includes(:market_designation_level, :institution, :geodemographic_year, :report_year, :autoscoring_project_filter_columns => :autoscoring_project_filter_values, :autoscoring_project_conversion_columns => :autoscoring_project_conversion_values).find(params[:id])
      @autoscoring_project_geodemographic_variables = @autoscoring_project.autoscoring_project_geodemographic_variables.joins(:variable).order('variables.position asc')
      @autoscoring_project_other_variables = @autoscoring_project.autoscoring_project_other_variables.joins(:variable).order('variables.name asc')
      @autoscoring_project_conversion_columns = @autoscoring_project.autoscoring_project_conversion_columns.includes(:autoscoring_project_conversion_values).order('name asc')
      @autoscoring_project_filter_columns = @autoscoring_project.autoscoring_project_filter_columns.includes(:autoscoring_project_filter_values).order('name asc')
      @autoscoring_project_scoring_jobs = @autoscoring_project.autoscoring_project_scoring_jobs.includes(:user).order("created_at DESC")
    end

    def new
      @autoscoring_project = AutoscoringProject.new
      Variable.find(:all, :conditions => "variable_type_id = 1").each do |variable|
        @autoscoring_project.autoscoring_project_geodemographic_variables.build(:variable_id => variable.id)
      end
    end

    # POST /autoscoring_projects
    def create
      @autoscoring_project = AutoscoringProject.new(params[:autoscoring_project])
      respond_to do |format|
        if @autoscoring_project.save
          format.html { redirect_to [:admin, @autoscoring_project], :notice => 'Autoscoring Project was successfully created.' }
        else
          format.html {
            flash[:error] = 'Autoscoring Project count not be saved. Please revise your selections.'
          render :action => "new"}
        end
      end
    end

    # PUT /autoscoring_projects/1
    def update
      @autoscoring_project = AutoscoringProject.find(params[:id])

      respond_to do |format|
        if @autoscoring_project.update_attributes(params[:autoscoring_project])
          format.html { redirect_to [:admin, @autoscoring_project], :notice => 'Autoscoring Project was successfully updated.' }
        else
          format.html {
            flash[:error] = 'Autoscoring Project count not be saved. Please revise your selections'
            render :action => "edit"
          }
        end
      end
    end

  end

  filter :institution_display_name, :as => :string
  filter :report_year, :as => :select, :collection => proc { Year.where('value >= ?', 2000) }
  filter :geodemographic_year, :as => :select, :collection => Year.where("has_geodemographic_data = true") #only geodemographic years
  filter :analyst
  filter :project_manager, :as => :select
  filter :name
  filter :autoscoring_project_status

  form :partial => "form"

  index do
    selectable_column
    column :id
    column :active do |autoscoring_project|
      status_tag(autoscoring_project.state)
    end
    column :autoscoring_project_status, :sortable => 'autoscoring_project_statuses.name'
    column :institution, :sortable => 'institutions.name'
    column :name, :sortable => 'institutions.name' do |autoscoring_project|
      autoscoring_project.name_with_configuration
    end
    column "Total # Jobs Completed" do |autoscoring_project|
      autoscoring_project.autoscoring_project_scoring_jobs.size
    end
    default_actions
  end

  show do
    columns do
      column do
        attributes_table do
          row :id
          row :institution
          row :report_year do |autoscoring_project|
            autoscoring_project.report_year.name
          end
          row :active
          row "Admin User (Receives Detailed Scoring Report)" do |autoscoring_project|
            link_to  autoscoring_project.admin_user.name, autoscoring_project.admin_user
          end
          row :project_manager
          row :autoscoring_project_status
          row :geodemographic_year do |autoscoring_project|
            autoscoring_project.geodemographic_year.name
          end
          row "Is Enrollment Manager Configured Project" do |autoscoring_project|
            if autoscoring_project.is_em_configured
              "Yes"
            else
              "No"
            end
          end
          if autoscoring_project.is_em_configured?
            row :em_person_id_column_name
            row :em_opportunity_id_column_name
          else
            row :client_opportunity_id_column_name
          end
          row :postal_code_column_name
          row :default_yield_rate
          row :model_y_intercept
          row :state_column_name
          row :split_output_by_column_name
          row :market_designation_level
          row :notes
          row :updated_at
        end




      end

      column do

        panel("Optional Filter Columns") do
          div do
            "By default, we include all records listed in the data file. If optional filters are specified below, we will then only include those records that meet the below criteria."
          end
          table_for(autoscoring_project_filter_columns) do
            column "Column Name" do |filter_column|
              filter_column.name
            end
            column "Description" do |filter_column|
              filter_column.description
            end
            column "Filter Method" do |filter_column|
              filter_column.filter_to_include_these_values? ? "Include These Values" : "Exclude These Values"
            end
            column "Values To Include (Within Parentheses)" do |filter_column|
              ul do
                filter_column.autoscoring_project_filter_values.each do |value|
                  li '"' + value.name + '"' + " : " + value.description
                end
              end
            end
          end
        end


        panel("Optional Conversion Columns") do
          div do
            span do
              "If optional column conversions are specified below, we will then convert records with the following <strong><i>From</i></strong> value into <strong><i>To</i></strong> value prior to scoring the record.".html_safe
            end
          end
          table_for(autoscoring_project_conversion_columns) do
            column "Column Name" do |conversion_column|
              conversion_column.name
            end
            column "Description" do |conversion_column|
              conversion_column.description
            end
            column "Conversion" do |conversion_column|
              ul do
                conversion_column.autoscoring_project_conversion_values.each do |value|
                  li "Convert values of \"<strong>#{value.from_value}</strong>\" to \"<strong>#{value.to_value}</strong>\"".html_safe
                end
              end
            end
          end
        end

      end
    end

    panel("Geodemographic Variables") do
      table_for(autoscoring_project_geodemographic_variables ) do
        column "Name" do |geodemographic_variable|
          geodemographic_variable.variable.name
        end
        column "Coefficient" do |geodemographic_variable|
          geodemographic_variable.coefficient_value
        end
        column "Wald Score" do |geodemographic_variable|
          geodemographic_variable.wald_score
        end
        column "Output" do |geodemographic_variable|
          status_tag(geodemographic_variable.included)
        end
        column "Status" do |autoscoring_project_other_variable|
          status_tag(autoscoring_project_other_variable.counts_present)
        end
        column "Counts" do |geodemographic_variable|
          if geodemographic_variable.has_uploaded_counts?
            link_to "View Counts", edit_admin_autoscoring_project_geodemographic_variable_count_import_path(:autoscoring_project_geodemographic_variable_id => geodemographic_variable.id)
          else
            link_to "Upload Counts", new_admin_autoscoring_project_geodemographic_variable_count_import_path(:autoscoring_project_geodemographic_variable_id => geodemographic_variable.id)
          end
        end
      end
    end

    panel("Other Variables") do
      table_for(autoscoring_project_other_variables) do
        column "Variable" do |autoscoring_project_other_variable|
          link_to autoscoring_project_other_variable.variable.name, admin_autoscoring_project_other_variable_path(autoscoring_project_other_variable)
        end
        column "Column Name(s)" do |autoscoring_project_other_variable|

          ol do
            li autoscoring_project_other_variable.name
            if autoscoring_project_other_variable.secondary_column_name?
              li autoscoring_project_other_variable.secondary_column_name
            end
            if autoscoring_project_other_variable.tertiary_column_name?
              li autoscoring_project_other_variable.tertiary_column_name
            end
          end
        end
        column "Coefficient" do |autoscoring_project_other_variable|
          autoscoring_project_other_variable.coefficient_value
        end
        column "Wald Score" do |autoscoring_project_other_variable|
          autoscoring_project_other_variable.wald_score
        end
        column "Date Format" do |autoscoring_project_other_variable|
          autoscoring_project_other_variable.date_format.name if autoscoring_project_other_variable.variable.is_date?
        end
        column "Output" do |autoscoring_project_other_variable|
          status_tag(autoscoring_project_other_variable.included)
        end
        column "Status" do |autoscoring_project_other_variable|
          status_tag(autoscoring_project_other_variable.counts_present)
        end
        column "Translations" do |autoscoring_project_other_variable|
          if autoscoring_project_other_variable.has_uploaded_translations?
            link_to "View Translations", edit_admin_autoscoring_project_other_variable_translation_import_path(:autoscoring_project_other_variable_id => autoscoring_project_other_variable.id)
          else
            link_to "Upload Translations", new_admin_autoscoring_project_other_variable_translation_import_path(:autoscoring_project_other_variable_id => autoscoring_project_other_variable.id)
          end
        end
        column "Counts" do |autoscoring_project_other_variable|
          if autoscoring_project_other_variable.has_uploaded_counts?
            link_to "View Counts", edit_admin_autoscoring_project_other_variable_count_import_path(:autoscoring_project_other_variable_id => autoscoring_project_other_variable.id)
          else
            link_to "Upload Counts", new_admin_autoscoring_project_other_variable_count_import_path(:autoscoring_project_other_variable_id => autoscoring_project_other_variable.id)
          end
        end

      end
    end

    panel("Scoring Jobs") do
      table_for(autoscoring_project_scoring_jobs) do
        column "ID" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.id
        end
        column "Created On" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.created_at.strftime("%m/%d/%y %I:%M %p %Z")
        end
        column "Updated On" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.updated_at.strftime("%m/%d/%y %I:%M %p %Z")
        end
        column "Row Count" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.row_count
        end
        column "User" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.user.name if autoscoring_project_scoring_job.user.present?
        end
        column "Public" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.is_public.to_s
        end
        column "Test Scoring" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.is_test_scoring.to_s
        end
        column "# Of Attempts" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.number_of_attempts
        end
        column "# Of Output Files" do |autoscoring_project_scoring_job|
          autoscoring_project_scoring_job.autoscoring_project_scoring_job_output_files.count
        end
        column "" do |autoscoring_project_scoring_job|
          link_to "Summary Details", admin_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job)
        end

        column "" do |autoscoring_project_scoring_job|
          link_to "Delete", admin_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job), :confirm => "Are you sure?", :method => :delete
        end
      end
    end
  end

end
