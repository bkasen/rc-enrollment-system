ActiveAdmin.register AutoscoringProjectGeodemographicVariableCountImport do
  menu false
  config.clear_action_items!

  breadcrumb do
    []
  end

  controller do
    # This code is evaluated within the controller class

    def index
      redirect_to admin_autoscoring_projects_path
    end

    def new

      @autoscoring_project_geodemographic_variable = AutoscoringProjectGeodemographicVariable.find(params[:autoscoring_project_geodemographic_variable_id])
      @autoscoring_project_geodemographic_variable_count_import = AutoscoringProjectGeodemographicVariableCountImport.new
    end

    def create
      @autoscoring_project_geodemographic_variable_count_import = AutoscoringProjectGeodemographicVariableCountImport.new(params[:autoscoring_project_geodemographic_variable_count_import])
      @autoscoring_project_geodemographic_variable = AutoscoringProjectGeodemographicVariable.find( @autoscoring_project_geodemographic_variable_count_import.autoscoring_project_geodemographic_variable_id)

      @autoscoring_project_geodemographic_variable_counts = AutoscoringProjectGeodemographicVariableCount.find(:all, :conditions => ["autoscoring_project_geodemographic_variable_id =?", @autoscoring_project_geodemographic_variable.id ], :order => "name ASC")

      
      respond_to do |format|
        if @autoscoring_project_geodemographic_variable_count_import.valid? && @autoscoring_project_geodemographic_variable_count_import.save
          @autoscoring_project_geodemographic_variable.reload
          @autoscoring_project_geodemographic_variable.has_uploaded_counts = true
          if @autoscoring_project_geodemographic_variable.save
            @autoscoring_project_geodemographic_variable.autoscoring_project.update_autoscoring_project_active_status_variable_counts
            format.html { redirect_to admin_autoscoring_project_path(@autoscoring_project_geodemographic_variable.autoscoring_project_id) , notice: "Imported counts successfully."}
          end
        else
          format.html {
            flash.now[:alert] = 'Import Did Not Complete Successfully. Please check the error details in the form below. Any existing records did not change.'
            render :action => "new"
          }
        end
      end
    end

    def edit

      @autoscoring_project_geodemographic_variable = AutoscoringProjectGeodemographicVariable.find(params[:autoscoring_project_geodemographic_variable_id])
      @autoscoring_project_geodemographic_variable_count_import = AutoscoringProjectGeodemographicVariableCountImport.new

      @autoscoring_project_geodemographic_variable_counts = AutoscoringProjectGeodemographicVariableCount.find(:all, :conditions => ["autoscoring_project_geodemographic_variable_id =?", @autoscoring_project_geodemographic_variable.id ], :order => "name ASC")
    end

    # def update
    #   debugger
    #   @autoscoring_project_geodemographic_variable_count_import = AutoscoringProjectGeodemographicVariableCountImport.new(params[:autoscoring_project_geodemographic_variable_count_import])
    #   @autoscoring_project_geodemographic_variable = AutoscoringProjectGeodemographicVariable.find( @autoscoring_project_geodemographic_variable_count_import.autoscoring_project_geodemographic_variable_id)
    #   params[:autoscoring_project_geodemographic_variable] = Hash.new
    # 
    #   respond_to do |format|
    #     if @autoscoring_project_geodemographic_variable_count_import.save
    #       params[:autoscoring_project_geodemographic_variable][:has_uploaded_counts] = true
    #       if @autoscoring_project_geodemographic_variable.update_attributes(params[:autoscoring_project_geodemographic_variable])
    #         debugger
    #         @autoscoring_project_geodemographic_variable.autoscoring_project.update_autoscoring_project_active_status_variable_counts
    #         format.html { redirect_to admin_autoscoring_project_path(@autoscoring_project_geodemographic_variable.autoscoring_project_id) , notice: "Imported counts successfully." }
    #       end
    #     else
    #       @autoscoring_project_geodemographic_variable.has_uploaded_counts = true
    #       format.html {
    #         flash.now[:alert] = 'Import Did Not Complete Successfully. Please check the error details in the form below. Any existing records did not change.'
    #         render :action => "new"
    #       }
    #     end
    #   end
    # 
    # end

    def show
      redirect_to admin_autoscoring_projects_path
    end

  end

  form :partial => "form"

end
