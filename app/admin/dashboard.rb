ActiveAdmin.register_page "Dashboard" do

  menu :priority => 0, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    #   div :class => "blank_slate_container", :id => "dashboard_default_message" do
    #     span :class => "blank_slate" do
    #       span I18n.t("active_admin.dashboard_welcome.welcome")
    #       small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #     end
    #   end
    #
    #   # Here is an example of a simple dashboard with columns and panels.
    #   #
    #   # columns do
    #   #   column do
    #   #     panel "Recent Posts" do
    #   #       ul do
    #   #         Post.recent(5).map do |post|
    #   #           li link_to(post.title, admin_post_path(post))
    #   #         end
    #   #       end
    #   #     end
    #   #   end
    #
    #   #   column do
    #   #     panel "Info" do
    #   #       para "Welcome to ActiveAdmin."
    #   #     end
    #   #   end
    #   # end

    columns do

      column do
        panel "Recent Scoring Jobs" do
          table_for AutoscoringProjectScoringJob.last(5).reverse do
            column :id
            column "Auto-Scoring Project" do |autoscoring_project_scoring_job|
              link_to autoscoring_project_scoring_job.autoscoring_project.name_with_configuration, admin_autoscoring_project_path(autoscoring_project_scoring_job.autoscoring_project)
            end
            column :user
            column "Scoring Type", :sortable => "is_test_scoring" do |autoscoring_project_scoring_job|
              if autoscoring_project_scoring_job.is_test_scoring && !autoscoring_project_scoring_job.is_public
                "Test Scoring / Private"
              elsif !autoscoring_project_scoring_job.is_test_scoring && !autoscoring_project_scoring_job.is_public
                "Actual Scoring / Private"
              elsif !autoscoring_project_scoring_job.is_test_scoring && autoscoring_project_scoring_job.is_public
                "Actual Scoring / Public"
              end
            end
            column "Current Status" do |autoscoring_project_scoring_job|
              autoscoring_project_scoring_job.autoscoring_project_job_status.name
            end
            column :rows_processed
            column :created_at
            column do |autoscoring_project_scoring_job|
              link_to "Summary Details", admin_autoscoring_project_scoring_job_path(autoscoring_project_scoring_job)
            end
          end
        end
      end

    end # columns

    columns do

      column do
        panel "CPU" do

          div do
            br
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/cRNWxahoPs" width="500" height="300" scrolling="no" frameborder="no"></iframe>}.html_safe
          end

        end
      end
      column do
        panel "Memory" do
          div do
            br
            text_node %{<iframe src="https://rpm.newrelic.com/public/charts/eqbGEGGXXZx" width="500" height="300" scrolling="no" frameborder="no"></iframe>  }.html_safe
          end
        end
      end

    end # columns

  end # content

end
