ActiveAdmin.register ProjectManager do
  menu :parent => "Users", :label => "Project Managers"

  scope :all, :default => true

  filter :name
  filter :first_name
  filter :last_name

  index do
    selectable_column
    column :id
    column :email
    column :first_name
    column :last_name
    default_actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.buttons
    end
  end
end
