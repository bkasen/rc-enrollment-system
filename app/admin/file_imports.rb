ActiveAdmin.register FileImport do
  menu :parent => "File Imports"
  
  scope :all, :default => true
  scope :unprocessed
  scope :complete

  action_item :only => :show do
    link_to('Import', import_file_import_path(file_import), :id =>'import') if !file_import.processed?
  end


  action_item :only => :show, :if => proc{ Rails.env.development? || Rails.env.test? }do
    link_to('Test Import', test_import_file_import_path(file_import), :id =>'import')
  end


  index do
    selectable_column
    column :id
    column :file_import_type
    column :name do |file_import|
      link_to file_import.name, download_file_import_path(file_import)
    end
    column :created_at
    column :updated_at
    column :processed do |file_import|
      status_tag(file_import.state)
    end
    default_actions
  end

  filter :file_import_type
  filter :admin_user
  filter :user
  filter :institution
  filter :name

  form do |f|
    f.inputs "Details" do
      f.input :file_import_type,   :as => :select, :collection => FileImportType.order("name ASC").where("institution_specific = ? ", false)
      f.input :name
      f.input :file_name
      f.input :admin_user_id, :as => :hidden, :value => current_admin_user.id
      f.input :processed, :as => :hidden, :value => false
    end

    f.buttons
  end

  show do |file_import|


    attributes_table do
      row :name do |file_import|
        link_to file_import.name, download_file_import_path(file_import)
      end
      row :file_import_type
      row :institution unless file_import.institution.nil?
      row :processed
      row :created_at
      row :updated_at

    end
    div id:"spinner", :class => "spinner", :style => "display:none" do
      div do
        image_tag('ajax-loader.gif')
      end
    end

    active_admin_comments
  end

end
