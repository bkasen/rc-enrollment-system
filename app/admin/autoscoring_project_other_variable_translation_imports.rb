ActiveAdmin.register AutoscoringProjectOtherVariableTranslationImport do
  menu false
  config.clear_action_items!

  breadcrumb do
    []
  end

  controller do
    # This code is evaluated within the controller class

    def index
      redirect_to admin_autoscoring_projects_path
    end

    def new
      @autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find(params[:autoscoring_project_other_variable_id])
      @autoscoring_project_other_variable_translation_import = AutoscoringProjectOtherVariableTranslationImport.new
    end

    def create
      @autoscoring_project_other_variable_translation_import = AutoscoringProjectOtherVariableTranslationImport.new(params[:autoscoring_project_other_variable_translation_import])
      @autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find( @autoscoring_project_other_variable_translation_import.autoscoring_project_other_variable_id)
      @autoscoring_project_other_variable.has_uploaded_translations = true

      if @autoscoring_project_other_variable_translation_import.save && @autoscoring_project_other_variable.save
        redirect_to admin_autoscoring_project_path(@autoscoring_project_other_variable.autoscoring_project_id) , notice: "Imported translations successfully."
      else
        render :new
      end
    end

    def edit

      @autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find(params[:autoscoring_project_other_variable_id])
      @autoscoring_project_other_variable_translation_import = AutoscoringProjectOtherVariableTranslationImport.new

      @autoscoring_project_other_variable_translations = AutoscoringProjectOtherVariableTranslation.find(:all, :conditions => ["autoscoring_project_other_variable_id =?", @autoscoring_project_other_variable.id ], :order => "code ASC")
    end

    def update

      @autoscoring_project_other_variable_translation_import = AutoscoringProjectOtherVariableTranslationImport.new(params[:autoscoring_project_other_variable_translation_import])
      @autoscoring_project_other_variable = AutoscoringProjectOtherVariable.find( @autoscoring_project_other_variable_translation_import.autoscoring_project_other_variable_id)
      @autoscoring_project_other_variable.has_uploaded_translations = true

      if @autoscoring_project_other_variable_translation_import.save && @autoscoring_project_other_variable.save
        redirect_to admin_autoscoring_project_path(@autoscoring_project_other_variable.autoscoring_project_id) , notice: "Imported translations successfully."
      else
        render :new
      end

    end

    def show
      redirect_to admin_autoscoring_projects_path
    end

  end

  form :partial => "form"

end
