ActiveAdmin.register User do
  menu :parent => "Users", :label => "General Users"
  action_item :only => :show do
    link_to "Reset Password & Email User",reset_password_user_path(user)
  end
  action_item :only => :show, :if => proc{ !user.has_signed_in_before? } do
    link_to("Re-send User Invitation", invite_user_path(user), :confirm => "Are you sure?")
  end

  controller do
    # This code is evaluated within the controller class

    def action_methods
      super - ['destroy']
    end


    def scoped_collection
      resource_class.includes(:job_title) # prevents N+1 queries to your database
    end
  end

  scope :all, :default => true
  scope :active
  scope :inactive



  filter :name
  filter :job_title
  filter :first_name
  filter :last_name
  filter :active

  index do
    selectable_column
    column :id
    column :email
    column "Job Title", :sortable => 'job_titles.name' do |user|
      user.job_title.name
    end
    column :first_name
    column :last_name
    column :created_at
    default_actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :job_title
      # f.input :job_title_name, :label => "Job Title", input_html:{data: {autocomplete_source: job_titles_path} }
      if user.new_record?
        f.input :active, :input_html => { :checked => 'checked' }
      else
        f.input :active
      end
      f.buttons
    end
  end
  
  show do |user|


    attributes_table do
      row :id 
      row :email
      row :first_name
      row :last_name
      row :job_title
      row :active do |user|
        if user.active
          "Yes"
        else
          "No"
        end
      end
      row :sign_in_count
      row :last_sign_in_at
      row :remember_created_at
      row :created_at
      row :updated_at

    end
    div id:"spinner", :class => "spinner", :style => "display:none" do
      div do
        image_tag('ajax-loader.gif')
      end
    end

    active_admin_comments
  end
end
