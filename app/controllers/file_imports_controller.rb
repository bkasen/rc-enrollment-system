class FileImportsController < ApplicationController
  before_filter :authenticate_admin_user!
  def download
    @file_import = FileImport.find(params[:id])
    send_file @file_import.file.to_s, :disposition => 'attachment'
  rescue ActiveRecord::RecordNotFound
    logger.warn("#{Time.now} - Requested File Not Found: #{params.inspect}")
    render :text => t('private_files_controller.not_found'), :status => 404
  end

  def import
    @file_import = FileImport.find(params[:id])

    respond_to do |format|
      if  Resque.enqueue(FileImportProcessor, @file_import.id)
        format.html { redirect_to( [:admin, @file_import], :notice => 'File Import was successfully placed on the queue to be processed & imported.') }
      else
        format.html { redirect_to( [:admin, @file_import], :alert => @file_import.error_details) }
      end
    end

  end
  
  def test_import
    @file_import = FileImport.find(params[:id])

    respond_to do |format|
      if  @file_import.import
        format.html { redirect_to( [:admin, @file_import], :notice => 'File Import was successfully placed on the queue to be processed & imported.') }
      else
        format.html { redirect_to( [:admin, @file_import], :alert => @file_import.error_details) }
      end
    end

  end

end
