class JobTitlesController < ApplicationController
  before_filter :authenticate_admin_user!
  def index
    @job_titles = JobTitle.order(:name).where("name like ?", "%#{params[:term]}%")
    render json: @job_titles.map(&:auto_complete_name)
  end


end
