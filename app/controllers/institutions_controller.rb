class InstitutionsController < ApplicationController
  before_filter :authenticate_admin_user!
  def index
    @institutions = Institution.order(:name).where("name like ?", "%#{params[:term]}%")
    render json: @institutions.map(&:auto_complete_name)
  end

  def select
    @institution = Institution.find(params[:id])
    session[:current_institution] = @institution.id
    session[:current_institution_name] = @institution.name
    respond_to do |format|
      if session[:current_institution]
        format.html { redirect_to :back, :notice => @institution.name + ' was selected.' }
      else
        format.html { redirect_to :back, :alert => "Error selecting school." }
      end

    end
  end

  def deselect
    @institution = Institution.find(session[:current_institution])
    session[:current_institution] = nil
    session[:current_institution_name] = nil
    redirect_to :back, :notice => @institution.name + ' was de-selected.'
  end
end
