class ApplicationController < ActionController::Base
  protect_from_forgery

  def authenticate_any_user!
    if admin_user_signed_in?
      true
    else
      authenticate_user!
    end
  end

  def check_autoscoring_project_availibility
    if admin_user_signed_in?
      true
    else

      if params[:controller] == "autoscoring_projects"
        # Used when presenting autoscoring_project summary page
        autoscoring_project = AutoscoringProject.find(params[:id])

      elsif params[:controller] == "autoscoring_project_scoring_jobs"
        if params[:autoscoring_project_id].present?
          # Used when presenting user the create new scoring job form
          autoscoring_project = AutoscoringProject.find(params[:autoscoring_project_id])
        else
          if params[:autoscoring_project_scoring_job].present?
            # Used when creating a new scoring job
            autoscoring_project = AutoscoringProject.find(params[:autoscoring_project_scoring_job][:autoscoring_project_id])
          else
            # Used when displaying a scoring job's show view
            autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
            autoscoring_project = autoscoring_project_scoring_job.autoscoring_project
          end
        end
      else
        autoscoring_project = params[:autoscoring_project_id]
      end

      if autoscoring_project.present? && (current_user.is_administrator? || autoscoring_project.active)
        true
      else
        redirect_to( root_url, :alert => 'The Auto-Scoring Project you tried to reference is inactive. Please make sure you are using the latest Auto-Scoring Project a client.')
      end
    end
  end
end
