class AutoscoringProjectScoringJobsController < ApplicationController
  before_filter :authenticate_any_user!
  before_filter :check_autoscoring_project_availibility
  
  
  def index
    @autoscoring_project_scoring_jobs = AutoscoringProjectScoringJob.all

  end

  def show
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.includes(:user, :autoscoring_project_scoring_job_input_files,:autoscoring_project_scoring_job_output_files, :autoscoring_project => [:autoscoring_project_geodemographic_variables => [:variable], :autoscoring_project_other_variables => [:variable]]).find(params[:id])
    @autoscoring_project_scoring_job_log = @autoscoring_project_scoring_job.last_autoscoring_project_scoring_job_log
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.new
    respond_to do |format|
      if params[:autoscoring_project_id]
        @autoscoring_project = AutoscoringProject.find(params[:autoscoring_project_id])
        1.times { @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_files.build }
        @user = current_user
        format.html # new.html.erb
      else
        format.html { redirect_to root_path }
      end
    end
  end

  # GET /autoscoring_project_scoring_jobs/1/edit
  def edit
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    @autoscoring_project = @autoscoring_project_scoring_job.autoscoring_project
    1.times { @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_files.build }
    @user = @autoscoring_project_scoring_job.user
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # POST /autoscoring_project_scoring_jobs
  def create
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.new(params[:autoscoring_project_scoring_job])
    @autoscoring_project = @autoscoring_project_scoring_job.autoscoring_project
    @autoscoring_project_scoring_job.autoscoring_project_job_status_id = 1 # Not Started Processing
    respond_to do |format|
      if @autoscoring_project_scoring_job.save
        @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 5) # Queued up for Processing
        if ScoringJobWorker.perform_async(@autoscoring_project_scoring_job.id)
          #queued up successfully
        else
          @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 1) # Not Started Processing
        end
        sleep(2)
        # ScoringJobsWorker.perform_async(@autoscoring_project_scoring_job.id)
        format.html { redirect_to( @autoscoring_project_scoring_job.autoscoring_project, :notice => 'Scoring Job was Created Successfully & placed in the Scoring Queue.') }
      else
        @user = current_user
        format.html { render :action => "new" }
      end
    end
  end

  # PUT /autoscoring_project_scoring_jobs/1
  def update
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    @autoscoring_project = @autoscoring_project_scoring_job.autoscoring_project
    params[:autoscoring_project_scoring_job][:has_file_dimension_counts] = false
    params[:autoscoring_project_scoring_job][:autoscoring_project_job_status_id] = 1 # Not Started Processing


    respond_to do |format|
      if @autoscoring_project_scoring_job.update_attributes(params[:autoscoring_project_scoring_job])
        @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 5) # Queued up for Processing
        if ScoringJobWorker.perform_async(@autoscoring_project_scoring_job.id)
          #queued up successfully
        else
          @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 1) # Not Started Processing
        end
        sleep(2)
        # format.html { redirect_to(@autoscoring_project_scoring_job.autoscoring_project, :anchor => "scoring-jobs-list", :notice => 'Scoring Job Input File was successfully updated.') }
        format.html { redirect_to( @autoscoring_project_scoring_job.autoscoring_project, :notice => 'Scoring Job was Created Updated & placed in the Scoring Queue.') }
      else
        @user = @autoscoring_project_scoring_job.user
        format.html { render :action => "edit" }
      end
    end
  end

  def delete_scoring_job_records
    authenticate_admin_user!
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    # delete scoring job records
    @autoscoring_project_scoring_job.delete_all_scoring_job_records
    respond_to do |format|
      format.html { redirect_to( [:admin,@autoscoring_project_scoring_job], :notice => 'Scoring Job Record Deletion Process Initiated for this Scoring Job.') }
    end
  end

  def download_current_input_file
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])

    input_file = @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_file.file_name

    # Store the file locally in the tmp file, Cronjob will clean it up later on
    input_file.retrieve_from_store!(File.basename(input_file.url))
    input_file.cache_stored_file!
    @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_file if send_file input_file.file.path, :disposition => 'attachment'

  rescue ActiveRecord::RecordNotFound
    redirect_to( @autoscoring_project_scoring_job, :alert => 'Requested CSV File Not Found on Server. Please contact the Admin for assistance.')

    # send_file @autoscoring_project_scoring_job.autoscoring_project_scoring_job_input_file.file_name.to_s, :disposition => 'attachment'
    # rescue ActiveRecord::RecordNotFound
    #   logger.warn("#{Time.now} - Requested File Not Found: #{params.inspect}")
    #   render :text => t('private_files_controller.not_found'), :status => 404
  end

  def create_dbf_file
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])

    respond_to do |format|
      if @autoscoring_project_scoring_job.create_dbf_file
        format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring DBF was successfully created.') }
      else
        format.html { redirect_to( @autoscoring_project_scoring_job, :alert => 'Error Creating DBF(s) for Scoring Job') }
      end
    end
  end


  def create_output_csv_file
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])

    respond_to do |format|
      if @autoscoring_project_scoring_job.create_scored_csv_output_file
        format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring CSV was successfully created.') }
      else
        format.html { redirect_to( @autoscoring_project_scoring_job, :alert => 'Error Creating CSV(s) for Scoring Job') }
      end
    end
  end

  def download_dbf_file
    @autoscoring_project_scoring_job_output_file = AutoscoringProjectScoringJobOutputFile.find(params[:id])
    scored_file = @autoscoring_project_scoring_job_output_file.file_name
    scored_file.retrieve_from_store!(File.basename(scored_file.url))
    scored_file.cache_stored_file!
    send_file scored_file.file.path, :disposition => 'attachment'
  rescue ActiveRecord::RecordNotFound
    redirect_to( @autoscoring_project_scoring_job_output_file.autoscoring_project_scoring_job, :alert => 'Requested DBF File Not Found on Server. Please contact the Admin for assistance.')
  end
  
  def download_csv_input_file
    debugger
      @autoscoring_project_scoring_job_input_file = AutoscoringProjectScoringJobInputFile.find(params[:input_file_id])
      input_file = @autoscoring_project_scoring_job_input_file.file_name

      # Store the file locally in the tmp file, Cronjob will clean it up later on
      input_file.retrieve_from_store!(File.basename(input_file.url))
      input_file.cache_stored_file!
      send_file input_file.file.path, :disposition => 'attachment'
    rescue ActiveRecord::RecordNotFound
      redirect_to( @autoscoring_project_scoring_job_input_file.autoscoring_project_scoring_job, :alert => 'Requested CSV File Not Found on Server. Please contact the Admin for assistance.')
    end

  def download_csv_output_file
    @autoscoring_project_scoring_job_output_file = AutoscoringProjectScoringJobOutputFile.find(params[:output_file_id])
    scored_file = @autoscoring_project_scoring_job_output_file.file_name

    # Store the file locally in the tmp file, Cronjob will clean it up later on
    scored_file.retrieve_from_store!(File.basename(scored_file.url))
    scored_file.cache_stored_file!
    send_file scored_file.file.path, :disposition => 'attachment'
  rescue ActiveRecord::RecordNotFound
    redirect_to( @autoscoring_project_scoring_job_output_file.autoscoring_project_scoring_job, :alert => 'Requested CSV File Not Found on Server. Please contact the Admin for assistance.')
  end

  # Resque Version
  def enqueue_file_on_resque
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    @autoscoring_project_scoring_job.autoscoring_project_job_status_id = 1 # Queued

    respond_to do |format|
      if @autoscoring_project_scoring_job.save
        if Resque.enqueue(ScoringJobProcessor, @autoscoring_project_scoring_job.id)
          @autoscoring_project_scoring_job.update_attributes(:autoscoring_project_job_status_id => 5) # Queued up for Processing
        end
        format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring Job was Created Updated & placed in the Scoring Queue.') }
      else
        format.html { render :action => "show" }
      end
    end
  end


  #Sidekiq Version
  def enqueue_file_on_sidekiq
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    @autoscoring_project_scoring_job.autoscoring_project_job_status_id = 1 # Queued

    respond_to do |format|
      if @autoscoring_project_scoring_job.save
        if ScoringJobWorker.perform_async(@autoscoring_project_scoring_job.id)
          @autoscoring_project_scoring_job.update_attributes(:autoscoring_project_job_status_id => 5) # Queued up for Processing
        end
        format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring Job was Created Updated & placed in the Scoring Queue.') }
      else
        format.html { render :action => "show" }
      end
    end
  end

  def process_file
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    @autoscoring_project_scoring_job.start_time = Time.now
    @autoscoring_project_scoring_job.process
    @autoscoring_project_scoring_job.finish_time = Time.now
    
    respond_to do |format|
      if !@autoscoring_project_scoring_job.errors.messages.any?
        if @autoscoring_project_scoring_job.save
          if @autoscoring_project_scoring_job.processed? && @autoscoring_project_scoring_job.completed?
            @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 3) # Completed Successfully
            @autoscoring_project_scoring_job.touch
            # send completed email
            ScoringJobMailer.scoring_job_completed_successfully(@autoscoring_project_scoring_job.id).deliver

            format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring Job was successfully processed.') }
          else
            if ScoringJobMailer.scoring_job_completed_with_error(@autoscoring_project_scoring_job.id).deliver
              @autoscoring_project_scoring_job.update_column(:autoscoring_project_job_status_id, 4) # Incomplete Successfully
              @autoscoring_project_scoring_job.touch
            end
            format.html { redirect_to( @autoscoring_project_scoring_job, :alert => @autoscoring_project_scoring_job.scoring_error_details) }
          end
        end
      end
    end
  end

  def preview_email
    @autoscoring_project_scoring_job = AutoscoringProjectScoringJob.find(params[:id])
    if @autoscoring_project_scoring_job.completed?
      ScoringJobMailer.scoring_job_completed_successfully(@autoscoring_project_scoring_job.id).deliver
    elsif @autoscoring_project_scoring_job.encountered_errors?
      ScoringJobMailer.scoring_job_completed_with_error(@autoscoring_project_scoring_job.id).deliver
    end
    respond_to do |format|
      format.html { redirect_to( @autoscoring_project_scoring_job, :notice => 'Scoring Job was Created Updated & placed in the Scoring Queue.') }
    end
  end

end
