class PostalCodesController < ApplicationController
  before_filter :authenticate_admin_user!
  def autocomplete_search
    if params[:term].length >= 3
      @postal_codes = PostalCode.order(:name).where("name like ?", "#{params[:term]}%")
      render json: @postal_codes.map(&:name)
    else
      render json: ["Continue Typing..."]
    end
  end

end
