class UsersController < ApplicationController
  before_filter :authenticate_admin_user!

  def invite
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.invite
        format.html { redirect_to [:admin, @user], :notice => "#{@user.email} was sent an invitation" }
      else
        format.html { redirect_to [:admin, @user], :error => "Encountered error inviting user."}
      end
    end
  end

  def reset_password
    @user = User.find(params[:id])
    
    respond_to do |format|
      if UserMailer.reset_general_user_password(@user).deliver
        format.html { redirect_to [:admin, @user], :notice => "#{@user.email} was sent a password reset email" }
      else
        format.html { redirect_to [:admin, @user], :error => "Encountered error sending password reset email to user."}
      end
    end
  end

end
