class AdminUsersController < ApplicationController

  before_filter :authenticate_admin_user!

  def invite
    @admin_user = AdminUser.find(params[:id])
    respond_to do |format|
      if @admin_user.invite
        format.html { redirect_to [:admin, @admin_user], :notice => "#{@admin_user.email} was sent an Admin Invitation" }
      else
        format.html { redirect_to [:admin, @admin_user], :error => "Encountered error inviting Admin User."}
      end
    end
  end

  def reset_password
    @admin_user = AdminUser.find(params[:id])
    respond_to do |format|
      if  @admin_user.reset_password
        format.html { redirect_to [:admin, @admin_user], :notice => "#{@admin_user.email} was sent an Admin Panel password reset email" }
      else
        format.html { redirect_to [:admin, @admin_user], :notice => "Encountered error sending Admin Panel password reset email to user." }
      end
    end
  end

end
