class HomeController < ApplicationController
  before_filter :authenticate_user!
  def index
    if current_user.is_administrator?
      @available_em_configured_autoscoring_projects = AutoscoringProject.includes(:autoscoring_project_scoring_jobs, :institution).select("institutions.name, autoscoring_projects.name").where('autoscoring_projects.active = true AND autoscoring_projects.autoscoring_project_status_id  IN (?) AND autoscoring_projects.is_em_configured = true', [2,3]).order('institutions.name ASC')
      @available_client_configured_autoscoring_projects = AutoscoringProject.includes(:autoscoring_project_scoring_jobs, :institution).select("institutions.name, autoscoring_projects.name").where('autoscoring_projects.active = true AND autoscoring_projects.autoscoring_project_status_id  IN (?) AND autoscoring_projects.is_em_configured = false', [2,3]).order('institutions.name ASC')
    else
      @current_users_available_recent_scoring_jobs = AutoscoringProjectScoringJob.includes(:autoscoring_project, :autoscoring_project_job_status).where('autoscoring_project_scoring_jobs.user_id = ? AND autoscoring_projects.active = true  AND autoscoring_projects.autoscoring_project_status_id = 3', current_user.id).order('autoscoring_project_scoring_jobs.updated_at DESC').limit(5)
      @available_em_configured_autoscoring_projects = AutoscoringProject.includes(:autoscoring_project_scoring_jobs, :institution).select("institutions.name, autoscoring_projects.name").where('autoscoring_projects.active = true AND autoscoring_projects.autoscoring_project_status_id = ? AND autoscoring_projects.is_em_configured = true', 3).order('institutions.name ASC')
      @available_client_configured_autoscoring_projects = AutoscoringProject.includes(:autoscoring_project_scoring_jobs, :institution).select("institutions.name, autoscoring_projects.name").where('autoscoring_projects.active = true AND autoscoring_projects.autoscoring_project_status_id = ? AND autoscoring_projects.is_em_configured = false', 3).order('institutions.name ASC')

    end
  end
end
