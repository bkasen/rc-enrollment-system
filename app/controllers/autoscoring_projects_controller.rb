class AutoscoringProjectsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_autoscoring_project_availibility

  def show
    @autoscoring_project = AutoscoringProject.includes(:report_year, {:autoscoring_project_geodemographic_variables => :variable}, {:autoscoring_project_other_variables => :variable}, {:autoscoring_project_filter_columns => :autoscoring_project_filter_values}, {:autoscoring_project_conversion_columns => :autoscoring_project_conversion_values}, :institution, :project_manager).find(params[:id])
    # @autoscoring_project_scoring_jobs = AutoscoringProjectScoringJob.includes(:autoscoring_project_job_status, :user, :autoscoring_project_scoring_job_input_files).select("autoscoring_project_scoring_jobs.id, autoscoring_project_scoring_jobs.created_at, autoscoring_project_scoring_jobs.updated_at, autoscoring_project_scoring_job.autoscoring_project_job_status_id").where('autoscoring_project_id = ? ', @autoscoring_project.id).order('autoscoring_project_scoring_jobs.created_at DESC')
    if current_user.is_administrator?
      @autoscoring_project_scoring_jobs = AutoscoringProjectScoringJob.includes(:autoscoring_project_job_status, :user).select("autoscoring_project_scoring_jobs.id, autoscoring_project_scoring_jobs.is_test_scoring, autoscoring_project_scoring_jobs.created_at, autoscoring_project_scoring_jobs.input_file_row_count, autoscoring_project_scoring_jobs.start_time, autoscoring_project_scoring_jobs.finish_time, autoscoring_project_scoring_jobs.number_of_attempts, users.first_name, users.last_name, autoscoring_project_scoring_job_statuses.name").where('autoscoring_project_id = ? ', @autoscoring_project.id).order('autoscoring_project_scoring_jobs.updated_at DESC')
    else
      @autoscoring_project_scoring_jobs = AutoscoringProjectScoringJob.includes(:autoscoring_project_job_status, :user).select("autoscoring_project_scoring_jobs.id, autoscoring_project_scoring_jobs.is_test_scoring, autoscoring_project_scoring_jobs.created_at, autoscoring_project_scoring_jobs.input_file_row_count, autoscoring_project_scoring_jobs.start_time, autoscoring_project_scoring_jobs.finish_time, autoscoring_project_scoring_jobs.number_of_attempts, users.first_name, users.last_name, autoscoring_project_scoring_job_statuses.name").where('autoscoring_project_scoring_jobs.autoscoring_project_id = ? AND autoscoring_project_scoring_jobs.is_test_scoring = ? AND autoscoring_project_scoring_jobs.is_public = ?', @autoscoring_project.id, false, true).order('autoscoring_project_scoring_jobs.updated_at DESC')
    end

    respond_to do |format|
      format.html # show.html.erb
    end
  end

end
