RcEnrollmentSystem::Application.routes.draw do

  resources :autoscoring_project_scoring_jobs do
    member do
      get 'process_file'
      get 'queued_process_file'
      get 'download_current_input_file'
      get 'create_dbf_file'
      get 'create_output_csv_file'
      get 'download_dbf_file'
      get 'download_csv_input_file'
      get 'download_csv_output_file'
      get 'enqueue_file_on_resque'
      get 'enqueue_file_on_sidekiq'
      get 'preview_email'
      get 'delete_scoring_job_records'
    end
  end

  resources :postal_codes do
    collection do
      get 'autocomplete_search'
    end
  end

  resources :job_titles

  resources :autoscoring_project_other_variables

  resources :autoscoring_project_other_variable_count_imports

  resources :autoscoring_project_geodemographic_variable_count_imports


  resources :autoscoring_projects

  resources :institutions

  resources :file_imports do
    member do
      get 'test_import'
      get 'import'
      get 'download'
    end
  end

  devise_for :users

  resources :users do
    member do
      get 'reset_password'
      get 'invite'
    end
  end

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  resources :admin_users do
    member do
      get 'reset_password'
      get 'invite'
    end
  end

  root :to => 'home#index'

  authenticate :admin_user do
    mount Resque::Server.new, :at => "/resque"
  end

  authenticate :admin_user do
    mount Sidekiq::Web.new, at: '/sidekiq'
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
