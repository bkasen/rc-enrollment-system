class String
  def to_bool
    return true   if self == true   || self =~ (/(true|t|yes|y|1|1.0)$/i)
    return false  if self == false  || self.blank? || self =~ (/(false|f|no|n|0|0.0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end

  def is_integer?
    !!(self =~ /^[-+]?[0-9]+$/)
  end

  def is_numeric?
    true if Float(self) rescue false
  end
  
  
  def remove_non_ascii
    require 'iconv'
    Iconv.conv('ASCII//IGNORE', 'UTF8', self)
  end
  
end
