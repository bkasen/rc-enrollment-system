  require 'resque'
  require 'resque/server'

  rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
  rails_env = ENV['RAILS_ENV'] || 'development'

  resque_config = YAML.load_file(rails_root + '/config/resque.yml')
  Resque.redis = resque_config[rails_env]
  
  
  # Pre-load all the ActiveRecord column metadata.
  Resque.before_first_fork do
    ActiveRecord::Base.send(:subclasses).each do |klass|
      unless klass.abstract_class?
        klass.primary_key
        klass.columns
      end
    end
  end
  
